exports.config = {
  // See http://brunch.io/#documentation for docs.
  modules: {
    autoRequire: {
      // outputFileName : [ entryModule ]
      'build/main.js': ['src/app/main']
    }
  },
  paths: {
    public: './www',
    watched: ['src']
  },
  files: {
    javascripts: {
      joinTo: {
        'build/vendor.js': /^node_modules/,
        'build/main.js': /^src/
      },
      order: {
        after: [/\.html$/, /\.css$/]
      }
    },
    stylesheets: {
      joinTo: 'build/main.css'
    },
    templates: {
      joinTo: 'build/main.js'
    }
  },
  conventions: {
    ignored: [
      /[\\/]_/,
      /vendor[\\/]node[\\/]/,
      /vendor[\\/](j?ruby-.*|bundle)[\\/]/,
      /main.prod/,
      /vendor.prod/,
      "node_modules/lodash-es/**/*.js"
    ],
    assets: [
      /public[\\/]/
    ]
  },
  plugins: {
    brunchTypescript: {
      removeComments: true,
      ignoreErrors: true
    },
    sass: {
      options: {
        includePaths: [
          // 'app/scss',
          // './node_modules/angular-material/*'
        ]
      }
    },
    inlineCss: {
      html: true,
      passthrough: [/^node_modules/, 'app/global.css']
    }
  }
};
