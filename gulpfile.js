var gulp = require('gulp');

var templatesWrap = require('gulp-ng2-template-wrap');
var webserver = require('gulp-webserver');
var concat = require('gulp-concat');
var gulpif = require('gulp-if');
var argv = require('yargs').argv;
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
var spawn = require('child_process').spawn;
var runSequence = require('run-sequence');
var chalk = require('chalk');
var rimraf = require('gulp-rimraf');
var ignore = require('gulp-ignore');
var rename = require("gulp-rename");
var sass = require('gulp-sass');

var paths = {
  libs: [
    'node_modules/lodash/lodash.js',
    'node_modules/moment/moment.js',
    'node_modules/jsbarcode/dist/JsBarcode.all.js',
    'node_modules/swiper/dist/js/swiper.js',
    'node_modules/geolib/dist/geolib.js',
  ],
  pureJsInjector: [
    'libs/*.js',
    'libs/**/*.js',
  ],
  assetFolder: 'src/public/**/*',
};

/**
 *  Web tasks
 */
gulp.task('templates', function () {
  gulp.src('src/**/*.html').pipe(templatesWrap({
    baseDir: 'src',
    templateIdDelimiter: '/',
    templatesModulePath: 'shared/templates.ts',
  }));
});

gulp.task('watch-templates', ['templates'], function () {
  gulp.watch('src/**/*.html', ['templates']);
});

gulp.task('js:lib', function () {
  return gulp.src(paths.libs.concat(paths.pureJsInjector))
    .pipe(concat('global.js'))
    .pipe(gulpif(argv.production, stripDebug()))
    .pipe(gulpif(argv.production, uglify()))
    .pipe(gulp.dest('www/build'));
});

gulp.task('js:pureJsInjector', function () {
  return gulp.src(paths.pureJsInjector)
    .pipe(concat('global.js'))
    .pipe(gulpif(argv.production, stripDebug()))
    .pipe(gulpif(argv.production, uglify()))
    .pipe(gulp.dest('dist'));
});


/**
 *  Bundler tasks
 */
gulp.task('dev', function (cb) {
  var brunch_process = spawn('node_modules/.bin/brunch', ['watch', '--server', '-P', '3336']);

  brunch_process.stdout.on('data', (data) => {
    console.log(chalk.green(`${data}`));
  });

  brunch_process.stderr.on('data', (data) => {
    console.log(chalk.red(`${data}`));
  });

  brunch_process.on('close', (code) => {
    console.log(`brunch process exited with code ${code}`);
    cb();
  });
});

gulp.task('dev:build', function (cb) {
  var brunch_process = spawn('node_modules/.bin/brunch', ['build']);

  brunch_process.stdout.on('data', (data) => {
    console.log(chalk.green(`${data}`));
  });

  brunch_process.stderr.on('data', (data) => {
    console.log(chalk.red(`${data}`));
  });

  brunch_process.on('close', (code) => {
    console.log(`brunch process exited with code ${code}`);
    cb();
  });
});

gulp.task('aot', function (cb) {
  var brunch_process = spawn('node_modules/.bin/ngc', ['-p', 'tsconfig-aot.json']);

  brunch_process.stdout.on('data', (data) => {
    console.log(chalk.green(`${data}`));
  });

  brunch_process.stderr.on('data', (data) => {
    console.log(chalk.red(`${data}`));
  });

  brunch_process.on('close', (code) => {
    console.log(`aot process exited with code ${code}`);
    cb();
  });
});

gulp.task('aot-clean', function () {
  return gulp.src('src/**/*.js', {read: false})
  // .pipe(ignore('**/main.prod.js'))
    .pipe(rimraf())
});

gulp.task('tree-shaking', function (cb) {
  var brunch_process = spawn('node_modules/.bin/rollup', ['-c', 'rollup-config.js']);

  brunch_process.stdout.on('data', (data) => {
    console.log(chalk.green(`${data}`));
  });

  brunch_process.stderr.on('data', (data) => {
    console.log(chalk.red(`${data}`));
  });

  brunch_process.on('close', (code) => {
    console.log(`tree-shaking process exited with code ${code}`);
    cb();
  });
});

gulp.task('sass', function () {
  return gulp.src('src/theme/global.scss')
    .pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(rename('main.css'))
    .pipe(gulp.dest('dist'));
});


/**
 *  Asset tasks
 */
// Preview
gulp.task('copy-preview-assets', function () {
  return gulp.src(paths.assetFolder)
    .pipe(gulp.dest('dist/preview'));
});

gulp.task('copy-preview-index', function () {
  return gulp.src('build-assets/preview-index.html')
    .pipe(rename("index.html"))
    .pipe(gulp.dest('dist/preview'));
});

// Mobile
gulp.task('copy-mobile-assets', function () {
  return gulp.src(paths.assetFolder)
    .pipe(gulp.dest('dist/mobile'));
});

gulp.task('copy-mobile-index', function () {
  return gulp.src('build-assets/mobile-index.html')
    .pipe(rename("index.html"))
    .pipe(gulp.dest('dist/mobile'));
});

gulp.task('copy-preset-data', function () {
  return gulp.src('src/preset-data/**')
    .pipe(gulp.dest('dist/preset-data'));
});

gulp.task('copy-app', function (cb) {
  var app = [
    'dist/global.js',
    'dist/build.js',
    'dist/main.css'
  ];

  return gulp.src(app)
    .pipe(gulp.dest('dist/mobile/build'))
    .pipe(gulp.dest('dist/preview/build'))
    .on('end', () => {
      gulp.src(app)
        .pipe(rimraf())
        .on('end', cb);
    });
});


/**
 *  Util tasks
 */
gulp.task('build-clean', function (cb) {
  return gulp.src('www')
    .pipe(rimraf());
});

gulp.task('build-setup', function (cb) {
  return gulp.src('dist/mobile/**')
    .pipe(gulp.dest('www'));
});

gulp.task('build-android', function (cb) {
  var brunch_process = spawn('ionic', ['build', 'android']);

  brunch_process.stdout.on('data', (data) => {
    console.log(chalk.green(`${data}`));
  });

  brunch_process.stderr.on('data', (data) => {
    console.log(chalk.red(`${data}`));
  });

  brunch_process.on('close', (code) => {
    console.log(`build-android process exited with code ${code}`);
    cb();
  });
});

gulp.task('run-android', function (cb) {
  var brunch_process = spawn('ionic', ['run', 'android']);

  brunch_process.stdout.on('data', (data) => {
    console.log(chalk.green(`${data}`));
  });

  brunch_process.stderr.on('data', (data) => {
    console.log(chalk.red(`${data}`));
  });

  brunch_process.on('close', (code) => {
    console.log(`run-android process exited with code ${code}`);
    cb();
  });
});

gulp.task('livereload-android', function (cb) {
  var brunch_process = spawn('ionic', ['run', 'android', '-l']);

  brunch_process.stdout.on('data', (data) => {
    console.log(chalk.green(`${data}`));
  });

  brunch_process.stderr.on('data', (data) => {
    console.log(chalk.red(`${data}`));
  });

  brunch_process.on('close', (code) => {
    console.log(`livereload-android process exited with code ${code}`);
    cb();
  });
});

gulp.task('build-ios', function (cb) {
  var brunch_process = spawn('ionic', ['build', 'ios']);

  brunch_process.stdout.on('data', (data) => {
    console.log(chalk.green(`${data}`));
  });

  brunch_process.stderr.on('data', (data) => {
    console.log(chalk.red(`${data}`));
  });

  brunch_process.on('close', (code) => {
    console.log(`build-ios process exited with code ${code}`);
    cb();
  });
});

gulp.task('run-ios', function (cb) {
  var brunch_process = spawn('ionic', ['run', 'ios']);

  brunch_process.stdout.on('data', (data) => {
    console.log(chalk.green(`${data}`));
  });

  brunch_process.stderr.on('data', (data) => {
    console.log(chalk.red(`${data}`));
  });

  brunch_process.on('close', (code) => {
    console.log(`run-ios process exited with code ${code}`);
    cb();
  });
});


/**
 *  실행 tasks
 */
gulp.task('serve', function () {
  gulp.src('www')
    .pipe(webserver({
      host: '0.0.0.0',
      livereload: true,
      port: 8000
    }));
});

gulp.task('mobile', function () {
  gulp.src('dist/mobile')
    .pipe(webserver({
      host: '0.0.0.0',
      livereload: true,
      port: 8001
    }));
});

gulp.task('preview', function () {
  gulp.src('dist/preview')
    .pipe(webserver({

      host: '0.0.0.0',
      livereload: true,
      port: 8002
    }));
});

// 개발 task
gulp.task('default', function () {
  runSequence(['watch-templates', 'js:lib'], 'dev');
});

// 개발 task
gulp.task('build', function () {
  runSequence(['watch-templates', 'js:lib'], 'dev:build');
});

gulp.task('android', function () {

  // TODO: create release arg
  if (!argv.l) {
    console.log(chalk.green("running release"));
    runSequence('release', 'build-clean', 'build-setup', 'run-android');
  }
  else {
    console.log(chalk.green("Running live-reload"));
    runSequence('default', 'livereload-android');
  }
});

gulp.task('ios', function () {
  runSequence('release', 'build-clean', 'build-setup', 'build-ios');
});


// 출시 task
gulp.task('release', function () {
  runSequence('templates', 'aot', 'tree-shaking', ['sass', 'copy-preview-assets', 'copy-mobile-assets', 'js:pureJsInjector'], ['copy-preset-data', 'copy-preview-index', 'copy-mobile-index', 'copy-app', 'aot-clean']);
});
