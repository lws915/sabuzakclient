/**
 * Created by andy on 3/29/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & andy - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by andy <>, 3/29/17
 *
 */

;var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

(function (f) {
  if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === "object" && typeof module !== "undefined") {
    module.exports = f();
  } else if (typeof define === "function" && define.amd) {
    define([], f);
  } else {
    var g;
    if (typeof window !== "undefined") {
      g = window;
    } else if (typeof global !== "undefined") {
      g = global;
    } else if (typeof self !== "undefined") {
      g = self;
    } else {
      g = this;
    }
    g.Logger = f.Instance;
  }
})(function () {
  function LogService() {
    this.level = 3;
    this.domains = [];
    this.LOG_LEVEL_ERROR = 1;
    this.LOG_LEVEL_WARN = 2;
    this.LOG_LEVEL_INFO = 3;
    this.LOG_LEVEL_VERBOSE = 4;
    this.LOG_LEVEL_DEBUG = 5;
    this.LOG_LEVEL_SILLY = 6;
  }


  Object.defineProperty(LogService, "Instance", {
    get: function () {
      // Do you need arguments? Make it a regular method instead.
      return this._instance || (this._instance = new this());
    },
    enumerable: true,
    configurable: true
  });


  LogService.prototype.setLevel = function (level) {
    this.level = level;
  };


  LogService.prototype.error = function (domain) {
    if (this.level < this.LOG_LEVEL_ERROR)
      return;
    if (arguments && arguments.length > 1) {
      var args = [];
      args.push("[" + domain + "]");
      for (var i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
      }
      console.error.apply(null, args);
    }
  };


  LogService.prototype.warn = function (domain) {
    if (this.level < this.LOG_LEVEL_WARN)
      return;
    if (!domain)
      return;
    if (this.domains.indexOf(domain) == -1)
      return;
    if (arguments && arguments.length > 1) {
      var args = [];
      args.push("[" + domain + "]");
      for (var i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
      }
      console.warn.apply(null, args);
    }
  };
  LogService.prototype.info = function (domain) {
    if (this.level < this.LOG_LEVEL_INFO)
      return;
    if (!domain)
      return;
    if (this.domains.indexOf(domain) == -1)
      return;
    if (arguments && arguments.length > 1) {
      var args = [];
      args.push("[" + domain + "]");
      for (var i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
      }
      console.log.apply(null, args);
    }
  };
  LogService.prototype.verbose = function (domain) {
    if (this.level < this.LOG_LEVEL_VERBOSE)
      return;
    if (!domain)
      return;
    if (this.domains.indexOf(domain) == -1)
      return;
    if (arguments && arguments.length > 1) {
      var args = [];
      args.push("[" + domain + "]");
      for (var i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
      }
      console.log.apply(null, args);
    }
  };
  LogService.prototype.debug = function (domain) {
    if (this.level < this.LOG_LEVEL_DEBUG)
      return;
    if (!domain)
      return;
    if (this.domains.indexOf(domain) == -1)
      return;
    if (arguments && arguments.length > 1) {
      var args = [];
      args.push("[" + domain + "]");
      for (var i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
      }
      console.log.apply(null, args);
    }
  };
  LogService.prototype.silly = function (domain) {
    if (this.level < this.LOG_LEVEL_SILLY)
      return;
    if (!domain)
      return;
    if (this.domains.indexOf(domain) == -1)
      return;
    if (arguments && arguments.length > 1) {
      var args = [];
      args.push("[" + domain + "]");
      for (var i = 1; i < arguments.length; i++) {
        args.push(arguments[i]);
      }
      console.log.apply(null, args);
    }
  };
  return LogService;
}());
