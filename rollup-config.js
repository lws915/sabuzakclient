import rollup      from 'rollup'
import nodeResolve from 'rollup-plugin-node-resolve'
import commonjs    from 'rollup-plugin-commonjs';
import uglify      from 'rollup-plugin-uglify'
import inject from 'rollup-plugin-inject';

const path = require('path');

export default {
  entry: 'src/app/main.prod.js',
  dest: 'dist/build.js', // output a single application bundle
  sourceMap: false,
  format: 'iife',
  onwarn: function (message) {
    if (/The 'this' keyword is equivalent to 'undefined' at the top level of an ES module, and has been rewritten./.test(message)) {
      return;
    } else if (/Conflicting namespaces/.test(message)) {
      return;
    } else if (/is not defined by/.test(message)) {
      return;
    }
    console.error(message);
  },
  plugins: [
    nodeResolve({jsnext: true, module: true}),
    commonjs({
      include: ['node_modules/**', 'libs/**'],
      namedExports: {
        'node_modules/ng2-localstorage/index.js': ['LocalStorage'],
        'node_modules/angular2-perfect-scrollbar/dist/index.js': ['PerfectScrollbarModule']
      }
    }),
    inject({
      include: '**/*.js',
      exclude: 'node_modules/**',
      modules: {
        _: 'lodash',
        moment: 'moment',
        Swiper: 'swiper',
        JsBarcode: 'jsbarcode',
        Proxy: 'proxy-polyfill',
        geolib: 'geolib'
      }
    }),
    uglify()
  ]
}
