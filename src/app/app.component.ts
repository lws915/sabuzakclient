import {NameCardMessageService} from "../shared/services/name-card-message.service";
/**
 * Created by andy on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

// Browser lib
declare var moment;
declare var Promise;
// Angular
import {Component, OnInit, NgZone} from "@angular/core";
import {Response} from "@angular/http";

// Angular third party lib
import {Platform, MenuController, IonicApp, ModalController, Events, AlertController} from "ionic-angular";
import {Splashscreen} from "ionic-native";
import {Minimize} from "../native/minimize";
import {HttpInterceptorService, getHttpOptionsAndIdx, getHttpHeadersOrInit} from "../lib/ng-http-interceptor";

// Appzet Source
import {PushService} from "../shared/services/push.service";
import {SpinnerService} from "../shared/services/spinner.service";
import {getTemplate} from "../shared/templates";
import {AppService} from "../shared/services/app.service";
import {DeviceService} from "../shared/services/device.service";
import {Login} from "../pages/login/login";
import {LocalStorage} from "ng2-localstorage";
import {AuthService} from "../shared/services/auth.service";
import {MdDialogRef, MdDialogConfig, MdDialog} from "@angular/material";

// dev test
import {EmailSns} from '../pages/login/email-sns/email-sns';
import {Register} from '../pages/login/register/register';
import {NumberCheck} from '../pages/login/number-check/number-check';
import {Profile} from '../pages/profile/profile';
import {VideoRegister} from '../pages/profile/video-register/video-register';
import {Sabuzak} from '../pages/sabuzak/sabuzak';

// Export lib
declare var window;
declare var device;
declare var cordova;
declare var _;


@Component({
  template: getTemplate('app/app.component')
})

export class MyApp implements OnInit {
  @LocalStorage("sabuzak_cred") credential;

  loginPage = Login;
  // loginPage = Profile;
  // loginPage = Sabuzak;

  isDevice:any;
  isAndroid:any;
  isIos:any;
  isCordova:any;
  isMobileWeb:any;

  rootPage;

  constructor(private httpInterceptor: HttpInterceptorService,
              private platform: Platform,
              private menuCtrl: MenuController,
              private pushService: PushService,
              private alertCtrl: AlertController,
              private appService: AppService,
              private deviceService: DeviceService,
              private authService: AuthService,
              private nameCardMessageService: NameCardMessageService,
              public events: Events,
              public modalCtrl: ModalController,
              public dialog: MdDialog,
              private ngZone: NgZone,
              private ionicApp: IonicApp) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    this.initializeApp();

    this.events.subscribe('auth', () => {
      this.checkRoot();
    });

    this.checkRoot();

    window.handleOpenURL = (url) => {
      if (this.credential && url.indexOf("?") > -1)
        this.openNewModal(url);
    };

    // Dev
    // this.openNewModal(window.location.href);
  }


  checkRoot() {
    if (this.credential && this.credential.token) {
      this.rootPage = this.loginPage;
    } else {
      this.rootPage = this.loginPage;
    }
  }


  /*****************************
   *        util functions
   *****************************/


  initializeApp() {
    this.platform.ready().then(
      () => {

        if (this.platform.is('cordova'))
          this.authService.db = window.sqlitePlugin.openDatabase({name: 'token.db', location: 'default'});

        // 스플래쉬 스크린 숨기기
        Splashscreen.hide();

        // 나중에 로딩 길때 넣어주는 기본 스피너
        SpinnerService.hide();

        // 상태바 설정

        // 뒤로가기 설정
        this.platform.registerBackButtonAction(() => {

          let overlayPortal = this.ionicApp._overlayPortal.getActive();
          let modalPortal = this.ionicApp._modalPortal.getActive();

          if (overlayPortal) {
            overlayPortal.dismiss();
          } else if (this.menuCtrl.isOpen()) {
            this.menuCtrl.close();
          } else {
            Minimize.home(() => {
            });
          }
        }, 100);

        // // 권한 설정 및 기기 설정
        this.resolvePermissions()
          .then(this.resolveDeviceId.bind(this))
          .then(this.initializeClient.bind(this));

        // 강제 권한 체크 이벤트 등록
        document.addEventListener("resume", () => {
          // 권한 설정 및 기기 설정
          this.resolvePermissions()
            .then(this.resolveDeviceId.bind(this))
            .then(this.initializeClient.bind(this));
        }, false);


      }
    );
    this.initializeInterceptor();

    if (this.credential && this.credential.token) {
      document.addEventListener("resume", () => {
        this.ngZone.run(() => {
          this.findProcessingMessage();
        });
      }, false);
    }
  }

  findProcessingMessage() {

    let createdAt = moment().hours(moment().hours() - 2);

    this.nameCardMessageService.findMine({
      query: {
        type: "location",
        status: 'processing',
        createdAt: {$lt: createdAt}
      },
      sort: {createdAt: -1},
      limit: 1,
      populate: [{path: 'nameCard'}]
    })
      .subscribe(
        (data) => {
          if (data.nameCardMessages && data.nameCardMessages.length > 0) {
            this.openDoneConfirm(data.nameCardMessages[0]);
          }
        },
        (err) => {
        });
  }

  config: MdDialogConfig = new MdDialogConfig();

  openDoneConfirm(nameCardMessage) {

    this.config.disableClose = false;
    // 905
    // 1005
    this.config['width'] = '83vw';
    this.config['height'] = '93vw';

  }


  resolvePermissions() {

    this.isDevice = this.platform.is('android') || this.platform.is('ios');
    this.isAndroid = this.platform.is('android');
    this.isIos = this.platform.is('ios');
    this.isCordova = this.platform.is('cordova');
    // this.isMobileWeb = this.platform.is('mobileweb') || this.platform.is('core');

    return new Promise((resolve, reject) => {
      if (this.platform.is('cordova') && this.platform.is('android')) {

        let version = device.version;
        let major = version.split('.')[0];
        major = parseInt(major);

        if (major > 5) {
          cordova.plugins.diagnostic.requestRuntimePermissions(function (status) {
              if (status == cordova.plugins.diagnostic.permissionStatus.GRANTED) return resolve();
              else return reject();

            }, function (err) {
            },
            [cordova.plugins.diagnostic.permission.READ_PHONE_STATE,
              cordova.plugins.diagnostic.permission.WRITE_EXTERNAL_STORAGE,
              cordova.plugins.diagnostic.permission.SEND_SMS]);
        } else resolve();
      } else resolve();
    });
  }

  resolveDeviceId() {
    return new Promise((resolve, reject) => {
      if (this.platform.is('cordova'))
        window.plugins.uniqueDeviceID.get((uuid) => {
            this.appService.deviceUUID = uuid;
            resolve();
          },
          (err) => {
            reject();
          });
      else resolve();
    });
  }


  /*****************************
   *       helper functions
   *****************************/

  initializeInterceptor() {
    // global request interceptor
    this.httpInterceptor.request().addInterceptor((req, method) => {

      let requestOption = getHttpOptionsAndIdx(req, method);
      requestOption.options.withCredentials = true;
      req[requestOption.idx] = requestOption.options;

      // Setting header?
      let requestHeaders = getHttpHeadersOrInit(req, method);
      if (this.credential && this.credential.token)
        requestHeaders.set('Authorization', 'Bearer ' + this.credential.token);

      return req;
    });

    // global response interceptor
    this.httpInterceptor.response().addInterceptor(
      (res, method) => {
        return res
          .map(res => {
            if (res.status == 403 || res.status == 401)
              this.logOut();

            let temp: Response = res.json();
            return temp;
          })
          .catch((err) => {
            throw err;
          });
      }
    );
  }


  // This include registration
  initializeClient() {
    this.pushService.register();
    this.deviceService.myDevice()
      .subscribe(
        (deviceWrapper) => {
          // this.appService.deviceIndex = deviceWrapper.user.devices[0]._id;
        }
      )
  }


  wrongCard() {
    let alert = this.alertCtrl.create({
      title: '알림',
      subTitle: "존재하지 않는 명함 입니다.",
      buttons: ['확인']
    });

    alert.present();
  }

  openNewModal(url) {

    let search, _id, param, key, value;
    if (!url) {
      this.wrongCard();
      return;
    }

    search = url.split("?")[1];

    if (!search)
      return this.wrongCard();


    let params = search.split("&");

    if (!params || params.length < 1)
      return this.wrongCard();

    _.forEach(params, (value) => {
      param = value.split("=");
      key = param[0];
      value = param[1];
      if (key == "_id")
        _id = value;
    });

    if (!_id)
      return this.wrongCard();
  }


  logOut() {
    this.credential = null;
    this.events.publish('auth');
  }

}
