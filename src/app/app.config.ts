/**
 * Created by Yoon Yong (Andy) Shin on 23/12/2016
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Yoon Yong (Andy) Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoon Yong (Andy) Shin <andy.shin@applicat.co.kr>, 23/12/2016
 *
 */

// Dev
// let serverUrl = "http://192.168.1.2:3000";
// let webUrl = "http://192.168.1.2:3337";

let serverUrl = "http://localhost:3000";
let webUrl = "http://localhost:3337";

// let serverUrl = "http://172.30.1.42:3000";
// let webUrl = "http://172.30.1.42:3337";

// let serverUrl = "http://baton-env.ap-northeast-2.elasticbeanstalk.com";
// let webUrl = "http://www.batonup.com.s3-website.ap-northeast-2.amazonaws.com";

export const config = {
  serverUrl: serverUrl,
  webUrl: webUrl
};
