import "./rxjs-extensions";
// Angular
import {NgModule, ElementRef} from "@angular/core";
import {HttpModule} from "@angular/http";
import {MaterialRootModule, 
        MdIconRegistry, 
        MdUniqueSelectionDispatcher, 
        MdDialog} from "@angular/material";
// Angular third party lib
import {IonicApp, 
        IonicModule, 
        Content} from "ionic-angular";
import {AgmCoreModule} from "angular2-google-maps/core";
import {HttpInterceptorModule} from "../lib/ng-http-interceptor";
import {PerfectScrollbarModule, 
        PerfectScrollbarConfigInterface} from "angular2-perfect-scrollbar";
// Appzet Source
import {MyApp} from "./app.component";


// shared
import {PushService} from "../shared/services/push.service";
import {AppService} from "../shared/services/app.service";
import {DeviceService} from "../shared/services/device.service";
import {AuthService} from "../shared/services/auth.service";
import {NameCardService} from "../shared/services/name-card.service";
import {FileService} from "../shared/services/file.service";
import {NameCardMessageService} from "../shared/services/name-card-message.service";
import {BannerService} from "../shared/services/banner.service";
import {NameCardWalletService} from "../shared/services/name-card-wallet.service";
import {UserService} from "../shared/services/user.service";
import {NoticeService} from "../shared/services/notice.service";
import {BallService} from "../shared/services/ball.service";
import {RankService} from "../shared/services/rank.service";
import {LikeService} from "../shared/services/like.service";
import {PostService} from "../theme/shared/post.service";
import {MobilePipe} from "../shared/pipes/mobile.pipe";
import {PhonePipe} from "../shared/pipes/phone.pipe";
import {SafePipe} from "../shared/pipes/safe.pipe";

// pages
import {Login} from "../pages/login/login";
import {Agreement} from "../pages/login/agreement/agreement";
import {NumberCheck} from "../pages/login/number-check/number-check";
import {NumberSent} from "../pages/login/number-sent/number-sent";
import {Register} from "../pages/login/register/register";
import {PasswordReset} from "../pages/login/password-reset/password-reset";
import {EmailSns} from '../pages/login/email-sns/email-sns';
import {Profile} from '../pages/profile/profile';
import {VideoRegister} from '../pages/profile/video-register/video-register';
import {Sabuzak} from '../pages/sabuzak/sabuzak';
import {Main} from '../pages/sabuzak/main/main';
import {ViewProfile} from '../pages/sabuzak/view-profile/view-profile';
import {ChatList} from '../pages/sabuzak/chat-list/chat-list';
import {NoticeList} from '../pages/sabuzak/notice-list/notice-list';
import {ChatRoom} from '../pages/sabuzak/chat-room/chat-room';
import {SelectList} from '../pages/sabuzak/select-list/select-list';
import {SelectDetail} from '../pages/sabuzak/select-detail/select-detail';
import {SearchLiker} from '../pages/sabuzak/search-liker/search-liker';
import {DreamConfig} from '../pages/sabuzak/dream-config/dream-config';
import {RechargeBall} from '../pages/sabuzak/recharge-ball/recharge-ball';
import {Usage} from '../pages/sabuzak/usage/usage';
import {Settings} from '../pages/sabuzak/settings/settings';
import {PasswordChange} from '../pages/sabuzak/password-change/password-change';
import {Notice} from '../pages/sabuzak/notice/notice';
import {SocialGender} from '../pages/login/social-gender/social-gender';



const PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    IonicModule.forRoot(MyApp, {
      backButtonText: ' '
    }),
    HttpModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDAtrNFyRqdUc7PtARqe_tnpM2s4RPlEZ8'
    }),
    HttpInterceptorModule.noOverrideHttp(),
    MaterialRootModule,
    PerfectScrollbarModule.forRoot(PERFECT_SCROLLBAR_CONFIG),
  ],
  declarations: [
    MyApp,
    Agreement,
    NumberSent,
    PhonePipe,
    MobilePipe,
    SafePipe,
    PasswordReset,
    
    Login,
    Register,
    NumberCheck,
    EmailSns,
    SocialGender,

    Profile,
    VideoRegister,

    Sabuzak,
    Main,
    ViewProfile,
    ChatList,
    NoticeList,
    ChatRoom,
    SelectList,
    SelectDetail,
    SearchLiker,
    DreamConfig,
    RechargeBall,
    Usage,
    Settings,
    PasswordChange,
    Notice
    
  ],
  bootstrap: [IonicApp],
  providers: [
    MdIconRegistry,
    MdUniqueSelectionDispatcher,
    MdDialog,
    PushService,
    AppService,
    DeviceService,
    AuthService,
    NameCardService,
    FileService,
    NameCardMessageService,
    PostService,
    BannerService,
    NameCardWalletService,
    UserService,
    NoticeService,
    BallService,
    RankService,
    LikeService
  ],
  entryComponents: [
    PasswordReset,
    MyApp,
    Agreement,
    NumberSent,
    
    Login,
    Register,
    EmailSns,
    NumberCheck,
    SocialGender,
    
    Profile,
    VideoRegister,

    Sabuzak,
    Main,
    ViewProfile,
    ChatList,
    NoticeList,
    ChatRoom,
    SelectList,
    SelectDetail,
    SearchLiker,
    DreamConfig,
    RechargeBall,
    Usage,
    Settings,
    PasswordChange,
    Notice
  ]
})

export class AppModule {
}
