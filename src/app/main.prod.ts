/**
 * Created by andy on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

import 'core-js/client/shim';
import 'es6-promise';
import 'zone.js/dist/zone';
import { platformBrowser } from '@angular/platform-browser';
import { AppModuleNgFactory } from '../../.aot/src/app/app.module.ngfactory';
import {SpinnerService} from "../shared/services/spinner.service";

// Spinner, Platform
SpinnerService.init("crescent", "ios");

platformBrowser().bootstrapModuleFactory(AppModuleNgFactory);
