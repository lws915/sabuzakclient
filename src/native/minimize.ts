import { Cordova, Plugin } from 'ionic-native';

/**
 * @name Splashscreen
 * @description This plugin displays and hides a splash screen during application launch. The methods below allows showing and hiding the splashscreen after the app has loaded.
 * @usage
 * ```typescript
 * import { Splashscreen } from 'ionic-native';
 *
 *
 * Splashscreen.show();
 *
 * Splashscreen.hide();
 * ```
 */
@Plugin({
  pluginName: 'cordova-plugin-android-home',
  plugin: 'cordova-plugin-android-home',
  pluginRef: 'navigator.home',
  repo: 'https://github.com/ZhichengChen/cordova-plugin-android-home'
})
export class Minimize {

  /**
   * Minimize the app
   */
  @Cordova({
    callbackOrder: 'reverse'
  })
  static home(success, fail?): void {
  }
}
