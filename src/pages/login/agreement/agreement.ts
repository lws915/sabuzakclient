/**
 * Created by wonseok Lee on 25/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 * Updater    수정자 - wonseok Lee 25/08/2017
 */
// Export lib
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {NumberCheck} from "../number-check/number-check";


@Component({
  template: getTemplate('pages/login/agreement/agreement')
})

export class Agreement implements OnInit, OnDestroy {

  servicePolicy;
  locationPolicy;
  numberPolicy;

  constructor(private nav: NavController) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {

    //Dev
    // this.toCellPhone();
  }

  ngOnDestroy() {
  }


  /*****************************
   *        util functions
   *****************************/

  goBack() {
    this.nav.pop();
  }

  allChecked() {
    if (!this.servicePolicy) return false;
    if (!this.locationPolicy) return false;
    if (!this.numberPolicy) return false;
    return true;
  }

  checkAll() {
    if (this.allChecked()) {
      this.servicePolicy = false;
      this.locationPolicy = false;
      this.numberPolicy = false;
    } else {
      this.servicePolicy = true;
      this.locationPolicy = true;
      this.numberPolicy = true;
    }
  }

  toCellPhone() {
    this.nav.push(NumberCheck);
  }

  toOnlineRead() {
    window.open("http://www.mybilling.co.kr/etc/etc_02.php", "_blank");
  }

}
