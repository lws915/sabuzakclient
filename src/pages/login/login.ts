/**
 * Created by wonseok Lee on 25/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 * Updater    수정자 - wonseok Lee 25/08/2017
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {App, 
        ViewController, 
        Platform, 
        NavController, 
        AlertController, 
        Events} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {Agreement} from "./agreement/agreement";
import {NumberCheck} from "./number-check/number-check";
import {Register} from "./register/register";
import {AuthService} from "../../shared/services/auth.service";
import {PasswordReset} from "./password-reset/password-reset";
import {EmailSns} from './email-sns/email-sns';
import {SocialGender} from './social-gender/social-gender';
import {Sabuzak} from '../sabuzak/sabuzak';


@Component({
  template: getTemplate('pages/login/login')
})

export class Login implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  selectedCategory;

  card;

  viewSize;

  identifier;
  password;

  loading;


  constructor(public platform: Platform,
              private nav: NavController,
              public appCtrl: App,
              public alertCtrl: AlertController,
              public events: Events,
              private authService: AuthService) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    
  }

  ngOnDestroy() {
  }


  toFindPassword() {
    this.nav.push(PasswordReset);
  }

  toRegister() {
    this.nav.push(EmailSns);
  }


  /*****************************
   *        util functions
   *****************************/
  regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

  isValid() {
    
    if (!this.identifier) return false;
    if (!this.regExp.test(this.identifier)) return false;
    if (!this.password) return false;
    if (this.password.length < 6) return false;

    return true;
  }

  writeToken(token) {
    this.authService.db.transaction((tx) => {

      tx.executeSql('CREATE TABLE IF NOT EXISTS TokenTable (token)');

      tx.executeSql('INSERT INTO TokenTable VALUES (?)', [token]);

    }, (error) => {
      console.log('Transaction ERROR: ' + error.message);
    }, () => {
      this.authService.db.executeSql('SELECT * FROM TokenTable', [], function (resultSet) {
        console.log('Sample column value: ', resultSet);
        _.times(resultSet.rows.length, (n) => {
          console.log(n);
          console.log('Sample column value: ', resultSet.rows.item(n).token);
        })
      });
    });
  }

  login() {
    this.loading = true;
    this.events.publish('auth');
    
    this.authService
      .login(this.identifier, this.password)
      .subscribe(
        (data) => {
          this.loading = false;
          localStorage.setItem('angular2ws_sabuzak_cred',JSON.stringify(data));
          
          if (this.platform.is('cordova'))
            this.writeToken(data.token);

          this.events.publish('auth');
          // this.nav.setRoot(Sabuzak);
          this.appCtrl.getRootNav().setRoot(Sabuzak);
        },
        (err) => {
          this.loading = false;
          let subTitle = "서버 응답이 없습니다.";

          switch (err.status) {
            case 401:
              subTitle = "비밀번호가 틀렸습니다.";
              break;
            case 400:
              subTitle = "잘못된 요청 입니다.";
              break;
          }

          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: subTitle,
            buttons: ['확인']
          });

          alert.present();
        });
  }

  socialLogin(){
    this.authService
        .socialLogin()
        .subscribe(
          data=>{
            console.log(data);
          }
        )
      }

  goSocial(){
    this.nav.push(SocialGender);
  }

}
