/**
 * Created by wonseok Lee on 25/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 * Updater    수정자 - wonseok Lee 25/08/2017
 */
// Export lib
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {NavController, 
        AlertController, 
        Platform} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {MdDialogRef, 
        MdDialogConfig, 
        MdDialog} from "@angular/material";
import {NumberSent} from "../number-sent/number-sent";
import {AuthService} from "../../../shared/services/auth.service";
import {Register} from "../register/register";
import {Profile} from "../../profile/profile";


@Component({
  template: getTemplate('pages/login/number-check/number-check')
})

export class NumberCheck implements OnInit, OnDestroy {

  viewSize;

  mobile;
  sent = true;
  checking = false;

  code;
  activated = false;

  dialogReleaseRef: MdDialogRef<NumberSent>;
  config: MdDialogConfig = new MdDialogConfig();


  testToggle = false;

  constructor(private nav: NavController,
              private authService: AuthService,
              public dialog: MdDialog,
              public platform: Platform,
              public alertCtrl: AlertController) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    this.viewSize = {
      width: this.platform.width(),
      height: this.platform.height()
    };

    //Dev
    // this.nav.push(Register, {mobile: "01049944159"});
  }

  ngOnDestroy() {
  }


  /*****************************
   *        util functions
   *****************************/

  reset() {
    this.activated = false;
    this.sent = false;
  }

  openSentModal() {
    this.config.disableClose = false;
    // 905
    // 1005
    this.config['width'] = '83vw';
    this.config['height'] = '93vw';

    this.dialogReleaseRef = this.dialog.open(NumberSent, this.config);
    this.dialogReleaseRef.componentInstance.number = this.mobile;

    this.dialogReleaseRef.afterClosed().subscribe(result => {
      this.dialogReleaseRef = null;
    });
  }

  sendActivationNumber() {
    this.sent = true;
    this.authService.checkMobileNumber(this.mobile)
      .subscribe(
        () => {
          this.openSentModal();
        },
        (err) => {
          this.sent = false;
          let subTitle = "서버 응답이 없습니다.";

          switch (err.status) {
            case 400:
              subTitle = "잘못된 전화번호 입니다.";
              break;
            case 422:
              subTitle = "모바일 인증은 1분에 최대 1번 요청 가능 합니다.";
              break;
            case 410:
              subTitle = "이미 가입한 모바일 번호 입니다.";
              break;
          }

          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: subTitle,
            buttons: ['확인']
          });

          alert.present();
        });
  }

  checkActivationNumber() {
    this.checking = true;
    this.authService.completeMobileNumber(this.code)
      .subscribe(
        () => {
          this.checking = false;
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: "인증 되었습니다.",
            buttons: ['확인']
          });

          alert.present();
          this.activated = true;
        },
        (err) => {
          this.checking = false;
          let subTitle = "서버 응답이 없습니다.";

          switch (err.status) {
            case 400:
            case 412:
              subTitle = "잘못된 인증번호 입니다.";
              break;
          }

          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: subTitle,
            buttons: ['확인']
          });

          alert.present();
        });
  }

  testTg(){
    this.testToggle = true;
  }

  testNext(){
    this.nav.push(Profile);
  }

  next() {
    this.nav.push(Register, {mobile: this.mobile});
  }

  goBack() {
    this.nav.popToRoot();
  }
}
