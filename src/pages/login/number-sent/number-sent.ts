/**
 * Created by wonseok Lee on 25/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 * Updater    수정자 - wonseok Lee 25/08/2017
 */

// Export Lib
declare var _;

// Angular
import {Component, 
        HostListener} from "@angular/core";
import {MdDialogRef} from "@angular/material";

// Project Sources
import {getTemplate} from "../../../shared/templates";


@Component({
  selector: 'number-sent',
  template: getTemplate('pages/login/number-sent/number-sent')
})
export class NumberSent {
  contentSize;

  number;

  constructor(public dialogRef: MdDialogRef<NumberSent>) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    
  }

  /*****************************
   *        util functions
   *****************************/

  dismiss() {
    this.dialogRef.close();
  }

  /*****************************
   *       helper functions
   *****************************/
}
