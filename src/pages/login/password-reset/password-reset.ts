/**
 * Created by wonseok Lee on 25/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 * Updater    수정자 - wonseok Lee 25/08/2017
 */
// Export lib
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {NavController, 
        AlertController, 
        Platform} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {MdDialogRef, 
        MdDialogConfig, 
        MdDialog} from "@angular/material";
import {NumberSent} from "../number-sent/number-sent";
import {AuthService} from "../../../shared/services/auth.service";
import {Register} from "../register/register";


@Component({
  template: getTemplate('pages/login/password-reset/password-reset')
})

export class PasswordReset implements OnInit, OnDestroy {

  mobile;
  sent;
  checking;
  activated;
  code;
  step;
  newPassword;
  passwordConfirm;
  viewSize;
  loading;

  constructor(private nav: NavController,
              private authService: AuthService,
              public dialog: MdDialog,
              public platform: Platform,
              public alertCtrl: AlertController) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {

    this.viewSize = {
      width: this.platform.width(),
      height: this.platform.height()
    };

    this.step = 1;
    this.newPassword = "";
    this.passwordConfirm = "";
    this.reset();
  }

  ngOnDestroy() {
  }


  /*****************************
   *        util functions
   *****************************/

  reset() {
    this.activated = false;
    this.sent = false;
  }

  sendActivationNumber() {
    this.sent = true;
    this.authService.forgotPasswordStartSMS(this.mobile)
      .subscribe(
        () => {
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: "비밀번호 찾기 인증코드가 전송 되었습니다.",
            buttons: ['확인']
          });

          alert.present();
        },
        (err) => {
          this.sent = false;
          let subTitle = "서버 응답이 없습니다.";

          switch (err.status) {
            case 400:
              subTitle = "잘못된 전화번호 입니다.";
              break;
            case 402:
              subTitle = "하루 비밀번호 재설정 요청량이 초과 하였습니다.";
              break;
          }

          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: subTitle,
            buttons: ['확인']
          });

          alert.present();
        });
  }

  checkActivationNumber() {
    this.checking = true;
    this.authService.forgotPasswordCheckSMS(this.mobile, this.code)
      .subscribe(
        () => {
          this.checking = false;
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: "인증 되었습니다.",
            buttons: ['확인']
          });

          alert.present();
          this.activated = true;
        },
        (err) => {
          this.checking = false;
          let subTitle = "서버 응답이 없습니다.";

          switch (err.status) {
            case 400:
              subTitle = "잘못된 요청입니다.";
              break;
            case 422:
              subTitle = "비밀번호 재설정을 요청 한적이 없습니다.";
              break;
            case 412:
              subTitle = "인증번호를 잘못 입력하셧거나, 비밀번호 재설정 시간이 지낫습니다.";
              break;
          }

          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: subTitle,
            buttons: ['확인']
          });

          alert.present();
        });
  }

  stepTwo() {
    this.step = 2;
  }

  isValid() {
    if (this.loading) return false;
    if (!this.newPassword) return false;
    if (this.newPassword.length < 8) return false;
    if (!this.passwordConfirm) return false;

    if (this.newPassword != this.passwordConfirm) return false;

    return true;
  }

  changePasssword() {
    this.loading = true;
    this.authService.forgotPasswordCompleteSMS(this.newPassword)
      .subscribe(
        () => {
          this.loading = false;
          this.stepThree();
        },
        (err) => {
          this.loading = false;
          let subTitle = "서버 응답이 없습니다.";

          switch (err.status) {
            case 400:
              subTitle = "잘못된 요청입니다.";
              break;
            case 412:
              subTitle = "비밀번호 변경을 요청 한적이 없습니다.";
              break;
          }

          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: subTitle,
            buttons: ['확인']
          });

          alert.present();
        });
  }

  stepThree() {
    this.step = 3;
  }

  goBack() {
    this.nav.pop();
  }
}
