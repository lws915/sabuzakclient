/**
 * Created by wonseok Lee on 25/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 * Updater    수정자 - wonseok Lee 25/08/2017
 */

// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {NavController, 
        NavParams, 
        AlertController, 
        Events} from "ionic-angular";
// Appzet Source
import {MdDialogRef, 
        MdDialogConfig, 
        MdDialog} from "@angular/material";
import {AuthService} from "../../../shared/services/auth.service";
import {LocalStorage} from "ng2-localstorage";
import {getTemplate} from "../../../shared/templates";
import {NumberCheck} from "../number-check/number-check"

// Export lib
declare var moment;


@Component({
  template: getTemplate('pages/login/register/register')
})

export class Register implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  EMAIL_REGEX = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i;
  gender;
  joinForm = {
    email: "",
    password: "",
    gender: ""
  };
  passwordChk = "";

  config: MdDialogConfig = new MdDialogConfig();

  constructor(public nav: NavController,
              public dialog: MdDialog,
              public events: Events,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public authService: AuthService) {

  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    this.initValue();
  }

  ngOnDestroy() {

  }

  /*****************************
   *        util functions
   *****************************/
  initValue(){
    this.gender = {
      male: false,
      female: false
    }
  }

  genderOn(input){
    Object.keys(this.gender).map( data => {
      if(data == input){
        this.gender[data] = true;
        this.joinForm.gender = data;
      }else{
        this.gender[data] = false;
      }
    })
  }

  goNext(){
    this.authService
        .checkEmail(this.joinForm.email)
        .subscribe(data=>{
          if(!data.isAvailable){
            this.warningAlert("사용중인 이메일 입니다.");
          }else if(!this.EMAIL_REGEX.test(this.joinForm.email)){
            this.warningAlert("이메일을 확인해주세요.");
          }else if(this.joinForm.password == ""){
            this.warningAlert("비밀번호를 입력해주세요.");
          }else if(this.joinForm.password.length < 6){
            this.warningAlert("비밀번호는 6자리 이상 입력해주세요.")
          }else if(this.joinForm.password !== this.passwordChk){
            this.warningAlert("비밀번호가 일치하지 않습니다.");
          }else if(!(this.gender.male || this.gender.female)){
            this.warningAlert("성별을 선택해주세요.");
          }else{
            localStorage.setItem('angular2ws_sabuzak_cred',JSON.stringify({user:this.joinForm}));
            console.log(this.joinForm);
            console.log(this.credential);
            this.nav.push(NumberCheck);
          }
        })
  }

  warningAlert(input){
    let alert = this.alertCtrl.create({
      title: '알림',
      subTitle: input,
      buttons: ['확인']
    });

    alert.present();
  }

  goBack() {
    this.nav.pop();
  }

}
