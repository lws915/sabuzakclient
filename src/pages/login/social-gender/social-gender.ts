/**
 * Created by wonseok Lee on 25/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 25/08/2017
 *
 * Updater    수정자 - wonseok Lee 25/08/2017
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController, 
        AlertController, 
        Events} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {Register} from "../register/register";


@Component({
  template: getTemplate('pages/login/social-gender/social-gender')
})

export class SocialGender implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  constructor(public platform: Platform,
              private nav: NavController,
              public alertCtrl: AlertController,
              public events: Events) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {

    //dev
    // this.toRegister();
    // this.toFindPassword();
  }

  ngOnDestroy() {
    
  }

  
  
  /*****************************
   *        util functions
   *****************************/

  toRegister() {
    this.nav.push(Register);
  }

  goBack() {
    this.nav.pop();
  }


}
