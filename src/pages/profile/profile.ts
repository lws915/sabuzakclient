/**
 * Created by wonseok Lee on 17/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/17
 *
 * Updater    수정자 - wonseok Lee 17/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy,
        ViewChild,
        NgZone
      } from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController, 
        AlertController, 
        Events,
        Content,
        Select,
        ActionSheetController
      } from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {VideoRegister} from "./video-register/video-register";
import {Sabuzak} from "../sabuzak/sabuzak";
import {AuthService} from '../../shared/services/auth.service';
import {UserService} from '../../shared/services/user.service';

@Component({
  template: getTemplate('pages/profile/profile')
})

export class Profile implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;
  @ViewChild(Content) content:Content;

  topState = false;
  videoState = false;
  ageList = [];
  locationList = [];
  bodyTypeList = [];
  tallList = [];
  faithList = [];
  drinkingList = [];
  smokingList = [];

  actAge: any
  actTall: any
  actBodyType: any
  actLocation: any
  actFaith: any
  actDrinking: any
  actSmoking: any

  joinForm: any


  constructor(public platform: Platform,
              public nav: NavController,
              public alertCtrl: AlertController,
              public events: Events,
              public zone: NgZone,
              public actionSheetCtrl: ActionSheetController,
              public authService: AuthService,
              public userService: UserService
            ) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    this.listInitializer();
  }

  ngOnDestroy() {
  }

  listInitializer(){
    for(let i = 18; i <= 45; i++){
      this.ageList.push(i);
    }
    for(let i = 150; i <= 190; i++){
      this.tallList.push(i);
    }
    if(this.credential.user.gender == 'male'){
      this.bodyTypeList = ['마른','슬림탄탄','보통','건장한','포근']
    }else if(this.credential.user.gender == 'female'){
      this.bodyTypeList = ['마른','슬림탄탄','보통','글래머','포근'];
    }
    this.locationList = [ '서울','경기','인천','대전','충북','충남','강원','부산','경북','대구','울산','광주','전북','전남','제주'];
    this.faithList = ['무교','기독교','천주교','불교','기타'];
    this.drinkingList = ['마시지 않음','가끔 즐김','술자리를 즐기는 편'];
    this.smokingList = ['흡연','비흡연'];
    
    this.actAge = {
      title: "나이",
      buttons: []
    };
    this.actTall = {
      title: "키",
      buttons: []
    };
    this.actBodyType = {
      title: "체형",
      buttons: []
    };
    this.actLocation = {
      title: "지역",
      buttons: []
    };
    this.actFaith = {
      title: "종교",
      buttons: []
    };
    this.actDrinking = {
      title: "음주",
      buttons: []
    };
    this.actSmoking = {
      title: "흡연",
      buttons: []
    };

    if(this.credential.user && this.credential.user._id){
      this.joinForm = this.credential.user;
      console.log(this.joinForm);
    }else{
      this.joinForm = {
        email: this.credential.user.email,
        password: this.credential.user.password,
        introduce: "",
        nickName: "",
        age:"",
        tall:"",
        bodyType:"",
        location:"",
        faith:"",
        drinking:"",
        smoking:""
      };
    }

    this.actionBtnSetter(this.ageList, this.actAge, "age");
    this.actionBtnSetter(this.tallList, this.actTall, "tall");
    this.actionBtnSetter(this.bodyTypeList, this.actBodyType, "bodyType");
    this.actionBtnSetter(this.locationList, this.actLocation, "location");
    this.actionBtnSetter(this.faithList, this.actFaith, "faith");
    this.actionBtnSetter(this.drinkingList, this.actDrinking, "drinking");
    this.actionBtnSetter(this.smokingList, this.actSmoking, "smoking");

  }
  
  /*****************************
   *        util functions
   *****************************/


  actionBtnSetter(actArr, actObj, dataLink){
    actArr.map(data => {
      actObj.buttons.push({
        text: data,
        handler: () => {
          this.joinForm[dataLink] = data;
        }
      })
    })

    actObj.buttons.push({
      text: 'Cancel',
      role: 'cancel',
      handler: () => {}
    })
  }

  isTopChk(){
    if(this.content.scrollTop != 0){
      this.zone.run(()=>{
        this.topState = true;
      })
    }else{
      this.zone.run(()=>{
        this.topState = false;
      })
    }
  }

  openSelect(input){
    let actionSheet = this.actionSheetCtrl.create(input); 
    actionSheet.present();
  }

  focused(input){
    input.path[1].style.borderBottom = "1px solid black";
    input.path[1].style.transitionDuration = "0.4s";
  }

  blured(input){
    input.path[1].style.borderBottom = "1px solid #CCCCCC";
    input.path[1].style.transitionDuration = "0.4s";
  }

  validator(){
    if(!this.joinForm.nickName){
      this.warningAlert("닉네임을 입력해주세요.");
      return false;
    }else if(!this.joinForm.age){
      this.warningAlert("나이를 선택해주세요.");
      return false;
    }else if(!this.joinForm.location){
      this.warningAlert("지역을 선택해주세요.");
      return false;
    }else if(!this.joinForm.bodyType){
      this.warningAlert("체형을 선택해주세요.");
      return false;
    }else if(!this.joinForm.tall){
      this.warningAlert("키를 선택해 주세요.");
      return false;
    }else{
      return true;
    }
  }

  warningAlert(input){
    let alert = this.alertCtrl.create({
      title: '알림',
      subTitle: input,
      buttons: ['확인']
    });

    alert.present();
  }

  // nav control functions
  goVideoReg(input){
    let buttonList;
    if(input == 'new'){
      buttonList = [
        {
          text: '내 라이브러리',
          handler: () => {
            
          }
        },
        {
          text: '카메라',
          handler: () => {
            
          }
        },
        {
          text: '취소',
          role: 'cancel'
        }
      ]
    }else{
      buttonList = [
        {
          text: '대표영상으로 선택',
          handler: () => {

          }
        },
        {
          text: '내 라이브러리',
          handler: () => {
            
          }
        },
        {
          text: '카메라',
          handler: () => {
            
          }
        },
        {
          text: '삭제',
          handler: () => {
            
          }
        },
        {
          text: '취소',
          role: 'cencel'
        }
  
      ]

    }
    this.alertCtrl.create({
      title: '프로필 영상',
    })
    this.nav.push(VideoRegister);
  }

  goMain(){
    console.log(this.validator() && this.credential.user._id);
    if(this.validator() && this.credential.user._id){
      this.updateUser();
    }else{
      this.createUser();
    }
  }
  
  createUser(){
    this.authService
    .register(this.joinForm)
    .subscribe(data=>{
      localStorage.setItem('angular2ws_sabuzak_cred',JSON.stringify(data));
      this.nav.setRoot(Sabuzak);
    },error=>{
      let subTitle = "서버 응답이 없습니다.";
      switch (error.status){
        case 400:
          subTitle = "잘못된 요청 입니다.";
          break;
      }
      let alert = this.alertCtrl.create({
        title: '알림',
        subTitle: subTitle,
        buttons: ['확인']
      });
    })

  }

  updateUser(){
    this.userService
    .update(this.joinForm)
    .subscribe(data=>{
      localStorage.setItem('angular2ws_sabuzak_cred',JSON.stringify(data));
      this.nav.pop();
    },error=>{
      let subTitle = "서버 응답이 없습니다.";
      switch (error.status){
        case 400:
          subTitle = "잘못된 요청 입니다.";
          break;
      }
      let alert = this.alertCtrl.create({
        title: '알림',
        subTitle: subTitle,
        buttons: ['확인']
      });
    })
  }

  goBack(){
    this.nav.pop();
  }

  goTop(){
    this.content.scrollToTop();
  }

 

}
