/**
 * Created by wonseok Lee on 17/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/17
 *
 * Updater    수정자 - wonseok Lee 17/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController, 
        AlertController, 
        Events} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {LocalStorage} from "ng2-localstorage";

@Component({
  template: getTemplate('pages/sabuzak/chat-room/chat-room')
})

export class ChatRoom implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  constructor(public platform: Platform,
              private nav: NavController,
              public alertCtrl: AlertController,
              public viewCtrl: ViewController,
              public events: Events) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    
  }

  ngOnDestroy() {
  }

  /*****************************
   *        util functions
   *****************************/
  dismiss(){
    this.viewCtrl.dismiss({
      // someting data return
    });
  }
  goBack(){
    this.nav.pop();
  }

}
