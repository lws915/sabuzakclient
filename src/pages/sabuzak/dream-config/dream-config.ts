/**
 * Created by wonseok Lee on 18/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 18/08/17
 *
 * Updater    수정자 - wonseok Lee 18/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController, 
        AlertController,
        ActionSheetController, 
        Events} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {LocalStorage} from "ng2-localstorage";

@Component({
  template: getTemplate('pages/sabuzak/dream-config/dream-config')
})

export class DreamConfig implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  videoState = false;

  dreamForm:any;

  ageLenge = {lower: 25, upper: 35};
  locationList = [];
  bodyTypeList = [];
  tallList = [];
  faithList = [];
  drinkingList = [];
  smokingList = [];

  actTall: any;
  actBodyType: any;
  actLocation: any;
  actFaith: any;
  actDrinking: any;
  actSmoking: any;

  constructor(public platform: Platform,
              public nav: NavController,
              public alertCtrl: AlertController,
              public events: Events,
              public actionSheetCtrl: ActionSheetController,
            ) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    this.listInitializer();
    
  }

  ngOnDestroy() {
  }

  listInitializer(){
    this.dreamForm = {
      ageMin: "",
      ageMax: "",
      tall: "",
      bodyType: "",
      location: "",
      faith: "",
      drinking: "",
      smoking: ""
    }

    for(let i = 150; i <= 190; i++){
      this.tallList.push(i);
    }
    if(this.credential.user.gender == 'male'){
      this.bodyTypeList = ['마른','슬림탄탄','보통','글래머','포근']
    }else if(this.credential.user.gender == 'female'){
      this.bodyTypeList = ['마른','슬림탄탄','보통','건장한','포근'];
    }
    this.locationList = [ '서울','경기','인천','대전','충북','충남','강원','부산','경북','대구','울산','광주','전북','전남','제주'];
    this.faithList = ['무교','기독교','천주교','불교','기타'];
    this.drinkingList = ['마시지 않음','가끔 즐김','술자리를 즐기는 편'];
    this.smokingList = ['흡연','비흡연'];
    
    this.actTall = {
      title: "키",
      buttons: []
    };
    this.actBodyType = {
      title: "체형",
      buttons: []
    };
    this.actLocation = {
      title: "지역",
      buttons: []
    };
    this.actFaith = {
      title: "종교",
      buttons: []
    };
    this.actDrinking = {
      title: "음주",
      buttons: []
    };
    this.actSmoking = {
      title: "흡연",
      buttons: []
    };

    this.actionBtnSetter(this.tallList, this.actTall, "tall");
    this.actionBtnSetter(this.bodyTypeList, this.actBodyType, "bodyType");
    this.actionBtnSetter(this.locationList, this.actLocation, "location");
    this.actionBtnSetter(this.faithList, this.actFaith, "faith");
    this.actionBtnSetter(this.drinkingList, this.actDrinking, "drinking");
    this.actionBtnSetter(this.smokingList, this.actSmoking, "smoking");

  }

  /*****************************
   *        util functions
   *****************************/

  actionBtnSetter(actArr, actObj, dataLink){
    actArr.map(data => {
      actObj.buttons.push({
        text: data,
        handler: () => {
          this.dreamForm[dataLink] = data;
        }
      })
    })

    actObj.buttons.push({
      text: 'Cancel',
      role: 'cancel',
      handler: () => {}
    })
  }

  openSelect(input){
    let actionSheet = this.actionSheetCtrl.create(input); 
    actionSheet.present();
  }

  goBack(){
    this.nav.pop();
  }

}
