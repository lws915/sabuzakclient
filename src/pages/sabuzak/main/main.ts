/**
 * Created by wonseok Lee on 17/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/17
 *
 * Updater    수정자 - wonseok Lee 17/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController,
        MenuController, 
        ModalController, 
        AlertController, 
        Events} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {ViewProfile} from "../view-profile/view-profile";
import {Profile} from '../../profile/profile';
import {SelectList} from '../select-list/select-list';
import {SelectDetail} from "../select-detail/select-detail";
import {UserService} from '../../../shared/services/user.service';
import {RankService} from '../../../shared/services/rank.service';

@Component({
  template: getTemplate('pages/sabuzak/main/main')
})

export class Main implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  recommendList: any;
  userSelect: any = true;
  locker: any = true;


  pageList = {
    viewProfile : {
      comp : ViewProfile
    },
    selectList : {
      comp : SelectList
    },
    selectDetail : {
      comp : SelectDetail
    }

  }

  pushDrive = {
    profile : {comp : Profile}
  }

  constructor(public platform: Platform,
              private nav: NavController,
              public alertCtrl: AlertController,
              public menuCtrl: MenuController,
              public modalCtrl: ModalController,
              public userService: UserService,
              public rankService: RankService,
              public events: Events) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    this.initialization();
  }
  ionViewDidLoad(){
    this.menuCtrl.enable(true, 'sabuzakMenu');
  }

  ngOnDestroy() {
  }

  /*****************************
   *        util functions
   *****************************/
  initialization(){
    this.userService
    .recommend()
    .subscribe(
      data=>{
        this.recommendList = data.users;
        this.recommendList.map(item => {
          if(data.lastOpened == item._id){
            item.lock = true;
            this.locker = false;
          }else{
            item.lock = false;
          }
        })
      }
    )
  }
  goBack(){
    this.nav.pop();
  }

  screenGuard(input){
    input.stopPropagation();
  }

  videoPlay(input, id){
    input.stopPropagation();
    let video:any = document.querySelector(`#video${id}`);
    if(video.paused){
      video.play();
    }else{
      video.pause();
    }

  }

  selectProfile(input, userObj){
    if(this.locker){
      let alert = this.alertCtrl.create({
        title: '오늘의 매칭 카드',
        subTitle: `
        이 카드를 선택하시겠습니까?<br>
        '오늘의 매칭카드'중 하나만<br>
        무료로 볼 수 있습니다.`,
        buttons: [
          {
            text: '취소',
            role: 'cancel'
          },
          {
            text: '확인',
            handler: () => {
              this.selectProfileModalOpen(input, userObj);
            }
          }
        ]
      })
      alert.present();
    }else{
      this.selectProfileModalOpen(input, userObj);
    }
      
  }
  selectProfileModalOpen(input,userObj){
    let sendObj = {
      type : 'recommend',
      userObj : userObj,
      parents : this
    }

    if(this.userSelect){
      this.userService
        .profileOpen({otherID : userObj._id})
        .subscribe(
          data => {
            this.userSelect = false;
            let newModal = this.modalCtrl.create(input.comp,sendObj);
            newModal.present();
          },
          error => {
            console.log(error);
          }
        )
    }else{
      let newModal = this.modalCtrl.create(input.comp,sendObj);
      newModal.present();
    }
  }

  goSelectDetail(input){
    this.userService
        .findOne({_id: this.credential._id})
        .subscribe(
          data => {
            if(data.user.upCnt == 0 ){
              let alert = this.alertCtrl.create({
                title: '알림',
                subTitle: "평가 횟수를 다 소모하셧습니다.",
                buttons: ['확인']
              })
              alert.present();
            }else{
              this.nav.push(input.comp,{upCnt : data.user.upCnt}, {
                animation: 'ios-transition'
              });
            }
          }
        )
  }

  goSelectList(input){
    this.nav.setRoot(input.comp,{},{
      animate: true,
      animation: 'md-transition',
      direction: 'forward'
    });
  }

  toggleMenu() {
    this.menuCtrl.toggle();
  }

}
