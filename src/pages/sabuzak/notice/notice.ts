/**
 * Created by wonseok Lee on 21/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 21/08/17
 *
 * Updater    수정자 - wonseok Lee 21/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController, 
        AlertController, 
        Events} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {NoticeService} from "../../../shared/services/notice.service";

@Component({
  template: getTemplate('pages/sabuzak/notice/notice')
})

export class Notice implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  noticeList: any;

  testList: any;

  constructor(public platform: Platform,
              private nav: NavController,
              public alertCtrl: AlertController,
              public viewCtrl: ViewController,
              public events: Events,
              public noticeService: NoticeService
            ) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
   this.initializer();
  }

  ngOnDestroy() {
  }

  initializer(){
    this.noticeService
      .find({query:{}})
      .subscribe(data=>{
      this.noticeList = data.notices;
      this.noticeList.map(data=>{
        data.state = false;
      })
    })
  }

  /*****************************
   *        util functions
   *****************************/
  goBack(){
    this.nav.pop();
  }

  stateToggle(input){
    input.state = input.state ? false : true;
  }

}
