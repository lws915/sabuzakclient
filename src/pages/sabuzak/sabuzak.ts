/**
 * Created by wonseok Lee on 17/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/17
 *
 * Updater    수정자 - wonseok Lee 17/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy,
        trigger,
        transition,
        animate} from "@angular/core";
// Angular third party lib
// import {IonicApp, ModalController} from "ionic-angular";
import {App, 
        IonicApp,
        ViewController,
        Platform, 
        MenuController, 
        NavController, 
        AlertController, 
        ModalController,
        Events} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {Main} from "./main/main"
import {ChatList} from "./chat-list/chat-list";
import {NoticeList} from "./notice-list/notice-list";
import {ViewProfile} from "./view-profile/view-profile";
import {SelectList} from "./select-list/select-list";
import {SelectDetail} from "./select-detail/select-detail";
import {DreamConfig} from "./dream-config/dream-config";
import {RechargeBall} from "./recharge-ball/recharge-ball";
import {Usage} from "./usage/usage";
import {Settings} from "./settings/settings";
import {PasswordChange} from "./password-change/password-change";
import {Notice} from "./notice/notice";
import {SearchLiker} from './search-liker/search-liker';

@Component({
  template: getTemplate('pages/sabuzak/sabuzak')
})

export class Sabuzak implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  indexPage = Main;

  pageList = {
    main : {
      comp : Main
    },
    searchLiker : {
      comp : SearchLiker
    },
    dreamConfig : {
      comp : DreamConfig
    },
    rechargeBall : {
      comp : RechargeBall
    },
    settings : {
      comp : Settings
    },
    chatList : {
      comp : ChatList,
    },
    noticeList : {
      comp : NoticeList,
    },
    viewProfile : {
      comp : ViewProfile
    }
  }

  constructor(public platform: Platform,
              public nav: NavController,
              public menuCtrl: MenuController,
              public alertCtrl: AlertController,
              public modalCtrl: ModalController,
              public appCtrl: App,
              public ionicApp : IonicApp,
              public events: Events) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    
  }

  ngOnDestroy() {
  }

  /*****************************
   *        util functions
   *****************************/
  pagePusher(input){
    this.nav.push(input.comp, {}, {
      animation: 'ios-transition'
    });
  }
  pageSetRoot(input){
    this.appCtrl.getActiveNav().setRoot(input.comp,{},{
      animate: true,
      animation: 'md-transition',
      direction: 'back'
    });
    this.menuCtrl.close();
  }
  goMyProfile(input){
    let newModal = this.modalCtrl
      .create(
        input.comp,
        {userObj:this.credential.user}
      );
    newModal.present();
  }

}
