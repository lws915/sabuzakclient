/**
 * Created by wonseok Lee on 18/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 18/08/17
 *
 * Updater    수정자 - wonseok Lee 18/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController,
        NavParams, 
        ModalController, 
        AlertController, 
        Events} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {ViewProfile} from "../view-profile/view-profile";
import {UserService} from "../../../shared/services/user.service";
import {RankService} from "../../../shared/services/rank.service";

@Component({
  template: getTemplate('pages/sabuzak/select-detail/select-detail')
})

export class SelectDetail implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  user:any = {};

  availableCnt:any;

  constructor(public platform: Platform,
              public nav: NavController,
              public params: NavParams,
              public alertCtrl: AlertController,
              public viewCtrl: ViewController,
              public modalCtrl: ModalController,
              public userService: UserService,
              public rankService: RankService,
              public events: Events) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    this.getRecommendUser();
    this.availableCnt = this.params.data.upCnt;
  }

  ngOnDestroy() {

  }

  /*****************************
   *        util functions
   *****************************/
  
  getRecommendUser(){
    this.rankService
    .recommend()
    .subscribe(
      data => {
        console.log(data);
        if(data.users.length == 0){
          let alert = this.alertCtrl.create({
            title: '알림',
            subTitle: '더 이상 추천가능한 이성이 없습니다.',
            buttons: [
              {
                text:'확인',
                handler: () => {
                  this.goBack();
                }
              }
            ]
          })
          alert.present();
        }else{
          this.user = data.users[0];
        }
      }
    )
  }
  
  upNDown(type){
    this.rankService
      .create({
        user: this.user._id,
        type: type 
      })
      .subscribe(
        data => {
          console.log(data);
          if(data.upCnt == 0){
            let alert = this.alertCtrl.create({
              title: '알림',
              subTitle: '평가 횟수를 모두 소모하셧습니다.',
              buttons: [
                {
                  text:'확인',
                  handler: () => {
                    this.goBack();
                  }
                }
              ]
            })
            alert.present();
          }else{
            this.getRecommendUser();
            this.availableCnt = data.upCnt;
          }
        },
        error => {
          console.log(error);
        }
      )
  }

  modalOpener(){
    let sendObj = {
      type : '',
      userObj : this.user
    }
    let newModal = this.modalCtrl.create(ViewProfile,sendObj);
    newModal.present();
  }


  goBack(){
    if(this.params.data.parentPage){
      this.params.data.parentPage.initializer();
    }
    this.nav.pop({
      animation: 'ios-transition'
    });
  }



}
