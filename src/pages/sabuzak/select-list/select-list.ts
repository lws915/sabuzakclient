/**
 * Created by wonseok Lee on 18/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 18/08/17
 *
 * Updater    수정자 - wonseok Lee 18/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy,
        NgZone,
        ViewChild} from "@angular/core";
// Angular third party lib
import {App,
        IonicApp,
        ViewController, 
        Platform, 
        NavController,
        MenuController, 
        AlertController, 
        Events,
        Content} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {Main} from "../main/main";
import {SelectDetail} from "../select-detail/select-detail";
import {SearchLiker} from "../search-liker/search-liker";
import {UserService} from "../../../shared/services/user.service";
import {RankService} from "../../../shared/services/rank.service";
import {LikeService} from "../../../shared/services/like.service";

@Component({
  template: getTemplate('pages/sabuzak/select-list/select-list')
})

export class SelectList implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;
  @ViewChild(Content) content:Content;
  
  topState = false;

  delState = false;
  pageLimit = 6;
  
  pageDrive = {
    main : {
      comp : Main
    },
    selectDetail : {
      comp : SelectDetail
    },
    searchLiker : {
      comp : SearchLiker
    }

  }

  listCollection: any;

  constructor(public platform: Platform,
              public nav: NavController,
              public alertCtrl: AlertController,
              public menuCtrl: MenuController,
              public viewCtrl: ViewController,
              public userService: UserService,
              public rankService: RankService,
              public likeService: LikeService,
              public appCtrl: App,
              public events: Events,
              public zone: NgZone) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    this.initializer();
  }

  ngOnDestroy() {

  }

  
  /*****************************
   *        util functions
   *****************************/

  initializer(){
    this.listCollection = [];

    this.likeMeCall(0)
      .flatMap(
        data => {
          this.listCollection.push({
            title : "나를 좋아하는 이성",
            type : "likeMeCall",
            users : data.likes,
            more : data.more,
            curNum : 1
          })
          return this.upMeCall(0);
        }
      )
      .flatMap(
        data => {
          this.listCollection.push({
            title : "나를 업한 이성",
            type : "upMeCall",
            users : data.ranks,
            more : data.more,
            curNum : 1
          })
          return this.likeUCall(0);
        }
      )
      .flatMap(
        data => {
          this.listCollection.push({
            title : "내가 좋아하는 이성",
            type : "likeUCall",
            users : data.likes,
            more : data.more,
            curNum : 1
          })
          return this.upUCall(0);
        }
      )
      .flatMap(
        data => {
          this.listCollection.push({
            title : "내가 업한 이성",
            type : "upUCall",
            users : data.ranks,
            more : data.more,
            curNum : 1
          })
          return this.eachUpCall(0);
        }
      )
      .subscribe(
        data => {
          this.listCollection.push({
            title : "서로 업한 이성",
            type : "eachUpCall",
            users : data.ranks,
            more : data.more,
            curNum : 1
          })
        }
      )
  }

  upUCall(num){
    return this.rankService
      .find({
        query: {owner: this.credential.user._id, type: 'up'},
        limit: this.pageLimit,
        skip: this.pageLimit * num,
        sort: {createdAt: -1},
        populate: ['user']
      });
  }
  upMeCall(num){
    return this.rankService
      .find({
        query: {userID: this.credential.user._id, type: 'up'},
        limit: this.pageLimit,
        skip: this.pageLimit * num,
        sort: {createdAt: -1},
        populate: ['user']
      });
  }
  eachUpCall(num){
    return this.rankService
      .find({
        query: {owner: this.credential.user._id, type: 'up', eachUp: true},
        limit: this.pageLimit,
        skip: this.pageLimit * num,
        sort: {createdAt: -1},
        populate: ['user']
      });
  }

  likeUCall(num){
    return this.likeService
      .find({
        query: {owner: this.credential.user._id},
        limit: this.pageLimit,
        skip: this.pageLimit * num,
        sort: {createdAt: -1},
        populate: ['user']
      });
  }

  likeMeCall(num){
    return this.likeService
      .find({
        query: {user: this.credential.user._id},
        limit: this.pageLimit,
        skip: this.pageLimit * num,
        sort: {createdAt: -1},
        populate: ['user']
      })
  }


  getMore(input){
    this[input.type](input.curNum).subscribe(
      data => {
        if(data.ranks){
          data.ranks.map(item => {
            input.users.push(item);
          })
        }else if(data.likes){
          data.likes.map(item => {
            input.users.push(item);
          })
        }
        input.more = data.more;
        input.curNum++;
      }
    )
  }

  goBack(){
    this.nav.pop();
  }

  delToggle(){
    this.delState = this.delState == false ? true : false;
  }

  pagePusher(input){
    this.nav.push(input.comp);
  }

  goSelectDetail(input){
    this.userService
        .findOne({_id: this.credential._id})
        .subscribe(
          data => {
            if(data.user.upCnt == 0 ){
              let alert = this.alertCtrl.create({
                            title: '알림',
                            subTitle: "평가 횟수를 다 소모하셧습니다.",
                            buttons: ['확인']
                          })
              alert.present();
            }else{
              this.nav.push(input.comp,
                {
                  upCnt : data.user.upCnt,
                  parentPage : this
                }, 
                {
                  animation: 'ios-transition'
                });
            }
          }
        )
  }

  pageSetRoot(input){
    this.nav.setRoot(input.comp);
  }

  isTopChk(){
    if(this.content.scrollTop != 0){
      this.zone.run(()=>{
        this.topState = true;
      })
    }else{
     this.zone.run(()=>{
       this.topState = false;
     })
    }
  }

  goTop(){
    this.content.scrollToTop();
   }

}
