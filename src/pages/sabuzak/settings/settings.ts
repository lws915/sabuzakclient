/**
 * Created by wonseok Lee on 21/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 21/08/17
 *
 * Updater    수정자 - wonseok Lee 21/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy} from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController, 
        AlertController, 
        Events} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {PasswordChange} from "../password-change/password-change";
import {Notice} from "../notice/notice";
import {UserService} from "../../../shared/services/user.service";

@Component({
  template: getTemplate('pages/sabuzak/settings/settings')
})

export class Settings implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;

  test: any;
  isAlert: any;
  isPush: any;
  isTodayMatch: any;

  pageList={
    passwordChange : {
      comp : PasswordChange
    },
    notice :{
      comp : Notice
    }
  }

  constructor(public platform: Platform,
              private nav: NavController,
              public alertCtrl: AlertController,
              public userService: UserService,
              public events: Events) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
  
    this.userService
      .find({
        query:{
          _id:this.credential.user._id
        }
      })
      .subscribe(
        data=>{
          this.isAlert = data.users[0].isAlert;
          this.isPush = data.users[0].isPush;
          this.isTodayMatch = data.users[0].isTodayMatch;
        }
      )
    
  }

  ngOnDestroy() {
    let sendObj ={
      _id : this.credential.user._id,
      isAlert : this.isAlert,
      isPush : this.isPush,
      isTodayMatch : this.isTodayMatch,
      location : this.credential.user.location,
      bodyType : this.credential.user.bodyType,
      tall : this.credential.user.tall
    }

    this.userService
      .update(sendObj)
      .subscribe(
        data=>{},
        error=>{
          this.warningAlert('요청중 문제가 발생했습니다.');
        }
      )

  }

  /*****************************
   *        util functions
   *****************************/

  warningAlert(input){
    let alert = this.alertCtrl.create({
      title: '알림',
      subTitle: input,
      buttons: ['확인']
    });

    alert.present();
  }
  goBack(){
    this.nav.pop();
  }
  pagePusher(input){
    this.nav.push(input.comp);
  }

}
