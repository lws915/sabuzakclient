/**
 * Created by wonseok Lee on 17/08/17
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/17
 *
 * Updater    수정자 - wonseok Lee 18/08/17
 */
// Export lib
declare var window;
declare var _;
// Angular
import {Component, 
        OnInit, 
        OnDestroy,
        ViewChild,
        NgZone} from "@angular/core";
// Angular third party lib
import {ViewController, 
        Platform, 
        NavController,
        NavParams, 
        AlertController, 
        Events,
        Content} from "ionic-angular";
// Appzet Source
import {getTemplate} from "../../../shared/templates";
import {LocalStorage} from "ng2-localstorage";
import {Profile} from "../../profile/profile";
import {UserService} from "../../../shared/services/user.service";
import {LikeService} from "../../../shared/services/like.service";

@Component({
  template: getTemplate('pages/sabuzak/view-profile/view-profile')
})

export class ViewProfile implements OnInit, OnDestroy {

  @LocalStorage("sabuzak_cred") credential;
  @ViewChild(Content) content:Content;
  
  topState = false;

  owner;
  user:any;
  type:string;

  constructor(public platform: Platform,
              private nav: NavController,
              public params: NavParams,
              public alertCtrl: AlertController,
              public viewCtrl: ViewController,
              public events: Events,
              public userService: UserService,
              public likeService: LikeService,
              public zone: NgZone) {
  }

  /*****************************
   *         life cycle
   *****************************/

  ngOnInit() {
    if(this.params.data.userObj._id == this.credential.user._id){
      this.owner = true;
    }else{
      this.owner = false;
    }
    this.user = this.params.data.userObj;
    this.type = this.params.data.type;
  }

  ngOnDestroy() {
  }

  /*****************************
   *        util functions
   *****************************/
  dismiss(){
    if(this.params.data.parents){
      this.params.data.parents.initialization();
    }
    this.viewCtrl.dismiss({
      // someting data return
    });
  }

  goProfileUpdate(){
    this.nav.push(Profile);
  }

  likeChk(){
    this.likeService
      .create({
        user: this.user._id
      })
      .subscribe(
        data => {
          console.log(data);
        },
        error => {
          console.log(error);
        }
      )
  }

  isTopChk(){
    if(this.content.scrollTop != 0){
      this.zone.run(()=>{
        this.topState = true;
      })
    }else{
     this.zone.run(()=>{
       this.topState = false;
     })
    }
  }

  goTop(){
    this.content.scrollToTop();
   }

}
