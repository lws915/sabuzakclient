/**
 * Created by andy on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

// Export lib
declare var Logger;

// Angular
import {Pipe, PipeTransform} from "@angular/core";



@Pipe({name: 'mobile'})
export class MobilePipe implements PipeTransform {

  transform(value: any): string {
    if (!value)
      return '';

    let result = "";

    if (value.length > 3) {
      result += value.substring(0, 3) + "-";
      value = value.substring(3);
    }

    if (value.length > 4) {
      result += value.substring(0, 4) + "-";
      value = value.substring(4);
    }

    result += value;

    return result;
  }
}
