/**
 * Created by andy on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

// Export lib
declare var Logger;
declare var _;

// Browser lib
import {Observable} from "rxjs";

// Angular
import {Injectable, EventEmitter} from "@angular/core";

// Logger.domains.push("app.service");

@Injectable()
export class AppService {

  // ["Start", "Ready", "Fail"]
  appStatus = "Start";

  deviceUUID;

  appStatusEvent: EventEmitter<any> = new EventEmitter();

  constructor() {
  }


  setAppStatus(status) {
    Logger.silly('app.service', 'setAppStatus');
    this.appStatus = status;
    this.appStatusEvent.emit(status);
  }

  getAppStatusEvent(): any {
    Logger.silly('app.service', 'getAppStatusEvent');
    if (this.appStatus != "Start") return Observable.from([this.appStatus]);
    else return this.appStatusEvent;
  }

}
