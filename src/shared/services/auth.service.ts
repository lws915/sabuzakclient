/**
 * Created by Yoon Yong (Andy) Shin on 23/12/2016
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Yoon Yong (Andy) Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoon Yong (Andy) Shin <andy.shin@applicat.co.kr>, 23/12/2016
 *
 */

// Export Lib
declare var Logger;
declare var _;

// Browser Lib
import "rxjs/add/operator/toPromise";

// Angular
import {Injectable} from "@angular/core";
import {RequestOptions, URLSearchParams, Headers, Http} from "@angular/http";

// Angular Third Party Lib
import {InterceptableHttp} from "../../lib/ng-http-interceptor";

// Project Sources
import {config} from "../../app/app.config";
import {Observable} from "rxjs";

// Logger.domains.push('auth.service');

@Injectable()
export class AuthService {
  private serverUrl = config.serverUrl;

  db;

  constructor(private  http: InterceptableHttp,
              private  httpNoIntercept: Http) {
  }

  checkEmail(email: string): Observable<any> {
    Logger.silly('auth.service', 'checkEmail');
    let url = this.serverUrl + '/checkEmail';

    let params: URLSearchParams = new URLSearchParams();
    params.set("email", email);

    return this.http
      .get(url, _.assign({search: params}));
  }

  register(data): Observable<any> {
    Logger.silly('auth.service', 'register');
    let url = this.serverUrl + '/register';

    return this.http
      .post(url, data);
  }

  login(identifier: string, password: string): Observable<any> {
    Logger.silly('auth.service', 'login');
    let url = this.serverUrl + '/login';

    return this.http
      .post(url, {identifier: identifier, password: password});
  }

  socialLogin():Observable<any>{
    Logger.silly('auth.service', 'socialLogin');
    let url = this.serverUrl + '/socialLogin';

    return this.http
      .get(url);
  }


  confirmActivation(identifier: string, code: string): Observable<any> {
    Logger.silly('auth.service', 'oauth');

    let url = this.serverUrl + '/activate/confirm';

    return this.http
      .put(url, {identifier: identifier, code: code});
  }

  forgotPasswordStart(identifier: string): Observable<any> {
    Logger.silly('auth.service', 'oauth');

    let url = this.serverUrl + '/forgotPassword';

    return this.http
      .post(url, {identifier: identifier});
  }

  forgotPasswordCheck(identifier: string, code: string): Observable<any> {
    Logger.silly('auth.service', 'oauth');

    let url = this.serverUrl + '/forgotPasswordCheck';

    return this.http
      .post(url, {identifier: identifier, code: code});
  }

  forgotPasswordComplete(newPassword: string): Observable<any> {
    Logger.silly('auth.service', 'oauth');

    let url = this.serverUrl + '/forgotPasswordComplete';

    return this.http
      .put(url, {newPassword: newPassword});
  }

  forgotPasswordStartSMS(mobile: string): Observable<any> {
    Logger.silly('auth.service', 'oauth');

    let url = this.serverUrl + '/forgotPasswordSMS';

    return this.http
      .post(url, {mobile: mobile});
  }

  forgotPasswordCheckSMS(mobile: string, code: string): Observable<any> {
    Logger.silly('auth.service', 'oauth');

    let url = this.serverUrl + '/forgotPasswordCheckSMS';

    return this.http
      .post(url, {mobile: mobile, code: code});
  }

  forgotPasswordCompleteSMS(newPassword: string): Observable<any> {
    Logger.silly('auth.service', 'oauth');

    let url = this.serverUrl + '/forgotPasswordCompleteSMS';

    return this.http
      .put(url, {newPassword: newPassword});
  }

  support(enquiry, file?): Observable<any> {
    Logger.silly('auth.service', 'oauth');

    const formData = new FormData();
    _.forEach(enquiry, (val, Key) => {
      formData.append(Key, val);
    });

    if (file)
      formData.append('file', file);

    const headers = new Headers({});
    let options = new RequestOptions({headers});
    let url = this.serverUrl + '/support';

    return this.http.post(url, formData, options);
  }


  logout(): Observable<any> {
    Logger.silly('auth.service', 'oauth');

    let url = this.serverUrl + '/logout';
    return this.http.get(url);
  }


  changePassword(oldPassword: string, newPassword: string): Observable<any> {
    Logger.silly('auth.service', 'changePassword');

    let url = this.serverUrl + '/changePassword';

    return this.http
      .put(url, {oldPassword: oldPassword, newPassword: newPassword});
  }

  getMyUserInfo(queryParams: any): Observable<any> {
    Logger.silly('auth.service', 'getMyUserInfo');

    let url = this.serverUrl + '/me';

    let params: URLSearchParams = new URLSearchParams();

    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

  updateMyInfo(user): Observable<any> {
    Logger.silly('auth.service', 'updateMyInfo');

    let url = this.serverUrl + '/me';

    return this.http
      .put(url, user);
  }

  sendActivationEmail(): Observable<any> {
    Logger.silly('auth.service', 'sendActivationEmail');

    let url = this.serverUrl + '/activate/send';

    return this.http
      .post(url, {});
  }


  checkMobileNumber(mobile): Observable<any> {
    Logger.silly('auth.service', 'checkMobileNumber');

    let url = this.serverUrl + '/checkMobileNumber';

    return this.http
      .post(url, {mobile: mobile});
  }

  completeMobileNumber(code): Observable<any> {
    Logger.silly('auth.service', 'completeMobileNumber');

    let url = this.serverUrl + '/completeMobileNumber';

    return this.http
      .post(url, {code: code});
  }

  changeSafeNumber(): Observable<any> {
    Logger.silly('auth.service', 'checkMobileNumber');

    let url = this.serverUrl + '/changeSafeNumber';

    return this.http
      .put(url, {});
  }


}
