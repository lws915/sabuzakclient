/**
 * Created by wonseok Lee on 24/08/2017
 * As part of appzetuseradmin
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 24/08/2017
 *
 */

// Export lib
// declare var Logger;
declare var _;

// Browser lib
import {Observable} from 'rxjs';

// Angular
import {Injectable} from '@angular/core';
import {URLSearchParams} from '@angular/http';

// Angular third party lib
import {InterceptableHttp} from '../../lib/ng-http-interceptor';

// Appzet Source
import {config} from "../../app/app.config";

// // Logger.domains.push("post.service");

@Injectable()
export class BallService {

  private serverUrl = config.serverUrl;

  constructor(private http: InterceptableHttp) {
  }

  find(queryParams: any): Observable<any> {
    // Logger.silly('post.service', 'find');
    let url = this.serverUrl + '/ball' + '/find';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key)=> {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

  // findOne(queryParams: any): Observable<any> {
  //   // Logger.silly('ball.service', 'findOne');
  //   let url = this.serverUrl + '/ball' + '/findOne';

  //   let params: URLSearchParams = new URLSearchParams();
  //   _.forEach(queryParams, (value, key)=> {
  //     params.set(key, JSON.stringify(value));
  //   });

  //   return this.http.get(url, {search: params});
  // }

  // create(params: Object): Observable<any> {
  //   // Logger.silly('ball.service', 'create');
  //   return this.http
  //     .ball(this.serverUrl + '/ball', params);
  // }

  // update(params: Object): Observable<any> {
  //   // Logger.silly('ball.service', 'update');
  //   return this.http
  //     .put(this.serverUrl + '/ball', params);
  // }

  // remove(_id: string): Observable<any> {
  //   // Logger.silly('ball.service', 'remove');
  //   let param: URLSearchParams = new URLSearchParams();

  //   param.set("_id", _id.toString());

  //   return this.http
  //     .delete(this.serverUrl + '/ball', {search: param});
  // }
}
