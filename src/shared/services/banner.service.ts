/**
 * Created by sungwookim on 06/10/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by sungwookim <ksw1652@gmail.com>, 06/10/2016
 *
 * Updater    수정자 - PHILIP 22/03/2017
 */

// Export lib
declare var _;
declare var Logger;

// Browser lib
import {Observable} from "rxjs";

// Angular
import {Injectable} from "@angular/core";
import {URLSearchParams} from "@angular/http";

// Angular third party lib
import {InterceptableHttp} from "../../lib/ng-http-interceptor";

// Appzet Source
import {config} from "../../app/app.config";

// Logger.domains.push("banner.service");

@Injectable()
export class BannerService {
  private serverUrl = config.serverUrl + '/banner';

  constructor(private http: InterceptableHttp) {
  }

  count(queryParams: any): Observable<any> {
    let url = this.serverUrl + '/count';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

  find(queryParams: any): Observable<any> {
    Logger.silly('banner.service', 'find');
    let url = this.serverUrl + '/find';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

  findOne(queryParams: any): Observable<any> {
    Logger.silly('banner.service', 'findOne');
    let url = this.serverUrl + '/findOne';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

}
