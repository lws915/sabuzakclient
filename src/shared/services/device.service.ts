/**
 * Created by PHILIP on 03/03/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by PHILIP <hbnb7894@gmail.com>, 03/03/2017
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

// Export lib
declare var _;
declare var Logger;

// Browser lib
import {Observable} from "rxjs";

// Angular
import {Injectable} from "@angular/core";
import {Headers, URLSearchParams} from "@angular/http";

// Angular third party lib
import {InterceptableHttp} from "../../lib/ng-http-interceptor";
import {config} from "../../app/app.config";

// Appzet Source

// Logger.domains.push("device.service");

@Injectable()
export class DeviceService {

  private serverUrl = config.serverUrl;
  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: InterceptableHttp) {
  }

  findOne(queryParams: any): Observable<any> {
    Logger.silly('device.service', 'findOne');
    let url = this.serverUrl + '/findOne';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

  pushOn(deviceId): Observable<any> {
    Logger.silly('device.service', 'update');
    return this.http
      .put(this.serverUrl, {deviceId: deviceId}, {headers: this.headers});
  }

  pushOff(deviceId): Observable<any> {
    Logger.silly('device.service', 'update');
    return this.http
      .put(this.serverUrl, {deviceId: deviceId}, {headers: this.headers});
  }

  update(params: Object): Observable<any> {
    Logger.silly('device.service', 'update');
    return this.http
      .put(this.serverUrl, params, {headers: this.headers});
  }

  myDevice(): Observable<any> {
    Logger.silly('device.service', 'myDevice');

    let url = this.serverUrl + '/mine';

    return this.http
      .get(url, {});
  }


}
