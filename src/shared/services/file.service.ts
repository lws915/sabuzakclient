import "rxjs/add/operator/toPromise";
import {Observable} from "rxjs/Observable";
// Angular core
import {Injectable} from "@angular/core";
import {RequestOptions, Headers} from "@angular/http";
// Angular third party lib
import {Transfer, FileUploadResult} from "ionic-native";
import {InterceptableHttp} from "../../lib/ng-http-interceptor/http/interceptable-http";
import {config} from "../../app/app.config";
/**
 * Created by PHILIP on 14/02/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by PHILIP <hbnb7894@gmail.com>, 14/02/2017
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

// Export lib
declare var _;

// Appzet Source


@Injectable()
export class FileService {
  private serverUrl = config.serverUrl + '/file';

  constructor(private http: InterceptableHttp) {
  }

  uploadInApp(targetPath, options): Promise<FileUploadResult> {
    let fileTransfer = new Transfer();
    return fileTransfer.upload(targetPath, this.serverUrl, options);
  }

  uploadInWeb(file): Observable<any> {

    const formData = new FormData();

    formData.append("file", file);

    const headers = new Headers({});
    let options = new RequestOptions({headers});
    let url = this.serverUrl;

    return this.http.post(url, formData, options);
  }
}
