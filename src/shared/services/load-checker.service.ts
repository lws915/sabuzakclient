/**
 * Created by andy on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP Seo 21/03/2017
 */

// Export lib
declare var Logger;

// Browser lib
import {Observable} from "rxjs";

// Angular
import {Injectable} from "@angular/core";

// Appzet Source
import {SpinnerService} from "./spinner.service";

// Logger.domains.push("load-checker.service");

@Injectable()
export class LoadChecker {

  loadedPages = {};

  constructor() {
  }

  checkPageLoaded(pageName: string) {
    Logger.silly('load-checker.service', 'checkPageLoaded');
    if (this.loadedPages[pageName]) {
      this.hideLoader();
      return true;
    } else {
      this.showLoader();
      return false;
    }
  }

  setPageLoaded(pageName: string) {
    Logger.silly('load-checker.service', 'setPageLoaded');
    this.loadedPages[pageName] = true;
    document.querySelector('#loading-page').classList.add('loaded');
  }

  //로더 보이기 (loading) --> for modal
  showLoader() {
    Logger.silly('load-checker.service', 'showLoader');
    SpinnerService.show();
  }

  //로더 숨기기 (loaded) --> for modal
  hideLoader() {
    Logger.silly('load-checker.service', 'hideLoader');
    SpinnerService.hide();
  }

  checkImageLoaded(url): Observable<any> {
    Logger.silly('load-checker.service', 'checkImageLoaded');
    return new Observable(observer => {
      let img = new Image();
      img.src = url;
      img.onload = () => {
        observer.next(img);
        observer.complete();
      };
      img.onerror = (err) => {
        observer.error(err);
      }
    });
  }

  checkImagesLoaded(urls: Array<any>): Observable<any> {
    Logger.silly('load-checker.service', 'checkImagesLoaded');
    let observables = [];
    urls.forEach((url) => {
      observables.push(this.checkImageLoaded(url));
    });
    return Observable.forkJoin(observables)
  }
}
