/**
 * Created by Yoon Yong (Andy) Shin on 23/12/2016
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Yoon Yong (Andy) Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoon Yong (Andy) Shin <andy.shin@applicat.co.kr>, 23/12/2016
 *
 */

// Export Lib
declare var Logger;
declare var _;

// Browser Lib
import {Observable} from "rxjs";
import "rxjs/add/operator/toPromise";

// Angular
import {Injectable} from "@angular/core";
import {URLSearchParams} from "@angular/http";
import {InterceptableHttp} from "../../lib/ng-http-interceptor/http/interceptable-http";

// Project Sources
import {config} from "../../app/app.config";

// Logger.domains.push('namecard.service');

@Injectable()
export class NameCardService {
  serverUrl = config.serverUrl + '/nameCard';

  constructor(private http: InterceptableHttp) {
  }

  count(queryParams: any): Observable<any> {
    Logger.silly('namecard.service', 'count');
    let url = this.serverUrl + '/count';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }


  findMine(queryParams: any): Observable<any> {
    Logger.silly('namecard.service', 'findMine');
    let url = this.serverUrl + '/findMine';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

  find(queryParams: any): Observable<any> {
    Logger.silly('namecard.service', 'find');
    let url = this.serverUrl + '/find';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

  findOne(queryParams: any): Observable<any> {
    Logger.silly('namecard.service', 'findOne');
    let url = this.serverUrl + '/findOne';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

  create(card): Observable<any> {
    let url = this.serverUrl;

    return this.http
      .post(url, card);
  }

  update(card): Observable<any> {
    let url = this.serverUrl;

    return this.http
      .put(url, card);
  }

  remove(id): Observable<any> {
    let params: URLSearchParams = new URLSearchParams();
    params.set("_id", id.toString());

    return this.http
      .delete(this.serverUrl, {search: params});
  }
}
