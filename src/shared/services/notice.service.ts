/**
 * Created by wonseok Lee on 24/08/2017
 * As part of appzetuseradmin
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 24/08/2017
 *
 */

// Export lib
// declare var Logger;
declare var _;

// Browser lib
import {Observable} from 'rxjs';

// Angular
import {Injectable} from '@angular/core';
import {URLSearchParams} from '@angular/http';

// Angular third party lib
import {InterceptableHttp} from '../../lib/ng-http-interceptor';

// Appzet Source
import {config} from "../../app/app.config";

// // Logger.domains.push("post.service");

@Injectable()
export class NoticeService {

  private serverUrl = config.serverUrl;

  constructor(private http: InterceptableHttp) {
  }

  find(queryParams: any): Observable<any> {
    // Logger.silly('notice.service', 'find');
    let url = this.serverUrl + '/notice' + '/find';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key)=> {
      params.set(key, JSON.stringify(value));
    });

    return this.http
      .get(url, {search: params});
  }

  // findOne(queryParams: any): Observable<any> {
  //   // Logger.silly('notice.service', 'findOne');
  //   let url = this.serverUrl + '/notice' + '/findOne';

  //   let params: URLSearchParams = new URLSearchParams();
  //   _.forEach(queryParams, (value, key)=> {
  //     params.set(key, JSON.stringify(value));
  //   });

  //   return this.http.get(url, {search: params});
  // }

  // create(params: Object): Observable<any> {
  //   // Logger.silly('notice.service', 'create');
  //   return this.http
  //     .notice(this.serverUrl + '/notice', params);
  // }

  // update(params: Object): Observable<any> {
  //   // Logger.silly('notice.service', 'update');
  //   return this.http
  //     .put(this.serverUrl + '/notice', params);
  // }

  // remove(_id: string): Observable<any> {
  //   // Logger.silly('notice.service', 'remove');
  //   let param: URLSearchParams = new URLSearchParams();

  //   param.set("_id", _id.toString());

  //   return this.http
  //     .delete(this.serverUrl + '/notice', {search: param});
  // }
}
