/**
 * Created by andy on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

// Export lib
declare var FirebasePlugin: any;
declare var plugins: any;
declare var window: any;
declare var Logger;
// Angular
import {config} from "../../app/app.config";
import {EventEmitter, Injectable} from "@angular/core";
// Angular Third Party
import {InterceptableHttp} from "../../lib/ng-http-interceptor";
import {AppService} from "./app.service";

// Appzet Source

// Logger.domains.push("push.service");

@Injectable()
export class PushService {

  serverUrl = config.serverUrl;
  onNotification = new EventEmitter();


  constructor(private http: InterceptableHttp,
              private appService: AppService) {
  }

  register() {
    // Logger.silly('push.service', 'register');
    // let _self = this;
    // if (!window.FirebasePlugin)return;
    // window.FirebasePlugin.grantPermission();
    // window.FirebasePlugin.getToken(
    //   (token) => {
    //     if (this.platform.is('ios')) {
    //       this.storeDeviceToken(token, 'iOS');
    //       window.FirebasePlugin.grantPermission();
    //     } else if (this.platform.is('android')) {
    //       this.storeDeviceToken(token, 'Android');
    //     }
    //   },
    //   (err) => {
    //     Logger.debug('error retrieving token: ' + err);
    //   }
    // );
    //
    // window.FirebasePlugin.onNotificationOpen(
    //   notification => {
    //     if (notification.profile != 0) {
    //       this.onNotification.emit(notification);
    //
    //       if (this.platform.is('android')) {
    //         // TODO: ...
    //         window.plugin.notification.local.schedule({
    //           title: notification.title,
    //           text: notification.body,
    //           icon: "res://icon.png",
    //           smallIcon: "res://pushicon.png"
    //         });
    //       } else if (this.platform.is('ios')) {
    //         if (notification.additionalData.foreground === true) {
    //           // TODO: ...
    //         } else {
    //           // TODO: ...
    //         }
    //       }
    //     }
    //
    //   },
    //   err => {
    //     Logger.debug("push.service", 'Error registering onNotification callback: ' + err);
    //   });
  }


  storeDeviceToken(pushId, deviceType) {
    // Logger.silly('push.service', 'storeDeviceToken');
    // let _self = this;
    // var registration = {
    //   deviceId: this.appService.deviceUUID,
    //   pushId: pushId,
    //   platform: deviceType,
    //   active: true
    // };
    //
    // Logger.debug("push.service", this.appService.deviceUUID);
    // _self.http.put(_self.serverUrl + '/device', registration)
    //   .subscribe(
    //     (dataWrapper: any) => {
    //       Logger.debug("push.service", "PushService - 서버에 등록된 기기정보 : ", dataWrapper);
    //
    //       window.device.deviceIndex = this.LPAD(dataWrapper.device._id + "", '0', 6);
    //       window.device.uniqueDeviceID = this.appService.deviceUUID;
    //
    //       Logger.debug("push.service", '현재 device의 정보들 :::\n', window.device);
    //     },
    //     err => {
    //       Logger.debug("push.service", "PushService - error: " + JSON.stringify(err));
    //     }
    //   );
  }

  LPAD(s, c, n) {
    Logger.silly('push.service', 'LPAD');
    if (!s || !c || s.length >= n) {
      return s;
    }
    let max = (n - s.length) / c.length;
    for (let i = 0; i < max; i++) {
      s = c + s;
    }
    return s;
  }
}
