/**
 * Created by wonseok Lee on 30/08/2017
 * As part of appzetuseradmin
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <ksw1652@gmail.com>, 30/08/2017
 *
 */

// Export lib
// declare var Logger;
declare var _;

// Browser lib
import {Observable} from 'rxjs';

// Angular
import {Injectable} from '@angular/core';
import {URLSearchParams} from '@angular/http';

// Angular third party lib
import {InterceptableHttp} from '../../lib/ng-http-interceptor';

// Appzet Source
import {AppService} from './app.service';
import {config} from "../../app/app.config";

// // Logger.domains.push("user.service");

@Injectable()
export class RankService {

	private serverUrl = config.serverUrl;

  constructor(private http: InterceptableHttp) {
  }
  create(data): Observable<any> {
    // Logger.silly('auth.service', 'register');
    let url = this.serverUrl + '/rank';

    return this.http
      .post(url, data);
  }

  count(queryParams: any): Observable<any> {
    // Logger.silly('user.service', 'find');
    let url = this.serverUrl + '/rank' + '/count';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http.get(url, {search: params});
  }

  find(queryParams: any): Observable<any> {
    // Logger.silly('user.service', 'find');
    let url = this.serverUrl + '/rank' + '/find';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http.get(url, {search: params});
  }

  findOne(queryParams: any): Observable<any> {
    // Logger.silly('user.service', 'findOne');
    let url = this.serverUrl + '/rank' + '/findOne';

    let params: URLSearchParams = new URLSearchParams();
    _.forEach(queryParams, (value, key) => {
      params.set(key, JSON.stringify(value));
    });

    return this.http.get(url, {search: params});
  }

  recommend(): Observable<any>{
    // Logger.silly('user.service', 'find');
    
    let url = this.serverUrl + '/rank' + '/recommend';

    return this.http.get(url, {});
  }
  
}
