/**
 * Created by PHILIP on 27/02/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by PHILIP <hbnb7894@gmail.com>, 27/02/2017
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

// Export lib
declare var _;
declare var cordova;
declare var Kakao;
declare var Logger;

// Angular
import {Injectable} from "@angular/core";

// Angular third party lib
import {AlertController} from "ionic-angular";
import {SocialSharing, AppAvailability, Device, Toast} from "ionic-native";

// Logger.domains.push("share.server");

@Injectable()
export class ShareService {

  pasteMessageHint = "내용이 복사되었습니다 붙여넣기 해주세요.";

  constructor(private alertController: AlertController) {
  }
  //
  // /*****************************
  //  *        util functions
  //  *****************************/
  //
  // kakaoTalk(message: string, url: string, image?: Object) {
  //   Logger.silly('share.server', 'kakaoTalk');
  //   if (this.appService.isCordova) {
  //     let alert = this.alertController.create();
  //     alert.addButton('확인');
  //     let encodedUrl = encodeURIComponent(url);
  //
  //     let options = {
  //       label: message,
  //       webButton: {
  //         text: "앱으로 이동",
  //         url: "http://redirect.appzet.com?sharetype=kakaotalk&id=" + this.appService.client.id + "&url=" + encodedUrl
  //       }
  //     };
  //
  //     if (image) {
  //       _.assignIn(options, {image: image});
  //     }
  //     this.appCheck('KAKAOTALK')
  //       .then(() => {
  //         let linkUrl = Kakao.Link.sendTalkLink(options);
  //         cordova.InAppBrowser.open(linkUrl, '_system');
  //       })
  //       .catch((err) => {
  //         if (!err) {
  //           alert.setTitle('실패');
  //           alert.setSubTitle('앱이 설치되지 않았습니다.');
  //           alert.present();
  //         }
  //       })
  //   } else if(this.appService.isMobileWeb) {
  //     if(this.appService.client.isPreview) {
  //       let alert = this.alertController.create({
  //         title: '지원하지 않는 기능',
  //         subTitle: '미리보기에서는 지원하지 않는 기능입니다.',
  //         buttons: ['확인']
  //       });
  //       alert.present();
  //     } else {
  //       let alert = this.alertController.create();
  //       alert.addButton('확인');
  //       let encodedUrl = encodeURIComponent(url);
  //
  //       let options = {
  //         label: message,
  //         webButton: {
  //           text: "웹으로 이동",
  //           url: "http://redirect.appzet.com?sharetype=kakaotalkMobileWeb&id=" + this.appService.client.id + "&url=" + encodedUrl
  //         }
  //       };
  //
  //       if (image) {
  //         _.assignIn(options, {image: image});
  //       }
  //
  //       let linkUrl = Kakao.Link.sendTalkLink(options);
  //       window.open(linkUrl);
  //     }
  //   } else {
  //     let alert = this.alertController.create({
  //       title: '지원하지 않는 기능',
  //       subTitle: '앱에서만 지원되는 기능입니다.',
  //       buttons: ['확인']
  //     });
  //     alert.present();
  //   }
  // }
  //
  // facebook(message: string, url: string, imageUrl?: string,) {
  //   Logger.silly('share.server', 'facebook');
  //   if (this.platformCheck()) {
  //     let alert = this.alertController.create();
  //     alert.addButton('확인');
  //
  //     imageUrl = null;
  //
  //     this.appCheck('FACEBOOK')
  //       .then(success => {
  //         Logger.debug("share.server", success);
  //
  //         let encodedUrl = encodeURIComponent(url);
  //
  //         let newUrl = "http://redirect.appzet.com/index.html?sharetype=facebook&id=" + this.appService.client.id + "&url=" + encodedUrl;
  //
  //         SocialSharing.shareViaFacebookWithPasteMessageHint(message, imageUrl, newUrl, this.pasteMessageHint)
  //           .then(() => {
  //           })
  //       })
  //       .catch((err) => {
  //         if (!err) {
  //           alert.setTitle('실패');
  //           alert.setSubTitle('앱이 설치되지 않았습니다.');
  //           alert.present();
  //         } else {
  //           alert.setTitle('실패');
  //           alert.setSubTitle('공유에 실패하였습니다. 잠시 후 다시 시도해주세요.');
  //           alert.present();
  //         }
  //       })
  //   }
  // }
  //
  // instagram(message: string, imageUrl: string) {
  //   Logger.silly('share.server', 'instagram');
  //   if (this.platformCheck()) {
  //     let alert = this.alertController.create();
  //     alert.addButton('확인');
  //
  //     if (imageUrl) {
  //       this.appCheck('INSTAGRAM')
  //         .then(() => {
  //           Toast.show(this.pasteMessageHint, "3000", "bottom")
  //             .subscribe(
  //               () => {
  //                 setTimeout(() => {
  //                   SocialSharing.shareViaInstagram(message, imageUrl)
  //                     .then(() => {
  //                     })
  //                 }, 1000);
  //               }
  //             );
  //         })
  //         .catch((err) => {
  //           if (!err) {
  //             alert.setTitle('실패');
  //             alert.setSubTitle('앱이 설치되지 않았습니다.');
  //             alert.present();
  //           } else {
  //             alert.setTitle('실패');
  //             alert.setSubTitle('공유에 실패하였습니다. 잠시 후 다시 시도해주세요.');
  //             alert.present();
  //           }
  //         })
  //     } else {
  //       alert.setTitle('실패');
  //       alert.setSubTitle('인스타그램 공유는 사진이 최소 1장 이상이어야 합니다.');
  //       alert.present();
  //     }
  //   }
  // }
  //
  // sms(message: string, url: string, phoneNumber?: string) {
  //   Logger.silly('share.server', 'sms');
  //   if (this.platformCheck()) {
  //
  //     let encodedUrl = encodeURIComponent(url);
  //
  //     let newUrl = "redirect.appzet.com/index.html?sharetype=sms&id=" + this.appService.client.id + "&url=" + encodedUrl;
  //     let newMessage = message + "\n\n" + newUrl;
  //
  //     SocialSharing.shareViaSMS(newMessage, phoneNumber)
  //       .then((success) => {
  //       })
  //       .catch((err) => {
  //         let alert = this.alertController.create({
  //           title: '실패',
  //           subTitle: '공유에 실패하였습니다. 잠시 후 다시 시도해주세요.',
  //           buttons: ['확인']
  //         });
  //         alert.present();
  //       });
  //   }
  // }
  //
  // platformCheck() {
  //   Logger.silly('share.server', 'platformCheck');
  //   if (this.appService.isCordova) {
  //     return true;
  //   } else {
  //     let alert = this.alertController.create({
  //       title: '지원하지 않는 기능',
  //       subTitle: '앱에서만 지원되는 기능입니다.',
  //       buttons: ['확인']
  //     });
  //     alert.present();
  //     return false;
  //   }
  // }
  //
  // appCheck(shareName: string): Promise<boolean> {
  //   Logger.silly('share.server', 'appCheck');
  //   let appScheme;
  //   switch (shareName) {
  //     case 'KAKAOTALK':
  //       if (Device.platform === 'iOS') {
  //         appScheme = 'kakaotalk://';
  //         break;
  //       } else if (Device.platform === 'Android') {
  //         appScheme = 'com.kakao.talk';
  //         break;
  //       }
  //     case 'FACEBOOK':
  //       if (Device.platform === 'iOS') {
  //         appScheme = 'fb://';
  //         break;
  //       } else if (Device.platform === 'Android') {
  //         appScheme = 'com.facebook.katana';
  //         break;
  //       }
  //     case 'INSTAGRAM':
  //       if (Device.platform === 'iOS') {
  //         appScheme = 'instagram://';
  //         break;
  //       } else if (Device.platform === 'Android') {
  //         appScheme = 'com.instagram.android';
  //         break;
  //       }
  //   }
  //   return AppAvailability.check(appScheme);
  // }
}
