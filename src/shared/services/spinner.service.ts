/**
 * Created by andy on 22/03/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 22/03/2017
 *
 * Updater    수정자 - PHILIP 23/03/2017
 */

// Export lib
declare var _;
declare var Logger;

// Logger.domains.push("spinner.service");

export class Spinner {

  private static _instance: Spinner;

  loadingPanel;
  spinnerPanel;

  _c: any[];
  _l: any[];
  _dur: number = null;

  private constructor() {
  }

  public static get Instance() {
    // Do you need arguments? Make it a regular method instead.
    return this._instance || (this._instance = new this());
  }

  public init(name, mode) {
    Logger.silly('spinner.service', 'init');

    this.loadingPanel = document.createElement("div");
    this.loadingPanel.id = "loading-page";

    this.spinnerPanel = document.createElement("div");
    this.spinnerPanel.id = "az-spinner";

    this.spinnerPanel.classList.add("spinner-" + name);
    this.spinnerPanel.classList.add("spinner-" + mode + "-" + name);

    this._l = [];
    this._c = [];

    const spinner = SPINNERS[name];

    if (spinner) {
      if (spinner.lines) {
        for (var i = 0, l = spinner.lines; i < l; i++) {
          this.spinnerPanel.appendChild(this.createSvgLines(spinner, i, l, "line"));
        }

      }
      else if (spinner.circles) {
        for (var i = 0, l = spinner.circles; i < l; i++) {
          this.spinnerPanel.appendChild(this.createSvgLines(spinner, i, l, "circle"));
        }
      }
    }

    this.loadingPanel.appendChild(this.spinnerPanel);
    let body = document.getElementsByTagName("body")[0];
    body.appendChild(this.loadingPanel);
  }

  createSvgLines(spinner, index, total, type) {
    Logger.silly('spinner.service', 'createSvgLines');

    let duration = this._dur || spinner.dur;
    let data = spinner.fn(duration, index, total);
    data.style.animationDuration = duration + 'ms';

    let svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
    svg.setAttribute("viewBox", "0 0 64 64");
    _.assign(svg.style, data.style);

    if (type === "line") {
      let line = document.createElementNS("http://www.w3.org/2000/svg", "line");
      line.setAttribute("transform", "translate(32,32)");
      line.setAttribute("y1", data.y1);
      line.setAttribute("y2", data.y2);
      svg.appendChild(line);
    }

    if (type === "circle") {

      let circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
      circle.setAttribute("transform", "translate(32,32)");
      circle.setAttribute("r", data.r);
      svg.appendChild(circle);
    }

    return svg;
  }

  hide(className?: string) {
    Logger.silly('spinner.service', 'hide');
    if (!this.loadingPanel) return;

    if (className) {
      this.loadingPanel.classList.remove(className);
    }

    this.loadingPanel.classList.add("loaded");
  }

  show(className?: string) {
    Logger.silly('spinner.service', 'show');
    if (!this.loadingPanel) return;

    if (className) {
      this.loadingPanel.classList.add(className);
    }

    this.loadingPanel.classList.remove("loaded");
  }

}

const SPINNERS: any = {

  ios: {
    dur: 1000,
    lines: 12,
    fn: function (dur: number, index: number, total: number) {
      const transform = 'rotate(' + (30 * index + (index < 6 ? 180 : -180)) + 'deg)';
      const animationDelay = -(dur - ((dur / total) * index)) + 'ms';
      return {
        y1: 17,
        y2: 29,
        style: {
          transform: transform,
          webkitTransform: transform,
          animationDelay: animationDelay,
          webkitAnimationDelay: animationDelay
        }
      };
    }
  },

  'ios-small': {
    dur: 1000,
    lines: 12,
    fn: function (dur: number, index: number, total: number) {
      const transform = 'rotate(' + (30 * index + (index < 6 ? 180 : -180)) + 'deg)';
      const animationDelay = -(dur - ((dur / total) * index)) + 'ms';
      return {
        y1: 12,
        y2: 20,
        style: {
          transform: transform,
          webkitTransform: transform,
          animationDelay: animationDelay,
          webkitAnimationDelay: animationDelay
        }
      };
    }
  },

  bubbles: {
    dur: 1000,
    circles: 9,
    fn: function (dur: number, index: number, total: number) {
      const animationDelay = -(dur - ((dur / total) * index)) + 'ms';
      return {
        r: 5,
        style: {
          top: (9 * Math.sin(2 * Math.PI * index / total)) + 'px',
          left: (9 * Math.cos(2 * Math.PI * index / total)) + 'px',
          animationDelay: animationDelay,
          webkitAnimationDelay: animationDelay
        }
      };
    }
  },

  circles: {
    dur: 1000,
    circles: 8,
    fn: function (dur: number, index: number, total: number) {
      const animationDelay = -(dur - ((dur / total) * index)) + 'ms';
      return {
        r: 5,
        style: {
          top: (9 * Math.sin(2 * Math.PI * index / total)) + 'px',
          left: (9 * Math.cos(2 * Math.PI * index / total)) + 'px',
          animationDelay: animationDelay,
          webkitAnimationDelay: animationDelay
        }
      };
    }
  },

  crescent: {
    dur: 750,
    circles: 1,
    fn: function (dur: number) {
      return {
        r: 26,
        style: {}
      };
    }
  },

  dots: {
    dur: 750,
    circles: 3,
    fn: function (dur: number, index: number, total: number) {
      const animationDelay = -(110 * index) + 'ms';
      return {
        r: 6,
        style: {
          left: (9 - (9 * index)) + 'px',
          animationDelay: animationDelay,
          webkitAnimationDelay: animationDelay
        }
      };
    }
  }

};

export const SpinnerService = Spinner.Instance;
