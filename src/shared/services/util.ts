/**
 * Created by Yoon Yong (Andy) Shin on 14/04/2017
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Yoon Yong (Andy) Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoon Yong (Andy) Shin <andy.shin@applicat.co.kr>, 14/04/2017
 *
 */

// Export Lib
declare var Logger;

// Logger.domains.push('util');

export const MODAL_WIDTH = 880;
export const MODAL_HEIGHT = 820;

/**
 * Throttle a function
 *
 * @export
 * @param {*} func
 * @param {number} wait
 * @param {*} [options]
 * @returns
 */

const KEY_PREFIX = "angular2ws";

export class WebStorageUtility {
  static generateStorageKey(key: string): string {
    return `${KEY_PREFIX}_${key}`
  }

  static get(storage: Storage, key: string): any {
    let storageKey = WebStorageUtility.generateStorageKey(key);

    let value = storage.getItem(storageKey);

    return WebStorageUtility.getGettable(value);
  }

  static set(storage: Storage, key: string, value: any): void {
    let storageKey = WebStorageUtility.generateStorageKey(key);

    storage.setItem(storageKey, WebStorageUtility.getSettable(value));
  }

  static remove(storage: Storage, key: string): void {
    let storageKey = WebStorageUtility.generateStorageKey(key);

    storage.removeItem(storageKey);
  }

  private static getSettable(value: any): string {
    return typeof value === "string" ? value : JSON.stringify(value);
  }

  private static getGettable(value: string): any {
    try {
      return JSON.parse(value);
    } catch (e) {
      return value;
    }
  }
}

export function resizeImage(size, url) {
  let urlTemp = url.split('/');
  let imageName = urlTemp[urlTemp.length - 1];


  let resizeQuery = 'w' + Math.ceil(size) + '/';
  return url.replace(imageName, resizeQuery + imageName);
}

export function throttle(func: any, wait: number, options?: any) {
  Logger.silly('util');
  options = options || {};
  let context;
  let args;
  let result;
  let timeout = null;
  let previous = 0;

  function later() {
    Logger.silly('util');
    previous = options.leading === false ? 0 : +new Date();
    timeout = null;
    result = func.apply(context, args);
  }

  return function () {
    let now = +new Date();

    if (!previous && options.leading === false) {
      previous = now;
    }

    let remaining = wait - (now - previous);
    context = this;
    args = arguments;

    if (remaining <= 0) {
      clearTimeout(timeout);
      timeout = null;
      previous = now;
      result = func.apply(context, args);
    } else if (!timeout && options.trailing !== false) {
      timeout = setTimeout(later, remaining);
    }

    return result;
  };
}

/**
 * Throttle decorator
 *
 *  class MyClass {
 *    throttleable(10)
 *    myFn() { ... }
 *  }
 *
 * @export
 * @param {number} duration
 * @param {*} [options]
 * @returns
 */
export function throttleable(duration: number, options?: any) {
  Logger.silly('util', 'throttleable');
  return function innerDecorator(target, key, descriptor) {
    return {
      configurable: true,
      enumerable: descriptor.enumerable,
      get: function getter() {
        Object.defineProperty(this, key, {
          configurable: true,
          enumerable: descriptor.enumerable,
          value: throttle(descriptor.value, duration, options)
        });

        return this[key];
      }
    };
  };
}
