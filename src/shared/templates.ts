var templates = { 
'app/app.component' : `<!--
/**
 * Created by andy on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */
-->


<ion-nav [root]="rootPage">

</ion-nav>


`,
'public/index' : `<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="UTF-8">
  <title>Ionic App</title>
  <meta http-equiv="pragma" content="no-cache"/>
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta name="format-detection" content="telephone=no">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="theme-color" content="#141414">

  <link rel="icon" type="image/x-icon" href="assets/icon/favicon.ico">
  <link rel="manifest" href="manifest.json">

  <script src="cordova.js"></script>

  <!-- un-comment this code to enable service worker
  <script>
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('service-worker.js')
        .then(() => Logger.debug('service worker installed'))
        .catch(err => Logger.debug('Error', err));
    }
  </script>-->
  <!-- google Map -->
  <link href="assets/ionic/ionic.min.css" rel="stylesheet">
  <link href="assets/ionic/ionicons.min.css" rel="stylesheet">
  <link href="build/main.css" rel="stylesheet">

</head>
<body>

<!-- Ionic's root component and where the app will load -->
<ion-app></ion-app>


<!-- Daum map api -->
<script type="text/javascript"
        src="https://apis.daum.net/maps/maps3.js?apikey=1d77329135df78c95c219758f5fdddfb&libraries=services"></script>

<!-- The polyfills js is generated during the build process -->
<!--<script src="build/polyfills.js"></script>-->

<!-- The bundle js is generated during the build process -->
<script src="build/global.js"></script>
<script src="build/vendor.js"></script>
<script src="build/main.js"></script>

</body>
</html>
`,
'pages/profile/profile' : `<!--
/**
 * Created by wonseok Lee on 16/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 16/08/2017
 *
 * Updater    수정자 - wonseok Lee 16/08/2017
 */
-->     

<ion-header class="profile-header">
    <ion-navbar hideBackButton="true">
        <ion-buttons left>
            <button class="btn-back"
                    (click)="goBack()">
                <img src="assets/img/register_back_arrow.png"/>
            </button>
        </ion-buttons>
        <ion-title>
            <div>프로필 작성</div>
        </ion-title>
        <ion-buttons right>
        </ion-buttons>

    </ion-navbar>
</ion-header>
<ion-content (ionScroll)="isTopChk()">

<div class="profile-content">
	<div class="top-fix-icon" 
					(click)="goTop()" 
					[ngClass]="{'hide':!topState}">
					<img src="assets/img/ic_scroll_to_top.png">
	</div>

	<div class="video-upload">
		<div class="no-video" *ngIf="!videoState">
			<img src="assets/img/ic_add_profile_video.png">
			<p>매력을 어필할 수 있는</p>
			<p>15초 영상을 등록하세요</p>
		</div>
		<div class="registered" *ngIf="videoState">
						<!-- <img src="assets/img/test.jpg" alt=""> -->
						test
		</div>
	</div>

	<div class="uploaded">
		<img (click)="goVideoReg('new')" src="assets/img/ic_add_video.png">
		<img (click)="goVideoReg('new')" src="assets/img/ic_add_video.png">
		<img (click)="goVideoReg('new')" src="assets/img/ic_add_video.png">
	</div>

	<div class="profile-container">
		<div class="input-panel">
			<div class="red-start"></div>
			<input type="text"
							name="introduce"
							placeholder="소개글"
							[(ngModel)]="joinForm.introduce"
							(focus)="focused($event)"
							(blur)="blured($event)"/>
		</div>
		<div class="input-panel">
			<div class="red-start">*</div>
			<input type="text"
							name="nickname"
							placeholder="닉네임"
							[(ngModel)]="joinForm.nickName"
							(focus)="focused($event)"
							(blur)="blured($event)"/>
		</div>
		<div class="input-panel">
			<div class="red-start">*</div>
			<div class="select" 
					[ngClass]="{'none-selected':!joinForm.age}" 
					(click)="openSelect(actAge)">
				<span *ngIf="!joinForm.age">나이</span>
				<span *ngIf="joinForm.age">{{joinForm.age}}</span>
			</div>
		</div>
		<div class="input-panel">
			<div class="red-start">*</div>
			<div class="select" 
					[ngClass]="{'none-selected':!joinForm.location}" 
					(click)="openSelect(actLocation)">
				<span *ngIf="!joinForm.location">지역</span>
				<span *ngIf="joinForm.location">{{joinForm.location}}</span>
			</div>
		</div>
		<div class="input-panel">
			<div class="red-start"></div>
			<input type="text"
							name="job"
							placeholder="직업"
							(focus)="focused($event)"
							(blur)="blured($event)"/>
		</div>
		<div class="input-panel">
			<div class="red-start">*</div>
			<div class="select" 
					[ngClass]="{'none-selected':!joinForm.bodyType}" 
					(click)="openSelect(actBodyType)">
				<span *ngIf="!joinForm.bodyType">체형</span>
				<span *ngIf="joinForm.bodyType">{{joinForm.bodyType}}</span>
			</div>
		</div>
		<div class="input-panel">
			<div class="red-start">*</div>
			<div class="select" 
					[ngClass]="{'none-selected':!joinForm.tall}" 
					(click)="openSelect(actTall)">
				<span *ngIf="!joinForm.tall">키</span>
				<span *ngIf="joinForm.tall">{{joinForm.tall}}</span>
			</div>
		</div>
		<div class="input-panel">
			<div class="red-start"></div>
			<div class="select" 
					[ngClass]="{'none-selected':!joinForm.faith}" 
					(click)="openSelect(actFaith)">
				<span *ngIf="!joinForm.faith">종교</span>
				<span *ngIf="joinForm.faith">{{joinForm.faith}}</span>
			</div>
		</div>
		<div class="input-panel">
			<div class="red-start"></div>
			<input type="text"
							name="school"
							placeholder="학교"
							(focus)="focused($event)"
							(blur)="blured($event)"/>
		</div>
		<div class="input-panel">
			<div class="red-start"></div>
			<div class="select" 
					[ngClass]="{'none-selected':!joinForm.drinking}" 
					(click)="openSelect(actDrinking)">
				<span *ngIf="!joinForm.drinking">음주</span>
				<span *ngIf="joinForm.drinking">{{joinForm.drinking}}</span>
			</div>
		</div>
		<div class="input-panel">
			<div class="red-start"></div>
			<div class="select" 
					[ngClass]="{'none-selected':!joinForm.smoking}" 
					(click)="openSelect(actSmoking)">
				<span *ngIf="!joinForm.smoking">흡연</span>
				<span *ngIf="joinForm.smoking">{{joinForm.smoking}}</span>
			</div>
		</div>
		<div class="input-panel">
			<div class="red-start"></div>
			<input type="text"
							name="interest"
							placeholder="관심사"
							(focus)="focused($event)"
							(blur)="blured($event)"/>
		</div>
	</div>

	<div class="submit-container">
		<button (click)="goMain()">확인</button>
	</div>

</div>
</ion-content>`,
'pages/login/login' : `<!--
/**
 * Created by wonseok Lee on 16/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 16/08/2017
 *
 * Updater    수정자 - wonseok Lee 16/08/2017
 */
-->

<div class="login-content">

  <div class="logo">
    사부작
  </div>

  <div class="login-container">
    <div class="input-panel">
      <input type="text"
             name="email"
             [(ngModel)]="identifier"
             placeholder="이메일"/>
    </div>
    <div class="input-panel">
      <input type="password"
             name="password"
             [(ngModel)]="password"
             placeholder="비밀번호"/>
    </div>
  </div>
  <div class="find-user">
    로그인이 안되나요?
  </div>
  <div class="submit-container">
    <button 
      [disabled]="!isValid() || loading" (click)="login()">로그인</button>
  </div>

  <div class="oauth-container">
    <div class="oauth-item" (click)="goSocial()">
      <img src="assets/img/login_sns_facebook.png"/>
      <div>페이스북 로그인</div>
    </div>
    <div class="oauth-item" (click)="socialLogin()">
      <img src="assets/img/login_sns_kakao.png"/>
      <div>카카오톡 로그인</div>
    </div>
  </div>
  <div class="join-container"
       (click)="toRegister()">
    회원가입
  </div>
</div>
`,
'pages/sabuzak/sabuzak' : `<!--
/**
 * Created by wonseok Lee on 17/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/2017
 *
 * Updater    수정자 - wonseok Lee 17/08/2017
 */
-->     
<ion-menu id="sabuzakMenu" class="sabuzak-menu-container" type="overlay" [content]="sabuzakContent">
  <div class="badge chat"
       (click)="pagePusher(pageList.chatList)">
    <img src="assets/img/drawer_chat.png">
    <div class="alert">2</div>
  </div>
  <div class="badge alert"
       (click)="pagePusher(pageList.noticeList)">
    <img src="assets/img/drawer_alert.png">
    <div class="alert">100</div>
  </div>
  
  <ion-content>
    <div class="menu-header">
      <div class="profile-pic"
           (click)="goMyProfile(pageList.viewProfile)">

      </div>
      <div class="nickname">
        {{credential.user.nickName}}
      </div>
      <ul class="info">
        <li (click)="goMyProfile(pageList.viewProfile)">
          <div class="label">내상태</div>
          <div class="detail">
            <span>{{credential.user.rank}}</span>등급</div>
        </li>
        <li (click)="pagePusher(pageList.rechargeBall)">
          <div class="label">구슬</div>
          <div class="detail">
            <span>{{credential.user.totalBall}}</span>개</div>
        </li>
      </ul>
    </div>
    <ul class="menu-content">
      <li (click)="pageSetRoot(pageList.main)">
        <img src="assets/img/drawer_home.png">
        홈
      </li>
      <!-- <li (click)="goHome()">
        <img src="assets/img/drawer_home.png">
        홈
      </li> -->
      <li (click)="pagePusher(pageList.searchLiker)">
        <img src="assets/img/drawer_find_people.png">
        나를 좋아하는 이성 찾기
      </li>
      <li (click)="pagePusher(pageList.dreamConfig)">
        <img src="assets/img/drawer_mytype.png">
        이상형 설정
      </li>
      <li (click)="pagePusher(pageList.rechargeBall)">
        <img src="assets/img/drawer_market.png">
        구슬 충전소
      </li>
      <li (click)="pagePusher(pageList.settings)">
        <img src="assets/img/drawer_setting.png">
        환경설정
      </li>
      <li>
        <img src="assets/img/drawer_question.png">
        문의
      </li>
    </ul>
  </ion-content>
</ion-menu>
<ion-nav id="nav" #sabuzakContent [root]="indexPage"></ion-nav>`,
'pages/profile/video-register/video-register' : `<!--
/**
 * Created by wonseok Lee on 17/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/2017
 *
 * Updater    수정자 - wonseok Lee 17/08/2017
 */
-->     

<ion-header class="video-register-header">
  <ion-navbar hideBackButton="true">
      <ion-buttons left>
          <button class="btn-back"
                  (click)="goBack()">
              <img src="assets/img/register_back_arrow.png"/>
          </button>
      </ion-buttons>
      <ion-title>
          <div>동영상 등록</div>
      </ion-title>
      <ion-buttons right>
      </ion-buttons>

  </ion-navbar>
</ion-header>
<ion-content>

<div class="video-register-content">
  
    <div class="video-upload">
      <div class="no-video" *ngIf="!videoState">
        <img src="assets/img/ic_add_profile_video.png">
        <p>매력을 어필할 수 있는</p>
        <p>15초 영상을 등록하세요</p>
      </div>
      <div class="registered" *ngIf="videoState">
        test
      </div>
    </div>

    <div class="comment-container">
      대표컷을 등록해주세요
    </div>

    <div class="uploaded">
        <img src="assets/img/ic_add_video.png">
        <img src="assets/img/ic_add_video.png">
        <img src="assets/img/ic_add_video.png">
        <img src="assets/img/ic_add_video.png">
    </div>
    <div class="submit-container">
      <button (click)="goBack()">확인</button>
    </div>

  </div>
</ion-content>`,
'pages/login/agreement/agreement' : `<!--
/**
 * Created by JongIn Koo on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by JongIn Koo <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */
-->

<ion-header class="agreement-header">
  <ion-navbar hideBackButton>
    <ion-buttons left>
      <button class="btn-back"
              (click)="goBack()">
        <img src="assets/image/ic_back.png"/>
      </button>
    </ion-buttons>
    <ion-title>
      <div>약관동의</div>
    </ion-title>
    <ion-buttons right>
    </ion-buttons>

  </ion-navbar>
</ion-header>

<ion-content class="agreement-content">

  <div class="check-container" (click)="servicePolicy = !servicePolicy">
    <img [src]="servicePolicy? 'assets/image/ic_check_active.png':'assets/image/ic_check_none.png'"/>
    <div> 서비스 이용약관 동의</div>
  </div>

  <div class="policy-container"
       [perfect-scrollbar]="{ suppressScrollX: false, suppressScrollY: false }">
<pre>[1장 총칙]

제 1 조 (목적)

본 약관은 이용고객이 로프(이하 “회사”라 합니다)가 구글플레이스토어를 통해 제공하는 앱 “4972 baton”(이하 “4972 baton”이라 합니다) 브랜드를 통해 제공하는 “서비스”를 이용함에 있어 회사와 이용고객간의 권리와 의무 및 책임사항을 규정함을 목적으로 합니다.


제 2 조 (정의)

본 약관에서 사용하는 주요 용어는 다음과 같습니다.

① “4972 baton”브랜드라 함은 회사가 교통사고 현장출동조사서비스, 견인 서비스, 자동차 긴급출동 서비스, 자동차 정비/수리 서비스 및 그 외에 퀵 배달서비스, 렌트카 서비스, 대리운전 등을 이용고객에게 제공하기 위하여 컴퓨터 등 정보통신 설비를 이용하여 재화, 용역을 거래할 수 있도록 설정한 가상의 영업장 또는 회사의 웹사이트 운영을 통칭 합니다. 아울러 사고조사 및 보상조치 서비스와 이에 따른 긴급출동, 정비, 수리를 위한 정비소와 위치 찾기 서비스의 브랜드를 말하며, 또한 ”4972 baton”브랜드를 운영하는 사업자의 의미로도 사용합니다.

② “서비스”라 함은, 구현되는 단말기(PC, 태블릿, 휴대용 단말기 등의 각 종 유무선 장치)와 상관없이 브랜드명 ‘4972 baton”을 사용하여 회사가 제공하는 제반 서비스를 말합니다.

③ “이용고객” 이라 함은, “4972 baton”에 접속 또는 오프라인“4972 baton”브랜드와 협약된 매장에 방문하여 본 약관에 따라 회사가 제공하는 서비스를 받는 회원과 비회원을 말합니다.

④ “가맹점”이라 함은, “이용고객”에게 정비, 수리 등의 자동차 제반 서비스를 제공하는 정비업소, 정비기사, 출동차량, 출동기사, 회사에 가입한 회원(앱으로 등록한 교통사고 현장출동등록자 및 보상담당자 포함)등이 “4972 baton” 앱 서비스“ 회사에 정상적으로 사용등록한자를 말합니다.

⑤ “게시물” 이라 함은, “이용고객”이 “서비스”를 이용함에 있어 “서비스상”에 게시한 부호, 문자, 음향, 음성, 화상, 동영상 등의 정보 형태의 글, 사진, 동영상 및 각종 파일과 링크 등을 의미합니다.

⑥ “주문거래”라 함은 이용고객이 서비스를 이용함에 있어 결제서비스를 이용하여 매매거래가 이루어지는 일체를 말합니다.


제 3 조 (약관의 게시, 효력 및 개정)

① 회사는 본 약관의 내용을 이용고객이 쉽게 알 수 있도록 서비스 화면에 게시합니다.

② 회사는 콘텐츠산업 진흥법, 전자상거래 등에서의 소비자보호에 관한 법률, 약관의 규제에 관한 법률, 소비자기본법 등 관련법을 위배하지 않는 범위에서 본 약관을 개정할 수 있습니다.

③ 회사가 약관을 개정할 경우에는 기존약관과 개정약관 및 개정약관의 적용일자와 개정사유를 명시하여 현행약관과 함께 그 적용일자 삼십(30)일 전부터 일정기간 서비스 내 공지하고, 로그인시 동의창, 이용고객의 이메일 등의 전자적 수단을 통해 고지합니다.

④ 회사가 전항에 따라 이용고객에게 통지하면서 공지 또는 공지·고지일로부터 개정약관 시행일 7일 후까지 거부의사를 표시하지 않으면 승인한 것으로 본다는 뜻을 명확하게 고지하였음에도, 이용고객이 명시적으로 거부의 의사 표시를 하지 않은 경우에는 변경된 약관을 승인한 것으로 봅니다.

⑤ 이용고객이 개정약관에 동의하지 않을 경우 이용고객은 제26조 제1항의 규정에 따라 이용계약을 해지할 수 있습니다.

⑥ 본 약관에서 정하지 아니한 사항과 이 약관의 해석에 관하여는 정부가 제정한 전자거래소비자보호지침 및 관계법령 또는 상관례에 따릅니다.


[2장 서비스의 이용]


제 4 조 (서비스 이용)

① 서비스 이용은 회사의 서비스 사용 승낙 직후부터 가능합니다.

② 서비스 이용시간은 회사의 업무상 또는 기술상 불가능한 경우를 제외하고는 연중무휴 1일 24시간(00:00-24:00)으로 함을 원칙으로 합니다. 다만, 서비스 설비의 정기점검 등의 사유로 회사가 서비스를 특정범위로 분할하여 별도로 날짜와 시간을 정할 수 있습니다.


제 5 조 (서비스 내용 변경통지 등)

회사가 서비스 내용을 변경하거나 종료하는 경우 회사는 서비스의 공지사항, 이용고객 휴대전화 문자메시지, 이용고객의 이메일 등의 전자적 방식을 통하여 서비스 내용의 변경 사항 또는 종료를 통지할 수 있습니다.


제 6 조 (서비스 이용의 제한 및 중지)

① 회사는 다음 사유가 발생한 경우에는 이용고객의 서비스 이용을 제한하거나 중지시킬 수 있습니다.

1. 이용고객이 회사 서비스의 운영을 고의 또는 중과실로 방해하는 경우

2. 이용고객이 제8조의 의무를 위반한 경우

3. 서비스용 설비 점검, 보수 또는 공사로 인하여 부득이한 경우

4. 전기통신사업법에 규정된 기간통신사업자가 전기통신 서비스를 중지했을 경우

5. 국가비상사태, 서비스 설비의 장애 또는 서비스 이용의 폭주 등으로 서비스 이용에 지장이 있는 때

6. 기타 중대한 사유로 인하여 회사가 서비스 제공을 지속하는 것이 부적당하다고 인정하는 경우

② 회사는 “주민등록법"을 위반한 명의도용 및 결제도용, 전화번호 도용, "저작권법" 및 "컴퓨터프로그램보호법"을 위반한 불법프로그램의 제공 및 운영방해, "정보통신망법"을 위반한 불법통신 및 해킹, 악성프로그램의 배포, 접속권한 초과행위 등과 같이 관련법을 위반한 경우에는 즉시 영구이용정지를 할 수 있습니다. 본 항에 따른 영구이용정지 시 서비스 이용을 통해 획득한 혜택 등도 모두 소멸되며, 회사는 이에 대해 별도로 보상하지 않습니다.

③ 회사는 본 조에 따라 서비스의 이용을 제한하거나 중지한 때에는 그 사유 및 제한 기간 등을 제5조 [서비스 내용 변경통지 등]에 따라 이용고객에게 통지합니다.

④ 이용고객은 본 조에 따른 이용제한 등에 대해 회사가 정한 절차에 따라 이의신청을 할 수 있습니다. 이 때 이의가 정당하다고 회사가 인정하는 경우 회사는 즉시 서비스의 이용을 재개합니다.


제 7조 (권리의 귀속 및 저작물의 이용)

① 회사가 이용고객에게 제공하는 각종 서비스에 대한 저작권을 포함한 일체의 권리는 회사에 귀속되며 이용고객이 서비스를 이용하는 과정에서 작성한 게시물에 대한 저작권을 포함한 일체에 관한 권리는 별도의 의사표시가 없는 한 해당 이용고객에게 귀속됩니다. (다만, 050 안심번호 서비스와 번호 소유권 및 사용권은 회사와 체결된 파트너사의 별정통신사업 이용약관에 따라 별도의 통보 없이 이를 제공하는 파트너사의 승인과 거절 및 취소가 될 수도 있습니다.)

② 회사는 서비스의 운영, 전시, 전송, 배포, 홍보 의 목적으로 이용고객의 별도의 허락 없이 무상으로 저작권법에 규정하는 공정한 관행에 합치되게 합리적인 범위 내에서 다음과 같이 이용고객이 등록한 게시물을 사용할 수 있습니다.


– 다 음 –

서비스 내에서 이용고객의 게시물의 복제, 수정, 개조, 전시, 전송, 배포 및 저작물성을 해치지 않는 범위 내에서의 편집 저작물 작성 미디어, 통신사 등 서비스 제휴 파트너에게 회원의 게시물 내용을 제공, 전시 혹은 홍보하게 하는 것.


제 8 조 (회원 및 비회원 이용고객의 의무)

① 회원 및 비회원 이용고객은 다음 행위를 하여서는 안 됩니다.

1. 회원정보에 허위 내용을 등록하는 행위

2. 회사의 서비스에 게시된 정보를 변경하거나 서비스를 이용하여 얻은 정보를 회사의 사전 승낙 없이 영리 또는 비영리의 목적으로 복제, 출판, 방송 등에 사용하거나 제3자에게 제공하는 행위

3. 회사나 기타 제3자의 명예를 훼손하거나 지적재산권을 침해하는 등 회사나 제3자의 권리를 침해하는 행위

4. 다른 회원의 회원 정보 또는 이용고객의 정보를 도용하여 부당하게 서비스를 이용한 경우

5. 정크메일(junk mail), 스팸메일(spam mail), 행운의 편지(chain letters), 피라미드 조직에 가입할 것을 권유하는 메일, 외설 또는 폭력적인 메시지 ·화상·음성 등이 담긴 메일을 보내거나 기타 미풍양속에 반하는 정보를 공개 또는 게시하는 행위

6. 정보통신망 이용촉진 및 정보보호 등에 관한 법률 등 관련 법령에 의하여 그 전송 또는 게시가 금지되는 정보(컴퓨터 프로그램 등)를 전송하거나 게시하는 행위

7. 공공질서 또는 미풍양속에 위배되는 내용의 정보, 문장, 도형, 음성 등을 유포하는 행위

8. 회사의 직원이나 서비스의 관리자를 가장하거나 사칭하여 또는 타인의 명의를 도용하여 글을 게시하거나 메시지를 발송하는 행위

9. 컴퓨터 소프트웨어, 하드웨어, 전기통신 장비의 정상적인 가동을 방해, 파괴할 목적으로 고안된 소프트웨어 바이러스, 기타 다른 컴퓨터 코드, 파일, 프로그램을 포함하고 있는 자료를 게시하거나 전자우편, SMS로 발송하는 행위

10. 스토킹(stalking) 등 다른 이용고객의 서비스 이용을 방해하는 행위

11. 다른 이용고객의 개인정보를 그 동의 없이 수집, 저장, 공개하는 행위

12. 불특정 다수의 이용고객을 대상으로 하여 광고 또는 선전을 게시하거나 스팸메일을 전송하는 등의 방법으로 회사에서 제공하는 서비스를 이용하여 영리목적의 활동을 하는 행위

13. 현행 법령, 회사가 제공하는 서비스에 정한 약관 기타 서비스 이용에 관한 규정을 위반하는 행위


② 회사는 회원 또는 비회원 이용고객이 제1항의 행위를 하는 경우 서비스의 이용을 제한하거나 일방적으로 본 계약을 해지할 수 있습니다.


제 9 조 (회사의 의무)

① 회사는 회사의 서비스 제공 및 보안과 관련된 설비를 지속적이고 안정적인 서비스 제공에 적합하도록 유지, 점검 또는 복구 등의 조치를 성실히 이행하여야 합니다.

② 회사는 서비스의 제공과 관련하여 알게 된 이용고객의 개인정보를 본인의 승낙 없이 제3자에게 누설, 배포하지 않고, 이를 보호하기 위하여 노력합니다. 회원의 개인정보보호에 관한 기타의 사항은 정보통신망 이용촉진 및 정보보호 등에 관한 법률 등 관계법령 및 회사가 별도로 정한 “개인정보취급방침”에 따릅니다.


제 10조 (“4972 baton”앱서비스에서 연결되는 웹사이트 간의 관계)

① “4972 baton”과 연결된 웹사이트란 하이퍼 링크(예: 하이퍼 링크의 대상에는 문자, 그림 및 동화상 등이 포함됨)방식 등으로 연결된 경우를 말합니다.

② 회사는 연결된 웹사이트가 독자적으로 제공하는 재화, 용역으로 이용고객과 해당 웹사이트가 행하는 거래에 대해서 보증책임을 포함하여 일체의 책임을 지지 않습니다


제 11 조 (양도금지)

① 이용고객의 서비스 받을 권리는 원칙적으로 이를 양도 내지 증여하거나 질권의 목적으로 제공 할 수 없습니다

② 다만, 회사에서 제공하는 양도서비스를 통해서 타인에게 양도 될 수 있습니다.


[3장 위치기반서비스]


제12조 (위치기반서비스의 내용)

회사는 위치정보사업자로부터 제공받은 위치정보수집대상의 위치정보를 이용하여 다음 각 호와 같은 내용의 위치기반서비스를 제공합니다

1. 현재 위치를 활용한 검색결과 제공: 개인위치정보주체의 현 위치를 기준으로 근거리 및 서비스가 가능한 업체 등의 검색결과를 제공합니다.

2. 현재 위치를 활용한 서비스 제공 업체와의 이동시간 제공: 개인위치정보주체의 현 위치를 기준으로 서비스가 가능한 업체와의 상대적인 이동시간 등을 제공합니다.


제13조 (위치정보 수집방법)

회사는 다음과 같은 방식으로 개인위치정보를 수집합니다.

1. 휴대폰 단말기를 이용한 기지국 기반(Cell ID방식)의 실시간 위치정보 수집

2. GPS칩이 내장된 전용 단말기를 통해 수집되는 GPS 정보를 통한 위치정보 수집

3. Wi-Fi칩이 내장된 전용 단말기를 통해 수집되는 WPS 정보를 통한 위치정보 수집

4. 기타 위치를 식별할 수 있는 장치를 통해 수집되는 위치정보 수집


제14조 (위치기반서비스의 이용)

① 위치기반서비스의 이용은 연중무휴 1일 24시간을 원칙으로 합니다. 다만, 회사의 업무상이나 기술상의 이유로 서비스가 일지 중지될 수 있고, 또한 운영상의 목적으로 회사가 정한 기간에는 서비스가 일시 중지될 수 있습니다. 이러한 경우 회사는 사전 또는 사후에 이를 공지합니다.

② 회사는 위치기반서비스를 일정범위로 분할하여 각 범위 별로 이용 가능한 시간을 별도로 정할 수 있으며 이 경우 그 내용을 공지합니다.

③ 회사는 전시·사변·천재지변 또는 이에 준하는 국가비상사태가 발생하거나 발생할 우려가 있는 경우와 전기통신사업법에 의한 기간통신사업자가 전기통신서비스를 중지하는 등 기타 부득이한 사유가 있는 경우에는 위치기반서비스의 전부 또는 일부를 제한하거나 정지할 수 있습니다.

④ 회사는 제4항의 규정에 의하여 위치기반서비스의 이용을 제한하거나 정지한 때에는 그 사유 및 제한기간 등을 지체 없이 이용고객에게 알리도록 노력해야 합니다.

⑤ 제3항에 의한 장애로 인하여 위치기반서비스가 제한 또는 정지된 경우에 회사는 일체의 책임을 지지 않습니다.


제15조 (개인위치정보의 이용 또는 제공)

① 개인위치정보를 이용하는 회사는 아래와 같습니다.

가. 상호 : 로프(rope)

나. 대표자: 조 성 수

다. 주소 :

라. 대표전화: 0506-610-4994

② 회사는 개인위치정보를 이용하여 서비스를 제공하고자 하는 경우에는 미리 이용약관에 명시한 후 개인위치정보주체의 동의를 얻어야 합니다.

③ 이용고객 및 법정대리인의 권리와 그 행사방법은 제소 당시의 이용고객의 주소에 의하며, 주소가 없는 경우에는 거소를 관할하는 지방법원의 전속관할로 합니다. 다만, 제소 당시 이용고객의 주소 또는 거소가 분명하지 않거나 외국 거주자의 경우에는 민사소송법상의 관할 법원에 제기합니다.

④ 회사는 위치정보의 보호 및 이용 등에 관한 법률 제16조 제2항에 근거하여 개인위치정보주체에 대한 위치정보 수집·이용·제공사실 확인 자료를 위치정보시스템에 자동으로 기록하며, 1년간 보관합니다.

⑤ 회사는 개인위치정보를 이용고객이 지정하는 제3자에게 제공하는 경우에는 개인위치정보를 수집한 당해 통신 단말장치로 매회 이용고객에게 제공받는 자, 제공일시 및 제공목적을 즉시 통보합니다. 단, 다음 각 호의 어느 하나에 해당하는 경우에는 이용고객이 미리 특정하여 지정한 통신 단말장치 또는 전자우편주소로 통보합니다.

1. 개인위치정보를 수집한 당해 통신단말장치가 문자, 음성 또는 영상의 수신기능을 갖추지 아니한 경우

2. 이용고객이 온라인 게시 등의 방법으로 통보할 것을 미리 요청한 경우


제16조 (개인위치정보주체의 권리)

① 이용고객은 회사에 대하여 언제든지 개인위치정보를 이용한 위치기반서비스 제공 및 개인위치 정보의 제3자 제공에 대한 동의의 전부 또는 일부를 철회할 수 있습니다. 이 경우 회사는 수집한 개인위치정보 및 위치정보 이용, 제공사실 확인 자료를 파기합니다.

② 이용고객은 회사에 대하여 언제든지 개인위치정보의 수집, 이용 또는 제공의 일시적인 중지를 요구할 수 있으며, 회사는 이를 거절할 수 없고 이를 위한 기술적 수단을 갖추고 있습니다.

③ 이용고객은 회사에 대하여 아래 각 호의 자료에 대한 열람 또는 고지를 요구할 수 있고, 당해 자료에 오류가 있는 경우에는 그 정정을 요구할 수 있습니다. 이 경우 회사는 정당한 사유 없이 이용고객의 요구를 거절할 수 없습니다.

1. 본인에 대한 위치정보 수집, 이용, 제공사실 확인자료

2. 본인의 개인위치 정보가 위치정보의 보호 및 이용 등에 관한 법률 또는 다른 법률 규정에 의하여 제 3자에게 제공된 이유 및 내용

④ 이용고객은 제1항 내지 제3항의 권리행사를 위해 회사가 정한 소정의 절차를 통해 요구할 수 있습니다.


제17조 (법정대리인의 권리)

① 회사는 14세 미만의 이용고객에 대해서 법정대리인은 제7조에 의한 이용고객의 권리를 모두 가집니다.

② 회사는 14세 미만의 아동의 개인위치정보 또는 위치정보 이용·제공사실 확인자료를 이용 약관에 명시 또는 고지한 범위를 넘어 이용하거나 제3자에게 제공하고자 하는 경우에는 14세미만의 아동과 그 법정대리인의 동의를 받아야 합니다. 단, 아래의 경우는 제외합니다.

1. 위치정보 및 위치기반서비스 제공에 따른 요금정산을 위하여 위치정보 이용, 제공사실 확인자료가 필요한 경우

2. 통계작성, 학술연구 또는 시장조사를 위하여 특정 개인을 알아볼 수 없는 형태로 가공하여 제공하는 경우


제18조 (8세 이하의 아동 등의 보호의무자의 권리)

① 회사는 아래의 경우에 해당하는 자(이하 “8세 이하의 아동 등”이라 한다)의 보호의무자가 8세 이하의 아동 등의 생명 또는 신체보호를 위하여 개인위치정보의 이용 또는 제공에 동의하는 경우에는 본인의 동의가 있는 것으로 봅니다.

1. 8세 이하의 아동

2. 금치산자

3. 장애인복지법 제2조 제2항 제2호의 규정에 의한 정신적 장애를 가진 자로서 장애인고용촉진 및 직업재활법 제2조 제2호의 규정에 의한 중증장애인에 해당하는 자(장애인 복지법 제29조의 규정에 의하여 장애인 등록을 한 자에 한한다.)

② 8세 이하의 아동 등의 생명 또는 신체의 보호를 위하여 개인위치정보의 이용 또는 제공에 동의를 하고자 하는 보호의무자는 서면동의서에 보호의무자임을 증명하는 서면을 첨부하여 회사에 제출하여야 합니다.

③ 보호의무자는 8세 이하의 아동 등의 개인위치정보 이용 또는 제공에 동의하는 경우 개인위치정보주체 권리의 전부를 행사할 수 있습니다.


제19조 (위치정보관리책임자의 지정)

① 회사는 위치정보를 적절히 관리, 보호하고 개인위치정보주체의 불만을 원활히 처리할 수 있도록 실질적인 책임을 질 수 있는 지위에 있는 자를 위치정보관리 책임자로 지정해 운영합니다.

② 위치정보관리책임자는 위치기반 서비스를 제공하는 부서의 부서장으로서 구체적인 사항은 본 약관의 부칙에 따릅니다.


[4장 기타]


제20조 (개인정보보호)

① 회사는 회원의 개인정보를 보호하기 위하여 정보통신망법 및 개인정보 보호법 등 관계 법령에서 정하는 바를 준수합니다.

② 회사는 회원의 개인정보를 보호하기 위해 "개인정보취급방침"을 수립하고 개인정보 보호 책임자를 지정하여 이를 게시하고 운영합니다.

③ 회사의 공식 사이트 이외의 링크된 사이트에서는 회사의 개인정보취급방침이 적용되지 않습니다. 링크된 사이트 및 구매 상품이나 서비스를 제공하는 제3자의 개인정보 취급과 관련하여는 해당 사이트 및 제3자의 개인정보취급방침을 확인할 책임이 회원에게 있으며, 회사는 이에 대하여 책임을 부담하지 않습니다.

④ 회사는 이용계약을 위하여 이용고객이 제공한 정보를 회사 서비스 운영을 위한 목적 이외의 용도로 사용하거나 이용고객의 동의 없이 제3자에게 제공하지 않습니다. 단, 다음 각 호의 경우에는 예외로 합니다.

1. 법령에 근거하여 회원정보의 이용과 제3자에 대한 정보제공을 허용하고 있는 경우

2. 배송업무 등에 필요한 최소한의 회원정보를 알려 주는 경우

3. 기타 회사의 약관 및 정책에 따라 이용고객의 동의를 구한 경우


제21조 (이용계약의 해지)

① 회원은 언제든지 회사가 정하는 절차에 따라 이용계약 해지 신청을 할 수 있으며, 회사는 관련법 등이 정하는 바에 따라 이를 즉시 처리하여야 합니다.

② 회원이 계약을 해지할 경우, 관련법 및 회사가 별도로 정하는 “개인정보취급방침”에 따라 회사가 회원의 개인정보 등을 보유하는 경우를 제외하고는 해지 즉시 회원의 모든 개인정보 등은 삭제됩니다.

③ 회원이 계약을 해지하는 경우, 회원이 작성한 게시물 중 프로필 등과 같이 본인 계정에 등록된 게시물 일체는 삭제됩니다. 다만, 다른 회원에 의해 담기, 스크랩 등이 되어 재 게시되거나, 공용게시판에 등록된 게시물 등은 삭제되지 않으니 사전에 삭제 후 탈퇴하시기 바랍니다.

④ “회원”이 제8조의 규정을 위반한 경우 회사는 사전통보 없이 일방적으로 본 계약을 해지할 수 있고, 이로 인하여 서비스 운영에 손해가 발생한 경우 이에 대한 민, 형사상 책임도 물을 수 있습니다.


제22조 (면책조항)

① 회사는 다음 사유로 서비스를 제공할 수 없는 경우 이로 인하여 회원에게 발생한 손해에 대해서는 책임을 부담하지 않습니다.

1. 천재지변 또는 이에 준하는 불가항력의 상태가 있는 경우

2. 서비스 제공을 위하여 회사와 서비스 제휴계약을 체결한 제3자의 고의적인 서비스 방해가 있는 경우

3. 회원의 귀책사유로 서비스 이용에 장애가 있는 경우

4. 회원간 또는 회원과 제3자 상호간에 서비스를 매개로 하여 거래 등을 한 경우에 회사의 귀책사유가 없는 경우

5. 기타 회사의 고의·과실이 없는 사유로 인한 경우

② 회사는 이용고객이 서비스를 이용하여 얻은 정보, 자료, 사실의 신뢰도, 정확성 등에 대해서는 보증을 하지 않으며 이로 인해 발생한 이용고객의 손해에 대하여는 책임을 부담하지 않습니다.

③ 회사를 통해 접수를 하지 않아 전산에서 조회가 되지 않는 건에 대해 당사는 책임을 지지 않습니다.

④ 회사는 이용고객이 서비스를 이용하여 기대하는 수익을 얻지 못하거나 상실한 것에 대하여 책임을 지지 않으며, 이용고객 상호간 및 이용고객과 제3자 상호간에 서비스를 매개로 발생한 분쟁에 대해 개입할 의무가 없고, 이로 인한 손해를 배상할 책임도 부담하지 않습니다.

⑤ 수리, 정비 등 이용고객과 수리업체간의 계약 하에 발생한 수리, 정비 등의 하자의 경우 회사는 원칙적으로 배상의 책임이 없으며, 다만 회사는 이용고객과 수리업체간의 적극적 중재역할을 담당합니다.

⑥ 회사는 무료로 제공되는 서비스 이용과 관련하여 관련법에 특별한 규정이 없는 한 책임을 지지 않습니다.


제23조 (분쟁의 해결)

① 회사는 이용고객이 제기하는 정당한 의견이나 불만을 반영하고 그 피해를 보상처리하기 위하여 피해보상처리기구를 설치·운영합니다.

② 회사는 이용고객으로부터 제출되는 불만사항 및 의견은 우선적으로 그 사항을 처리합니다. 다만, 신속한 처리가 곤란한 경우에는 이용고객에게 그 사유와 처리일정을 즉시 통보해 줍니다.

③ 회사와 이용고객 간에 발생한 전자상거래 분쟁과 관련하여 이용고객의 피해구제신청이 있는 경우에는 공정거래위원회 또는 시·도지사가 의뢰하는 분쟁조정기관의 조정에 따를 수 있습니다.


제24조 (재판권 및 준거법)

본 약관은 대한민국법령에 의하여 규정되고 이행되며, 서비스 이용과 관련하여 회사와 이용고객간에 발생한 분쟁에 대해서는 민형사소송법상의 주소지를 관할하는 법원을 합의관할로 합니다.


부칙

제1조. 본 약관은 2017년 07월 00일부터 적용됩니다.

제2조. 회사의 위치정보관리책임자는 다음과 같습니다.

1. 이름: 조 성 수  2. 연락처: 0506-610-4994
</pre>
  </div>

  <div class="check-container" (click)="locationPolicy = !locationPolicy">
    <img [src]="locationPolicy? 'assets/image/ic_check_active.png':'assets/image/ic_check_none.png'"/>
    <div>위치정보제공 동의</div>
  </div>

  <div class="policy-container"
       [perfect-scrollbar]="{ suppressScrollX: false, suppressScrollY: false }">
<pre>
  제 1장. 총 칙

제 1 조 (목적) 본 약관은 회원( 4972 BATON 서비스 약관에 동의한 자를 말합니다. 이하 “회원“이라고 합니다.)

이 로프 (이하 “회사“라고 합니다.)이 제공하는 4972 BATON 서비스(이하 “서비스“라고 합니다)를 이용함에

있어 회사와 회원의 권리·의무 및 책임사항을 규정함을 목적으로 합니다.


제 2 조 (이용약관의 효력 및 변경)

① 본 약관은 서비스를 신청한 고객 또는 개인위치정보주체가 본 약관에 동의하고 회사가 정한 소정의 절차에 따라 서비스의 이용자로 등록함으로써 효력이 발생합니다.

② 회원이 온라인에서 본 약관의 “동의하기” 버튼을 클릭하였을 경우 본 약관의 내용을 모두 읽고 이를 충분히 이해하였으며, 그 적용에 동의한 것으로 봅니다.

③ 회사는 위치정보의 보호 및 이용 등에 관한 법률, 콘텐츠산업 진흥법, 전자상거래 등에서의 소비자보호에 관한 법률, 소비자 기본법 약관의 규제에 관한 법률 등 관련법령을 위배하지 않는 범위에서 본 약관을 개정할 수 있습니다.

④ 회사가 약관을 개정할 경우에는 기존약관과 개정약관 및 개정약관의 적용일자와 개정사유를 명시하여 현행약관과 함께 그 적용일자 10일 전부터 적용일 이후 상당한 기간 동안 공지만을 하고, 개정 내용이 회원에게 불리한 경우에는 그 적용일자 30일 전부터 적용일 이후 상당한 기간 동안 각각 이를 서비스 홈페이지에 게시하거나 회원에게 전자적 형태(전자우편, SMS 등)로 약관 개정 사실을 발송하여 고지합니다.

⑤ 회사가 전항에 따라 회원에게 통지하면서 공지 또는 공지ㆍ고지일로부터 개정약관 시행일 7일 후까지 거부의사를 표시하지 아니하면 이용약관에 승인한 것으로 봅니다. 회원이 개정약관에 동의하지 않을 경우 회원은 이용계약을 해지할 수 있습니다.


제 3 조 (관계법령의 적용) 본 약관은 신의성실의 원칙에 따라 공정하게 적용하며, 본 약관에 명시되지 아니한 사항에 대하여는 관계법령 또는 상관례에 따릅니다.


제 4 조 (서비스의 내용) 회사가 제공하는 서비스는 아래와 같습니다.

서비스 명
서비스 내용
4972 BATON
λ  교통사고 현장 조치 및 도로우회 정보 알림 서비스
λ  대리기사 서비스
λ  퀵 배송 서비스
λ  그 외 찾아가는 서비스


제 5 조 (서비스 이용요금)

① 회사가 제공하는 서비스는 기본적으로 무료입니다. 단, 별도의 유료 서비스의 경우 해당 서비스에 명시된 요금을 지불하여야 사용 가능합니다.

② 회사는 유료 서비스 이용요금을 회사와 계약한 전자지불업체에서 정한 방법에 의하거나 회사가 정한 청구서에 합산하여 청구할 수 있습니다.

③ 유료서비스 이용을 통하여 결제된 대금에 대한 취소 및 환불은 회사의 결제 이용약관 등 관계법에 따릅니다.

④ 회원의 개인정보도용 및 결제사기로 인한 환불요청 또는 결제자의 개인정보 요구는 법률이 정한 경우 외에는 거절될 수 있습니다.

⑤ 무선 서비스 이용 시 발생하는 데이터 통신료는 별도이며 가입한 각 이동통신사의 정책에 따릅니다.

⑥ MMS 등으로 게시물을 등록할 경우 발생하는 요금은 이동통신사의 정책에 따릅니다.


제 6 조 (서비스내용변경 통지 등)

① 회사가 서비스 내용을 변경하거나 종료하는 경우 회사는 회원의 등록된 전자우편 주소로 이메일을 통하여 서비스 내용의 변경 사항 또는 종료를 통지할 수 있습니다.

② ①항의 경우 불특정 다수인을 상대로 통지를 함에 있어서는 웹사이트 등 기타 회사의 공지사항을 통하여 회원들에게 통지할 수 있습니다.


제 7 조 (서비스이용의 제한 및 중지)

① 회사는 아래 각 호의 1에 해당하는 사유가 발생한 경우에는 회원의 서비스 이용을 제한하거나 중지시킬 수 있습니다.

• 1. 회원이 회사 서비스의 운영을 고의 또는 중과실로 방해하는 경우

• 2. 서비스용 설비 점검, 보수 또는 공사로 인하여 부득이한 경우

• 3. 전기통신사업법에 규정된 기간통신사업자가 전기통신 서비스를 중지했을 경우

• 4. 국가비상사태, 서비스 설비의 장애 또는 서비스 이용의 폭주 등으로 서비스 이용에 지장이 있는 때

• 5. 기타 중대한 사유로 인하여 회사가 서비스 제공을 지속하는 것이 부적당하다고 인정하는 경우

② 회사는 전항의 규정에 의하여 서비스의 이용을 제한하거나 중지한 때에는 그 사유 및 제한기간 등을 회원에게 알려야 합니다.


제 8 조 (개인위치정보의 이용 또는 제공)

① 회사는 개인위치정보를 이용하여 서비스를 제공하고자 하는 경우에는 미리 이용약관에 명시한 후 개인위치정보주체의 동의를 얻어야 합니다.

② 회원 및 법정대리인의 권리와 그 행사방법은 제소 당시의 이용자의 주소에 의하며, 주소가 없는 경우에는 거소를 관할하는 지방법원의 전속관할로 합니다. 다만, 제소 당시 이용자의 주소 또는 거소가 분명하지 않거나 외국 거주자의 경우에는 민사소송법상의 관할법원에 제기합니다.

③ 회사는 타사업자 또는 이용 고객과의 요금정산 및 민원처리를 위해 위치정보 이용·제공, 사실 확인자료를 자동 기록·보존하며, 해당 자료는 1년간 보관합니다.

④ 회사는 개인위치정보를 회원이 지정하는 제3자에게 제공하는 경우에는 개인위치정보를 수집한 당해 통신 단말장치로 매회 회원 에게 제공받는 자, 제공 일시 및 제공목적을 즉시 통보합니다. 단, 아래 각 호의 1에 해당하는 경우에는 회원이 미리 특정하여 지정한 통신 단말장치 또는 전자우편주소로 통보합니다.

• 1. 개인위치정보를 수집한 당해 통신단말장치가 문자, 음성 또는 영상의 수신기능을 갖추지 아니한 경우

• 2. 회원이 온라인 게시 등의 방법으로 통보할 것을 미리 요청한 경우


제 9 조 (개인위치정보주체의 권리)

① 회원은 회사에 대하여 언제든지 개인위치정보를 이용한 위치기반서비스 제공 및 개인위치정보의 제3자 제공에 대한 동의의 전부 또는 일부를 철회할 수 있습니다. 이 경우 회사는 수집한 개인위치정보 및 위치정보 이용, 제공사실 확인자료를 파기합니다.

② 회원은 회사에 대하여 언제든지 개인위치정보의 수집, 이용 또는 제공의 일시적인 중지를 요구할 수 있으며, 회사는 이를 거절할 수 없고 이를 위한 기술적 수단을 갖추고 있습니다.

③ 회원은 회사에 대하여 아래 각 호의 자료에 대한 열람 또는 고지를 요구할 수 있고, 당해 자료에 오류가 있는 경우에는 그 정정을 요구할 수 있습니다. 이 경우 회사는 정당한 사유 없이 회원의 요구를 거절할 수 없습니다.

• 1. 본인에 대한 위치정보 수집, 이용, 제공사실 확인자료

• 2. 본인의 개인위치정보가 위치정보의 보호 및 이용 등에 관한 법률 또는 다른 법률 규정에 의하여 제3자에게 제공된 이유 및 내용

④ 회원은 제1항 내지 제3항의 권리행사를 위해 회사의 소정의 절차를 통해 요구할 수 있습니다.


제 10 조 (법정대리인의 권리)

① 회사는 14세 미만의 회원에 대해서는 개인위치정보를 이용한 위치기반서비스 제공 및 개인위치정보의 제3자 제공에 대한 동의를 당해 회원과 당해 회원의 법정대리인으로부터 동의를 받아야 합니다. 이 경우 법정대리인은 제9조에 의한 회원의 권리를 모두 가집니다.

② 회사는 14세 미만의 아동의 개인위치정보 또는 위치정보 이용, 제공사실 확인자료를 이용약관에 명시 또는 고지한 범위를 넘어 이용하거나 제3자에게 제공하고자 하는 경우에는 14세 미만의 아동과 그 법정대리인의 동의를 받아야 합니다. 단, 아래의 경우는 제외합니다.

• 1. 위치정보 및 위치기반서비스 제공에 따른 요금정산을 위하여 위치정보 이용, 제공사실 확인자료가 필요한 경우

• 2. 통계작성, 학술연구 또는 시장조사를 위하여 특정 개인을 알아볼 수 없는 형태로 가공하여 제공하는 경우


제 11 조 (8세 이하의 아동 등의 보호의무자의 권리)

① 회사는 아래의 경우에 해당하는 자(이하 “8세 이하의 아동“등이라 한다)의 보호의무자가 8세 이하의 아동 등의 생명 또는 신체보호를 위하여 개인위치정보의 이용 또는 제공에 동의하는 경우에는 본인의 동의가 있는 것으로 봅니다.

• 1. 8세 이하의 아동

• 2. 금치산자

• 3. 장애인복지법제2조제2항제2호의 규정에 의한 정신적 장애를 가진 자로서 장애인고용촉진및직업재활법 제2조제2호의 규정에 의한 중증장애인에 해당하는 자(장애인복지법 제29조의 규정에 의하여 장애인등록을 한 자에 한한다)

② 8세 이하의 아동 등의 생명 또는 신체의 보호를 위하여 개인위치정보의 이용 또는 제공에 동의를 하고자 하는 보호의무자는 서면동의서에 보호의무자임을 증명하는 서면을 첨부하여 회사에 제출하여야 합니다.

③ 보호의무자는 8세 이하의 아동 등의 개인위치정보 이용 또는 제공에 동의하는 경우 개인위치정보주체 권리의 전부를 행사할 수 있습니다.


제 12조 (위치정보관리책임자의 지정)

① 회사는 위치정보를 적절히 관리·보호하고 개인위치정보주체의 불만을 원활히 처리할 수 있도록 실질적인 책임을 질 수 있는 지위에 있는 자를 위치정보관리책임자로 지정해 운영합니다.

② 위치정보관리책임자는 위치기반서비스를 제공하는 부서의 부서장으로서 구체적인 사항은 본 약관의 부칙에 따릅니다.


제 13 조 (손해배상)

① 회사가 위치정보의 보호 및 이용 등에 관한 법률 제15조 내지 제26조의 규정을 위반한 행위로 회원에게 손해가 발생한 경우 회원은 회사에 대하여 손해배상 청구를 할 수 있습니다. 이 경우 회사는 고의, 과실이 없음을 입증하지 못하는 경우 책임을 면할 수 없습니다.

② 회원이 본 약관의 규정을 위반하여 회사에 손해가 발생한 경우 회사는 회원에 대하여 손해배상을 청구할 수 있습니다. 이 경우 회원은 고의, 과실이 없음을 입증하지 못하는 경우 책임을 면할 수 없습니다.


제 14 조 (면책)

① 회사는 다음 각 호의 경우로 서비스를 제공할 수 없는 경우 이로 인하여 회원에게 발생한 손해에 대해서는 책임을 부담하지 않습니다.

• 1. 천재지변 또는 이에 준하는 불가항력의 상태가 있는 경우

• 2. 서비스 제공을 위하여 회사와 서비스 제휴계약을 체결한 제3자의 고의적인 서비스 방해가 있는 경우

• 3. 회원의 귀책사유로 서비스 이용에 장애가 있는 경우

• 4. 제1호 내지 제3호를 제외한 기타 회사의 고의ㆍ과실이 없는 사유로 인한 경우

② 회사는 서비스 및 서비스에 게재된 정보, 자료, 사실의 신뢰도, 정확성 등에 대해서는 보증을 하지 않으며 이로 인해 발생한 회원의 손해에 대하여는 책임을 부담하지 아니합니다.


제 15 조 (규정의 준용)

① 본 약관은 대한민국법령에 의하여 규정되고 이행됩니다.

② 본 약관에 규정되지 않은 사항에 대해서는 관련법령 및 상관습에 의합니다.


제 16 조 (분쟁의 조정 및 기타)

① 회사는 위치정보와 관련된 분쟁에 대해 당사자간 협의가 이루어지지 아니하거나 협의를 할 수 없는 경우에는 위치정보의 보호 및 이용 등에 관한 법률 제28조의 규정에 의한 방송통신위원회에 재정을 신청할 수 있습니다.

② 회사 또는 고객은 위치정보와 관련된 분쟁에 대해 당사자간 협의가 이루어지지 아니하거나 협의를 할 수 없는 경우에는 개인정보보호법 제43조의 규정에 의한 개인정보분쟁조정위원회에 조정을 신청할 수 있습니다.


제 17 조 (회사의 연락처) 회사의 상호 및 주소 등은 다음과 같습니다.

1. 상     호 : 로 프
2. 대 표 자 :  조 성 수
3. 주     소 : 서울시 종로구 창덕궁1가길 12, 103 (원서동)
2. 대 표 전 화 : 0506-610-4994


부 칙

제1조 (시행일) 이 약관은 2017년 7월15일 부터 시행한다.

제2조 위치정보관리책임자는 2017년 7월15일 을 기준으로 다음과 같이 지정합니다.

1. 소     속 : 로 프  대 표
2. 연 락 처 : 0506-610-4994


</pre>
  </div>

  <div class="remote-read">
    <div class="check-container" (click)="numberPolicy = !numberPolicy">
      <img [src]="numberPolicy? 'assets/image/ic_check_active.png':'assets/image/ic_check_none.png'"/>
      <div>안심번호 이용약관 동의</div>
    </div>
    <div class="other-item"
         (click)="toOnlineRead()">
      <div></div>
      <img src="assets/image/ic_arrow_right_color.png"/>
    </div>
  </div>


  <div class="check-all-container" (click)="checkAll()">
    <img [src]="allChecked()? 'assets/image/ic_check_all_active.png':'assets/image/ic_check_all_none.png'"/>
    <div>전체 약관을 확인하였고 이에 동의합니다.</div>
  </div>

  <div class="submit-container">
    <button
      [disabled]="!allChecked()"
      (click)="toCellPhone()">다음
    </button>
  </div>

</ion-content>
`,
'pages/login/email-sns/email-sns' : `<!--
/**
 * Created by wonseok Lee on 16/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 16/08/2017
 *
 * Updater    수정자 - wonseok Lee 16/08/2017
 */
-->
<ion-content>
  <div class="email-sns-content">

    <div class="logo"></div>

    <div class="email-sns-container">

      <div class="submit-container">
        <button (click)="toRegister()">
          이메일 가입
        </button>
      </div>
    
      <div class="oauth-container">
        <div class="oauth-item">
          <img src="assets/img/login_sns_facebook.png"/>
          <div>페이스북 로그인</div>
        </div>
        <div class="oauth-item">
          <img src="assets/img/login_sns_kakao.png"/>
          <div>카카오톡 로그인</div>
        </div>
      </div>

      
    </div>
    <div class="footer-container"
        (click)="goBack()">
      <img src="assets/img/register_back_arrow.png">
      이전
    </div>
  </div>
</ion-content>`,
'pages/login/number-check/number-check' : `<!--
/**
 * Created by wonseok on 16/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok <whereiswonny@gmail.com>, 16/08/2017
 *
 * Updater    수정자 - wonseok Lee 16/08/2017
 */
-->

<ion-header class="number-check-header">
  <ion-navbar hideBackButton="true">
    <ion-title>
      <div (click)="goBack()">가입취소</div>
      <div>문의하기</div>
    </ion-title>
  </ion-navbar>
</ion-header>

<ion-content>
  <div class="number-check-content">
    <div class="content-view" [style.height]="viewSize.height + 'px'">

      <div class="instruction">
        <p class="title">휴대폰인증</p>
        <p>허위 중복 가입을 막기 위한 절차입니다.</p>
        <p>전화번호는 절대 공개하지 않습니다.</p>
      </div>

      <div class="input-container"
          *ngIf="!testToggle">
        <div class="input-panel">
          <input type="text"
                [(ngModel)]="mobile"
                (ngModelChange)="reset()"
                placeholder="전화번호"/>
        </div>
        <div class="btn-panel">
          <!-- <button [disabled]="!mobile || sent" (click)="sendActivationNumber()">인증번호 요청</button> -->
          <button (click)="testTg()">인증번호 요청</button>
        </div>
      </div>

      <div class="input-container"
          *ngIf="testToggle">
        <div class="input-panel">
          <input type="text"
                [(ngModel)]="code"
                placeholder="인증번호 입력"/>
        </div>
        <div class="btn-panel">
          <!-- <button [disabled]="!code || !sent || checking || activated" (click)="checkActivationNumber()">확인</button> -->
          <button (click)="testNext()">확인</button>
        </div>
      </div>

      <!-- <div class="ruler"></div>

      <div class="safe-number-info-container"
          *ngIf="false">
        <div class="safe-label">
          <img src="assets/image/ic_info.png"/>
          <div>안심번호</div>
        </div>
        <div class="safe-number">1504-2938-3938</div>
      </div> -->

      <!-- <div class="submit-container">
        <button (click)="next()"
                [disabled]="!activated">다음
        </button>
      </div> -->
    </div>
  </div>
</ion-content>`,
'pages/login/number-sent/number-sent' : `<!--
 * Created by Yoon Yong (Andy) Shin on 23/12/2016
 * As part of AppZet
 *
 * Copyright (C) Applicat (www.applicat.co.kr) & Yoon Yong (Andy) Shin - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Yoon Yong (Andy) Shin <andy.shin@applicat.co.kr>, 23/12/2016
 *
-->

<div class="alert-content">
  <div class="header">
    <div class="button-container">
      <button class="button">
      </button>
    </div>
    <div class="title">알림</div>
    <div class="button-container">

    </div>
  </div>
  <div class="content">
    <div class="message">
      <span>{{number | mobile}}</span>
      로
    </div>
    <div class="message">
      인증번호가 전송되었습니다.
    </div>
  </div>
  <div class="footer">
    <button class="submit"
            (click)="dismiss()">확인
    </button>
  </div>
</div>
`,
'pages/login/password-reset/password-reset' : `<!--
/**
 * Created by JongIn Koo on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by JongIn Koo <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */
-->

<ion-header class="passsword-reset-header">
  <ion-navbar hideBackButton>
    <ion-buttons left>
      <button class="btn-back"
              (click)="goBack()">
        <img src="assets/image/ic_back.png"/>
      </button>
    </ion-buttons>
    <ion-title>
      <div>비밀번호 찾기</div>
    </ion-title>
    <ion-buttons right>
    </ion-buttons>

  </ion-navbar>
</ion-header>

<div class="step-1" *ngIf="step == 1">
  <div class="content-view" [style.height]="viewSize.height + 'px'">
    <div class="instruction">가입 시 등록한 휴대폰 번호를 입력하세요.</div>

    <div class="ruler"></div>

    <div class="input-container">
      <div class="input-panel">
        <input type="text"
               [(ngModel)]="mobile"
               (ngModelChange)="reset()"
               placeholder="(-) 없이 휴대폰 번호를 입력해주세요."/>
      </div>
      <div class="btn-panel">
        <button [disabled]="!mobile || sent" (click)="sendActivationNumber()">인증번호 요청</button>
      </div>
    </div>

    <div class="input-container">
      <div class="input-panel">
        <input type="text"
               [(ngModel)]="code"
               placeholder="인증번호를 입력하세요."/>
      </div>
      <div class="btn-panel">
        <button [disabled]="!code || !sent || checking || activated" (click)="checkActivationNumber()">확인</button>
      </div>
    </div>

    <div class="submit-container">
      <button (click)="stepTwo()"
              [disabled]="!activated">다음
      </button>
    </div>

  </div>
</div>

<div class="passsword-reset-content step-2"
     *ngIf="step == 2">
  <div class="content-view" [style.height]="viewSize.height + 'px'">

    <div class="instruction">새로운 비밀번호를 등록하세요.</div>

    <div class="ruler"></div>

    <div class="input-container">
      <div class="input-panel">
        <input type="password"
               [(ngModel)]="newPassword"
               placeholder="새로운 비밀번호 입력 (최소 8자리 이상)"/>
      </div>
    </div>

    <div class="input-container">
      <div class="input-panel">
        <input type="password"
               [(ngModel)]="passwordConfirm"
               placeholder="새로운 비밀번호 확인"/>
      </div>
    </div>


    <div class="submit-container">
      <button (click)="changePasssword()"
              [disabled]="!isValid() || loading">확인
      </button>
    </div>

  </div>
</div>

<div class="step-3"
     *ngIf="step == 3">
  <div class="content-view" [style.height]="viewSize.height + 'px'">


    <div class="note"><img src="assets/image/ic_alert.png"/></div>

    <div class="instruction">비밀번호가 변경되었습니다.</div>

    <div class="login-container">
      <button (click)="goBack()">
        로그인하기
      </button>
    </div>
  </div>
</div>
`,
'pages/login/register/register' : `<!--
/**
 * Created by wonseok Lee on 16/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 16/08/2017
 *
 * Updater    수정자 - wonseok Lee 16/08/2017
 */
-->
<ion-content>


  <div class="register-content">
    
    <div class="register-container">
      <div class="input-panel">
        <input type="text"
              name="email"
              [(ngModel)]="joinForm.email"
              placeholder="이메일"/>
      </div>
      <div class="input-panel">
        <input type="password"
              name="password"
              [(ngModel)]="joinForm.password"
              placeholder="비밀번호"/>
      </div>
      <div class="input-panel">
          <input type="password"
                name="password"
                [(ngModel)]="passwordChk"
                placeholder="비밀번호 확인"/>
        </div>
    </div>

    <div class="gender-container">
      <div class="gender-wrapper male"
          [ngClass]="{active: gender.male}"
          (click)="genderOn('male')">
        <img src="assets/img/register_male_active.png"
            *ngIf="gender.male">
        <img src="assets/img/register_male_nor.png"
            *ngIf="!gender.male">
        남자
      </div>
      <div class="gender-wrapper female"
          [ngClass]="{active: gender.female}"
          (click)="genderOn('female')">
        <img src="assets/img/register_female_active.png"
            *ngIf="gender.female">
        <img src="assets/img/register_female_nor.png"
            *ngIf="!gender.female">
        여자
      </div>
    </div>

    <div class="submit-container"
        (click)="goNext()">
      <button>회원가입</button>
    </div>

    <div class="footer-container"
        (click)="goBack()">
      <img src="assets/img/register_back_arrow.png">
      이전
    </div>
  </div>
</ion-content>`,
'pages/login/social-gender/social-gender' : `<!--
/**
 * Created by wonseok Lee on 22/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 22/08/2017
 *
 * Updater    수정자 - wonseok Lee 22/08/2017
 */
-->
<ion-content>
  <div class="social-gender-content">
    <div class="social-gender-container">
      <div class="notice-wrapper">
        자신의 성별을 선택해주세요
      </div>
      <div class="gender-wrapper">
        <div class="male">
          <img src="assets/img/sns_select_male.png" alt="">
          <span>남자</span>
        </div>
        <div class="female">
          <img src="assets/img/sns_select_female.png" alt="">
          <span>여자</span>
        </div>
      </div>
    </div>
    <div class="footer-container"
        (click)="goBack()">
      <img src="assets/img/register_back_arrow.png">
      이전
    </div>
  </div>
</ion-content>`,
'pages/sabuzak/chat-list/chat-list' : `<!--
/**
 * Created by wonseok Lee on 17/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/2017
 *
 * Updater    수정자 - wonseok Lee 17/08/2017
 */
-->     
<ion-header class="chat-list-header">
  <ion-navbar hideBackButton>
    <ion-buttons left>
      <button class="btn-back"
              (click)="goBack()">
        <img src="assets/img/header_back_arrow_bk.png"/>
      </button>
    </ion-buttons>
    <ion-title>
      <div>대화목록</div>
    </ion-title>
    <ion-buttons right *ngIf="!delState">
      <button (click)="delToggle()">
        편집
      </button>
    </ion-buttons>
    <ion-buttons right *ngIf="delState">
      <button (click)="delToggle()">
        완료
      </button>
    </ion-buttons>
  
  </ion-navbar>
</ion-header>

<ion-content>
    
  <div class="chat-list-content">
    <ul class="list-container">
      <li *ngFor="let i of [1,2,3,4,5,6]">
        <div class="thumbnail-wrapper">
          <!-- <img src="" alt=""> -->
        </div>
        <ul class="message-wrapper"
            (click)="goChatRoom()">
          <li>우루우루사</li>
          <li>
            안녕하세요?안녕하세요?안녕하세요?안녕하세요?안녕하세요?안녕하세요?
            안녕하세요?안녕하세요?안녕하세요?안녕하세요?안녕하세요?안녕하세요?
            안녕하세요?안녕하세요?안녕하세요?
          </li>
        </ul>
        <div class="delete-wrapper">
          <div class="normal-mode"
               *ngIf="!delState">
            <div>오후 5:00</div>
            <div>22</div>
          </div>
          <div class="delete-btn"
               *ngIf="delState">
            삭제
          </div>
        </div>
      </li>
    </ul>
  </div>
    
</ion-content>`,
'pages/sabuzak/chat-room/chat-room' : `<!--
/**
 * Created by wonseok Lee on 17/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/2017
 *
 * Updater    수정자 - wonseok Lee 17/08/2017
 */
-->     
<ion-header class="chat-room-header">
  <ion-navbar hideBackButton>
    <ion-buttons left>
      <button class="btn-back"
              (click)="goBack()">
        <img src="assets/img/header_back_arrow_bk.png"/>
      </button>
    </ion-buttons>
    <ion-title>
      <div>명랑수수</div>
    </ion-title>
  
  </ion-navbar>
</ion-header>
<ion-content>
  <div class="chat-room-content">
    <ul class="message-container">

      <li class="message-wrapper">
        <div class="pic"></div>
        <div class="chat-box">
          <div class="nickname">
            우루우루우루사
          </div>
          <div class="msg-box">
            <div class="msg">
              안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
              안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
              안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
              안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
              안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
              안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
            </div>
            <div class="time">
              오후 5:00
            </div>
          </div>
        </div>
      </li>

      <li class="send-wrapper">
        <div class="time-box">
          <div class="read">1</div>
          <div class="time">오후 5:00</div>
        </div>
        <div class="msg">
            안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
            안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
            안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
            안녕하세요? 반갑습니다^^ 안녕하세요? 반갑습니다^^ 
        </div>
      </li>
      <li></li>
      <li></li>
      <li></li>
    </ul>
    <div class="text-container">
      <textarea name="chatMsg" id="" placeholder="메시지를 입력해주세요"></textarea>
      <!-- <ion-textarea aria-placeholder="메시지를 입력해주세요"></ion-textarea> -->
      <div class="msg-send-btn">전송</div>
    </div>
  </div>
</ion-content>`,
'pages/sabuzak/dream-config/dream-config' : `<!--
/**
 * Created by wonseok Lee on 18/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 18/08/2017
 *
 * Updater    수정자 - wonseok Lee 18/08/2017
 */
-->     

<ion-header class="dream-config-header">
  <ion-navbar hideBackButton="true">

    <ion-buttons left>
      <button class="btn-back"
              (click)="goBack()">
        <img src="assets/img/register_back_arrow.png"/>
      </button>
    </ion-buttons>
    <ion-title>
      <div>이상형 설정</div>
    </ion-title>
    <ion-buttons right>
    </ion-buttons>

  </ion-navbar>
</ion-header>
<ion-content>

<div class="dream-config-content">
  <div class="dream-config-container">
    <div class="age-label">
      <div>나이</div>
      <div>{{ageLenge.lower}}~{{ageLenge.upper}}세</div>
    </div>
    <div class="input-panel range">
      <ion-range dualKnobs="true" 
                  min="18" 
                  max="45"
                  [(ngModel)]="ageLenge"></ion-range>
    </div>
    <div class="input-panel">
      <div class="label">선호지역</div>
      <div name="location" 
           class="select" 
           [ngClass]="{'none-selected':!dreamForm.location}"
           (click)="openSelect(actLocation)">
        <span *ngIf="!dreamForm.location">설정해주세요</span>
        <span *ngIf="dreamForm.location">{{dreamForm.location}}</span>
      </div>
    </div>
    <div class="input-panel">
      <div class="label">키</div>
      <div name="tall" 
           class="select" 
           [ngClass]="{'none-selected':!dreamForm.tall}"
           (click)="openSelect(actTall)">
        <span *ngIf="!dreamForm.tall">설정해주세요</span>
        <span *ngIf="dreamForm.tall">{{dreamForm.tall}}</span>
      </div>
    </div>
    <div class="input-panel">
      <div class="label">체형</div>
      <div name="bodyType" 
           class="select" 
           [ngClass]="{'none-selected':!dreamForm.bodyType}"
           (click)="openSelect(actBodyType)">
        <span *ngIf="!dreamForm.bodyType">설정해주세요</span>
        <span *ngIf="dreamForm.bodyType">{{dreamForm.bodyType}}</span>
      </div>
    </div>
    <div class="input-panel">
      <div class="label">성격</div>
      <div name="mind" 
           class="select" 
           [ngClass]="{'none-selected':true}">
        <span>설정해주세요</span>
        <!-- <span *ngIf="dreamForm">{{dreamForm.}}</span> -->
      </div>
    </div>
    <div class="input-panel">
      <div class="label">종교</div>
      <div name="faith" 
           class="select" 
           [ngClass]="{'none-selected':!dreamForm.faith}"
           (click)="openSelect(actFaith)">
        <span *ngIf="!dreamForm.faith">설정해주세요</span>
        <span *ngIf="dreamForm.faith">{{dreamForm.faith}}</span>
      </div>
    </div>
    <div class="input-panel">
      <div class="label">흡연</div>
      <div name="smoking" 
           class="select" 
           [ngClass]="{'none-selected':!dreamForm.smoking}"
           (click)="openSelect(actSmoking)">
        <span *ngIf="!dreamForm.smoking">설정해주세요</span>
        <span *ngIf="dreamForm.smoking">{{dreamForm.smoking}}</span>
      </div>
    </div>
    <div class="input-panel">
      <div class="label">음주</div>
      <div name="drinking" 
           class="select" 
           [ngClass]="{'none-selected':!dreamForm.drinking}"
           (click)="openSelect(actDrinking)">
        <span *ngIf="!dreamForm.drinking">설정해주세요</span>
        <span *ngIf="dreamForm.drinking">{{dreamForm.drinking}}</span>
      </div>
    </div>
    
  </div>

</div>
</ion-content>`,
'pages/sabuzak/main/main' : `<!--
/**
 * Created by wonseok Lee on 17/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/2017
 *
 * Updater    수정자 - wonseok Lee 17/08/2017
 */
-->     

<ion-content>
  
  <div class="main-content">
    <img class="icon-absolute menu-icon" 
         name="menu"
         src="assets/img/header_navicon_w.png"
         (click)="toggleMenu()">
    <img class="icon-absolute like-icon" 
         name="thumbs-up"
         src="assets/img/header_like_w.png"
         (click)="goSelectDetail(pageList.selectDetail)">
    <div class="icon-absolute list-up"
         (click)="goSelectList(pageList.selectList)">
      리스트 보기
      <ion-icon name="ios-arrow-up"></ion-icon>
    </div>
    
    <div class="recommend" 
         (click)="selectProfile(pageList.viewProfile,recommend)"
         *ngFor="let recommend of recommendList; let i = index">
      <div class="introduce">
        {{recommend.nickName}}
        <img src="assets/img/ic_view_profile.png"
             (click)="videoPlay($event, i)">
      </div>
      <div class="sub-detail">
        <span>{{recommend.location}}</span>
        <span>{{recommend.age}}</span>
        <span *ngIf="recommend.job">{{recommend.job}}</span>
      </div>
      <video id="video{{i}}" loop>
        <source src="assets/video/typing-cat.mp4" type="video/mp4">
      </video>
      <div class="lock-screen" 
           (click)="screenGuard($event)"
           *ngIf = "recommend.lock == locker">
        <img src="assets/img/main_locked.png">
      </div>
    </div>
  </div>

</ion-content>`,
'pages/sabuzak/notice/notice' : `<!--
/**
 * Created by wonseok Lee on 21/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 21/08/2017
 *
 * Updater    수정자 - wonseok Lee 21/08/2017
 */
-->     
<ion-header class="notice-header">
    <ion-navbar hideBackButton>
      <ion-buttons left>
        <button class="btn-back"
                (click)="goBack()">
          <img src="assets/img/header_back_arrow_bk.png"/>
        </button>
      </ion-buttons>
      <ion-title>
        <div>공지사항</div>
      </ion-title>
    </ion-navbar>
  </ion-header>
  
  <ion-content>
    
    <div class="notice-content">
      <ul class="list-container">
        <li *ngFor="let notice of noticeList">

          <div class="message-wrapper" (click)="stateToggle(notice)">
            <ul class="message-box">
              <li>{{notice.title}}</li>
              <li>{{notice.createdAt|date:"yyyy.MM.dd HH:mm:ss"}}</li>
            </ul>
            <div class="icon-box">
              <img src="assets/img/profile_select_down_arrow.png" *ngIf="!notice.state">
              <img src="assets/img/profile_select_down_arrow.png" *ngIf="notice.state">
            </div>
          </div>
          <div class="detail-wrapper" *ngIf="notice.state">
            {{notice.content}}
          </div>

        </li>
      </ul>
    </div>
      
  </ion-content>`,
'pages/sabuzak/notice-list/notice-list' : `<!--
/**
 * Created by wonseok Lee on 18/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 18/08/2017
 *
 * Updater    수정자 - wonseok Lee 18/08/2017
 */
-->     
<ion-header class="notice-list-header">
    <ion-navbar hideBackButton>
      <ion-buttons left>
        <button class="btn-back"
                (click)="goBack()">
          <img src="assets/img/header_back_arrow_bk.png"/>
        </button>
      </ion-buttons>
      <ion-title>
        <div>알림</div>
      </ion-title>
    </ion-navbar>
  </ion-header>
  
  <ion-content>
      
    <div class="notice-list-content">
      <ul class="list-container">
        <li *ngFor="let i of [1,2,3,4,5,6]">
          <ul class="message-wrapper">
            <li>[우수회원 특별 이벤트]오늘 20시 전까지만 사용한 보너스 소개팅이 도착했습니다.</li>
            <li>2017.07.21 02:30</li>
          </ul>
          <div class="icon-wrapper">
            <img src="assets/img/drawer_gnb_arrow.png">
          </div>
        </li>
        <li *ngFor="let i of [1,2,3,4,5,6]">
            <ul class="message-wrapper">
              <li>오늘의 매칭이 전달되었습니다.</li>
              <li>2017.07.21 02:30</li>
            </ul>
            <div class="icon-wrapper">
              <img src="assets/img/drawer_gnb_arrow.png">
            </div>
          </li>
      </ul>
    </div>
      
  </ion-content>`,
'pages/sabuzak/password-change/password-change' : `<!--
/**
 * Created by wonseok Lee on 21/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 21/08/2017
 *
 * Updater    수정자 - wonseok Lee 21/08/2017
 */
-->     

<ion-header class="password-change-header">
    <ion-navbar hideBackButton="true">
  
      <ion-buttons left>
        <button class="btn-back"
                (click)="goBack()">
          <img src="assets/img/register_back_arrow.png"/>
        </button>
      </ion-buttons>
      <ion-title>
        <div>비밀번호 변경</div>
      </ion-title>
      <ion-buttons right>
      </ion-buttons>
  
    </ion-navbar>
  </ion-header>
  <ion-content>
  
  <div class="password-change-content">
    <div class="input-container">
      <input type="password" placeholder="기존 비밀번호">
      <input type="password" placeholder="새 비밀번호">
      <input type="password" placeholder="비밀번호 확인">
    </div>
    <div class="submit-container">
      변경하기
    </div>
  
  </div>
  </ion-content>`,
'pages/sabuzak/recharge-ball/recharge-ball' : `<!--
/**
 * Created by wonseok Lee on 18/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 18/08/2017
 *
 * Updater    수정자 - wonseok Lee 18/08/2017
 */
-->     

<ion-header class="recharge-ball-header">
  <ion-navbar hideBackButton="true">

    <ion-buttons left>
      <button class="btn-back"
              (click)="goBack()">
        <img src="assets/img/register_back_arrow.png"/>
      </button>
    </ion-buttons>
    <ion-title>
      <div>구슬충전소</div>
    </ion-title>
    <ion-buttons right>
    </ion-buttons>

  </ion-navbar>
</ion-header>
<ion-content>

<div class="recharge-ball-content">
  <div class="img-container">
    <img src="assets/img/img_market_ball.png">
    <div>
      구슬 300개 보유중
    </div>
  </div>
  <ul class="price-container">
    <li>
      <div class="image-wrapper">
        <img src="assets/img/img_ball_thumbnail.png">
        <span>30 개</span>
      </div>
      <div class="price">
        $ 4.39
      </div>
    </li>
    <li>
      <div class="image-wrapper">
        <img src="assets/img/img_ball_thumbnail.png">
        <span>80 + 5 개</span>
      </div>
      <div class="price">
        $ 10.99
      </div>
    </li>
    <li>
      <div class="image-wrapper">
        <img src="assets/img/img_ball_thumbnail.png">
        <span>150 + 15 개</span>
      </div>
      <div class="price">
        $ 18.69
      </div>
    </li>
    <li>
      <div class="image-wrapper">
        <img src="assets/img/img_ball_thumbnail.png">
        <span>300 + 40 개</span>
      </div>
      <div class="price">
        $ 36.29
      </div>
    </li>
    <li>
      <div class="image-wrapper">
        <img src="assets/img/img_ball_thumbnail.png">
        <span>600 + 100 개</span>
      </div>
      <div class="price">
        $ 65.99
      </div>
    </li>
  </ul>
  <div class="usage-container" (click)="goUsage()">
    사용내역
  </div>

</div>
</ion-content>`,
'pages/sabuzak/search-liker/search-liker' : `<!--
/**
 * Created by wonseok Lee on 18/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 18/08/2017
 *
 * Updater    수정자 - wonseok Lee 18/08/2017
 */
-->     
<ion-header class="search-liker-header">
  <ion-navbar hideBackButton>
    <ion-buttons left>
      <button class="btn-img"
              (click)="goBack()">
        <img src="assets/img/header_back_arrow_bk.png"/>
      </button>
    </ion-buttons>
    <ion-title>
      <div>나를 좋아하는 이성찾기</div>
    </ion-title>
  </ion-navbar>
</ion-header>
    
<ion-content>  
  <div class="search-liker-content">
    <ul class="list-container">
      <li>
        <ul class="list-box">
          <li class="thumbnail" *ngFor="let item of [1,2,3,4,5]">
            <div class="pic"></div>
            <div class="label">
              운동하는여자
            </div>
          </li>
          <li class="thumbnail-finding">
            <div class="pic">
              <img src="assets/img/ic_placeholder.png">
            </div>
          </li>
        </ul>
        <div class="notice-wrapper">
          <div class="searching">
            나에게 관심있는 이성을 찾는중.. (0/6)
          </div>
          <div class="notice">
            <p>
              나를 좋아하는 이성 6명을 찾을 때 까지 나의 프로필을 계속 노출시킵니다. 매칭 확률이 매우 높아지니 나를 좋아하는 이성을 찾아보세요.
            </p>
            <p>
              중단을 원하시면 그만하기를 눌러주세요 사용하신 구슬은 환불되지 않습니다.
            </p>
          </div>
        </div>
        
      </li>
    </ul>
    <div class="stop-wrapper">
      그만하기
    </div>
  </div>
    
</ion-content>`,
'pages/sabuzak/select-detail/select-detail' : `<!--
/**
 * Created by wonseok Lee on 18/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 18/08/2017
 *
 * Updater    수정자 - wonseok Lee 18/08/2017
 */
-->     
<ion-header class="select-detail-header">
    <ion-navbar hideBackButton>
      <ion-buttons left>
        <button class="btn-img" 
                (click)="goBack()">
            <img src="assets/img/header_back_arrow_bk.png"/>
        </button>
      </ion-buttons>
      <ion-title>
        <div>평가({{availableCnt}})</div>
      </ion-title>
    </ion-navbar>
  </ion-header>
    
  <ion-content>
    <div class="select-detail-content">
      <div class="video-container">
        <ion-card class="absolute more-icon"
                  (click)="modalOpener()">
          <ion-icon name="more"></ion-icon>
        </ion-card>
      </div>
      <div class="info-container">
        <div class="nickname">
          {{user.nickName}}
        </div>
        <div class="info">
          {{user.age}},{{user.location}}
          <span *ngIf="user.interest">- {{user.interest}}을 좋아하는 
            <span *ngIf="user.gender == 'female'">여자</span>
            <span *ngIf="user.gender == 'male'">남자</span>
          </span>
        </div>
      </div>
      <div class="up-and-down-container">
        <div class="up-wrapper"
             (click)="upNDown('down')">
          <img src="assets/img/ic_evaluation_down.png">
        </div>
        <div class="down-wrapper"
             (click)="upNDown('up')">
          <img src="assets/img/ic_evaluation_up.png">
        </div>
      </div>
    </div>
  </ion-content>`,
'pages/sabuzak/select-list/select-list' : `<!--
/**
 * Created by wonseok Lee on 18/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 18/08/2017
 *
 * Updater    수정자 - wonseok Lee 18/08/2017
 */
-->     
<ion-header class="select-list-header">
  <ion-navbar hideBackButton>
    <ion-buttons left>
      <button class="btn-img" menuToggle>
        <img src="assets/img/header_navicon.png"/>
      </button>
    </ion-buttons>
    <ion-title>
      <div>리스트</div>
    </ion-title>
    <ion-buttons right>
      <button class="btn-img"
              (click)="goSelectDetail(pageDrive.selectDetail)">
        <img src="assets/img/header_like_bk.png">
      </button>
    </ion-buttons>
  
  </ion-navbar>
</ion-header>
  
<ion-content (ionScroll)="isTopChk()">  
  <div class="select-list-content">
    <div class="top-fix-icon" 
          (click)="goTop()" 
          [ngClass]="{'hide':!topState}">
          <img src="assets/img/ic_scroll_to_top.png">
    </div>

    <div class="search-btn-container"
         (click)="pagePusher(pageDrive.searchLiker)">
      나를 좋아하는 이성찾기
    </div>
    <ul class="list-container">
      <li *ngFor="let listItem of listCollection">
        <div class="sub-title" 
             *ngIf="listItem.users.length">
          {{listItem.title}}
        </div>
        <ul class="list-box"
            *ngIf="listItem.users.length">
          <li class="thumbnail" *ngFor="let user of listItem.users">
            <div class="pic">
              
            </div>
            <div class="label">
              {{user.user.interest}}
            </div>
            <div class="info">
              {{user.user.age}}세,{{user.user.location}}
            </div>
          </li>
          <!-- 
            <li class="thumbnail">
              <div class="locked">
                <img src="assets/img/main_locked.png">
              </div>  
              <div class="pic">
                TEST
              </div>
              <div class="label">
                운동하는여자
              </div>
              <div class="info">
                23세,서울
              </div>
            </li>
            <li class="thumbnail">
              <div class="locked">
                <img src="assets/img/main_locked.png">
              </div>
              <div class="pic blur">
                TEST
              </div>
              <div class="label">
                운동하는여자
              </div>
              <div class="info">
                23세,서울
              </div>
            </li> 
        -->
        </ul>
        <div class="more"
             *ngIf = "listItem.users.length">
            <div *ngIf = "listItem.more"
                 (click)="getMore(listItem)">
              더보기
              <img src="assets/img/list_more_down_arrow.png">
            </div>
        </div>
        
      </li>
    </ul>
  </div>
    
</ion-content>`,
'pages/sabuzak/settings/settings' : `<!--
/**
 * Created by wonseok Lee on 21/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 21/08/2017
 *
 * Updater    수정자 - wonseok Lee 21/08/2017
 */
-->     

<ion-header class="settings-header">
    <ion-navbar hideBackButton="true">
  
      <ion-buttons left>
        <button class="btn-back"
                (click)="goBack()">
          <img src="assets/img/register_back_arrow.png"/>
        </button>
      </ion-buttons>
      <ion-title>
        <div>환경설정</div>
      </ion-title>
      <ion-buttons right>
      </ion-buttons>
  
    </ion-navbar>
  </ion-header>
  <ion-content>
  
  <div class="settings-content">
    <ul class="settings-container">
      <li>
        <div>알림받기</div>
        <ion-toggle [(ngModel)]="isAlert"></ion-toggle>
      </li>
      <li>
        <div>푸시알림</div>
        <ion-toggle [(ngModel)]="isPush"></ion-toggle>
      </li>
      <li>
        <div>오늘의 카드 받기</div>
        <ion-toggle [(ngModel)]="isTodayMatch"></ion-toggle>
      </li>
      <li (click)="pagePusher(pageList.passwordChange)">
        <div>비밀번호 변경</div>
        <img src="assets/img/drawer_gnb_arrow.png">
      </li>
      <li>
        <div>로그아웃</div>
        <img src="assets/img/drawer_gnb_arrow.png">
      </li>
      <li>
        <div>서비스 탈퇴</div>
        <img src="assets/img/drawer_gnb_arrow.png">
      </li>
      <li (click)="pagePusher(pageList.notice)">
        <div>공지사항</div>
        <img src="assets/img/drawer_gnb_arrow.png">
      </li>
    </ul>
  
  </div>
  </ion-content>`,
'pages/sabuzak/usage/usage' : `<!--
/**
 * Created by wonseok Lee on 21/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 21/08/2017
 *
 * Updater    수정자 - wonseok Lee 21/08/2017
 */
-->     

<ion-header class="usage-header">
  <ion-navbar hideBackButton="true">

    <ion-buttons left>
      <button class="btn-back"
              (click)="goBack()">
        <img src="assets/img/register_back_arrow.png"/>
      </button>
    </ion-buttons>
    <ion-title>
      <div>구슬 사용내역</div>
    </ion-title>
    <ion-buttons right>
    </ion-buttons>

  </ion-navbar>
</ion-header>
<ion-content>

<div class="usage-content">
  <ul class="usage-container">
    <li class="label">
      <div>날짜</div>
      <div>내용</div>
      <div>변화</div>
      <div>누적</div>
    </li>
    <li class="items" *ngFor="let myBall of myBallList">
      <div>{{myBall.createdAt|date:"MM/dd"}}</div>
      <div class="detail">{{myBall.usage}}</div>
      <div>{{myBall.amount}}</div>
      <div>99</div>
    </li>
  </ul>

</div>
</ion-content>`,
'pages/sabuzak/view-profile/view-profile' : `<!--
/**
 * Created by wonseok Lee on 17/08/2017
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by wonseok Lee <whereiswonny@gmail.com>, 17/08/2017
 *
 * Updater    수정자 - wonseok Lee 18/08/2017
 */
-->     

<ion-content (ionScroll)="isTopChk()">
    
  <div class="view-profile-content">
      <div class="top-fix-icon" 
            (click)="goTop()" 
            [ngClass]="{'hide':!topState}">
            <img src="assets/img/ic_scroll_to_top.png">
      </div>
    <img class="icon-fixed down-arrow" 
         src="assets/img/profile_down_btn.png"
         (click)="dismiss()">
    <div class="image-container">
      <div class="image-wrapper">
        
      </div>
      <div class="mini-image-wrapper">
        <ul class="nickname">
          <li>{{user.nickName}}</li>
          <li>{{user.age}}세, {{user.location}}</li>
        </ul>
        <ul class="mini-images">
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    </div>
    <ul class="rank-container"
        *ngIf="owner">
      <li>나의 현재 등급은 {{user.rank}} 입니다.</li>
      <li>이성으로부터 받은 평가를 기반으로 당신의 등급이 정해집니다.</li>
    </ul>
    <ul class="profile-container" *ngIf="owner">
      <li *ngIf="user.introduce">
        {{user.introduce}}
      </li>
      <li>
        저는 <span>{{user.age}}세</span>이고 <span>{{user.location}}</span>에 살아요
      </li>
      <li *ngIf="user.job">
        직업은 <span>{{user.job}}</span>입니다
      </li>
      <li>
        키는 <span>{{user.tall}}cm</span>에 <span>{{user.bodyType}}</span>한 체형이에요
      </li>
      <li *ngIf="user.faith">
        종교는 <span>{{user.faith}}</span>입니다
      </li>
      <li *ngIf="user.school">
        <span>{{user.school}}</span>에서 공부했어요
      </li>
      <li *ngIf="user.drinking && user.smoking">
        <span>술은 {{user.drinking}}</span>마시는 편이고, <span>{{user.smoking}}</span>입니다.
      </li>
      <li *ngIf="user.interest">
        <span>{{user.interest}}</span>을 좋아해요
      </li>
    </ul>
    <ul class="profile-container" *ngIf="!owner">
      <li *ngIf="user.introduce">
        {{user.introduce}} 
      </li>
      <li>
        저는 not owner page <span>{{user.age}}세</span>이고 <span>{{user.location}}</span>에 살아요
      </li>
      <li *ngIf="user.job">
        직업은 <span>{{user.job}}</span>입니다
      </li>
      <li>
        키는 <span>{{user.tall}}cm</span>에 <span>{{user.bodyType}}</span>한 체형이에요
      </li>
      <li *ngIf="user.faith">
        종교는 <span>{{user.faith}}</span>입니다
      </li>
      <li *ngIf="user.school">
        <span>{{user.school}}</span>에서 공부했어요
      </li>
      <li *ngIf="user.drinking && user.smoking">
        <span>술은 {{user.drinking}}</span>마시는 편이고, <span>{{user.smoking}}</span>입니다.
      </li>
      <li *ngIf="user.interest">
        <span>{{user.interest}}</span>을 좋아해요
      </li>
    </ul>
    <div class="submit-container"
         (click)="likeChk()"
         *ngIf="!owner && (type == 'recommend')">
      좋아요
    </div>
    <div class="submit-container"
         (click)="goProfileUpdate()"
         *ngIf="owner">
      프로필 수정
    </div>
  </div>
    
</ion-content>`,
}
export function getTemplate(id){return templates[id];}