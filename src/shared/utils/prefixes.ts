/**
 * Created by andy on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

// Appzet Source
import {camelCase} from './camel-case';

let cache = {};
let testStyle = document.createElement('div').style;

// Get Prefix
// http://davidwalsh.name/vendor-prefix
const prefix = (function () {
  const styles = window.getComputedStyle(document.documentElement, '');
  const pre = (Array.prototype.slice.call(styles).join('').match(/-(moz|webkit|ms)-/))[1];
  const dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1];

  return {
    dom,
    lowercase: pre,
    css: `-${pre}-`,
    js: pre[0].toUpperCase() + pre.substr(1)
  };
})();

export function getVendorPrefixedName(property) {
  const name = camelCase(property);

  if(!cache[name]) {
    if(testStyle[prefix.css + property] !== undefined) {
      cache[name] = prefix.css + property;
    } else if(testStyle[property] !== undefined) {
      cache[name] = property;
    }
  }

  return cache[name];
}