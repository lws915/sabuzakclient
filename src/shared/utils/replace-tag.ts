/**
 * Created by PHILIP on 23/03/2017
 * As part of nail-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by PHILIP <hbnb7894@gmail.com>, 23/03/2017
 *
 * Updater    수정자 - PHILIP 23/03/2017
 */

// Export lib
declare var _;
declare var Logger;

// Logger.domains.push("replace-tag");

export function ReplaceTag(text){
  Logger.silly('replace-tag');

  let html = '';
  let tempHtml = _.split(text, ' ');

  tempHtml.forEach((string) => {
    if(string.indexOf('#') > -1){
      string = '<span class="tag">' + string + '</span>';
    }
    html += string + ' ';
  });

  return html;
}
