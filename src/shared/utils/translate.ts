/**
 * Created by andy on 02/11/2016
 * As part of cafe-1-template
 *
 * Copyright (C) Applicat (www.applicat.co.kr) - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Andy Yoon Yong Shin <developer@applicat.co.kr>, 02/11/2016
 *
 * Updater    수정자 - PHILIP 21/03/2017
 */

// Export lib
declare var Logger;

// Appzet Source
import {getVendorPrefixedName} from './prefixes';
import {camelCase} from './camel-case';

// browser detection and prefixing tools
let transform = getVendorPrefixedName('transform');
let backfaceVisibility = getVendorPrefixedName('backfaceVisibility');
let hasCSSTransforms = !!getVendorPrefixedName('transform');
let hasCSS3DTransforms = !!getVendorPrefixedName('perspective');
let ua = window.navigator.userAgent;
let isSafari = (/Safari\//).test(ua) && !(/Chrome\//).test(ua);

// Logger.domains.push("translate");

export function translateXY(styles, x, y) {
  Logger.silly('translate', 'translateXY');
  if (hasCSSTransforms) {
    if (!isSafari && hasCSS3DTransforms) {
      styles[transform] = `translate3d(${x}px, ${y}px, 0)`;
      styles[backfaceVisibility] = 'hidden';
    } else {
      styles[camelCase(transform)] = `translate(${x}px, ${y}px)`;
    }
  } else {
    styles.top = `${y}px`;
    styles.left = `${x}px`;
  }
}
